//
//  BEFade.m
//  BrushEngine
//
//  Created by Germano Guerrini on 11/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEFade.h"
#import "BESprite.h"

#pragma mark - BEFade

@interface BEFade ()

@property (nonatomic, readonly) float startAlpha;
@property (nonatomic, readonly) float endAlpha;
@property (nonatomic, readonly) float variance;
@property (nonatomic, readonly) float cachedVariance;

@end

@implementation BEFade

- (id)initWithTarget:(BEGameObject *)target alpha:(float)alpha duration:(NSTimeInterval)duration
{
    if (![target respondsToSelector:@selector(alpha)]) {
        // TODO Better logging
        NSLog(@"BEFade: you should pass a BESprite or a subclass of it");
        return nil;
    }
    if ((self = [super initWithTarget:target duration:duration])) {
        _endAlpha = alpha;
        _cachedVariance = INFINITY;
    }
    return self;
}

- (id)initWithTarget:(BEGameObject *)target alpha:(float)alpha
{
    return [self initWithTarget:target alpha:alpha duration:0.0f];
}

- (float)variance
{
    if (_cachedVariance == INFINITY) {
        _cachedVariance = _endAlpha - _startAlpha;
    }
    return _cachedVariance;
}

- (void)setup
{
    [super setup];
    _startAlpha = ((BESprite *)self.target).alpha;
}

- (void)stop
{
    [super stop];
    _cachedVariance = INFINITY;
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BESprite *)self.target).alpha = _startAlpha + self.variance * offset;
}

@end

#pragma mark - BEFadeIn

@implementation BEFadeIn

- (id)initWithTarget:(BEGameObject *)target duration:(NSTimeInterval)duration
{
    return [super initWithTarget:target alpha:1.0f duration:duration];
}

@end

#pragma mark - BEFadeOut

@implementation BEFadeOut

- (id)initWithTarget:(BEGameObject *)target duration:(NSTimeInterval)duration
{
    return [super initWithTarget:target alpha:0.0f duration:duration];
}

@end
