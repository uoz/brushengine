//
//  BEChoreography.m
//  BrushEngine
//
//  Created by Germano Guerrini on 26/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEChoreography.h"

@interface BEChoreography ()

@property (nonatomic, readonly) BEChoreographyType type;
@property (nonatomic, readonly) NSArray *actions;
@property (nonatomic, readonly) id<BEAction> currentAction;
@property (nonatomic, readonly) NSUInteger currentActionIndex;
@property (nonatomic, readonly) NSUInteger actionCount;

@end

@implementation BEChoreography

@synthesize delegate;

- (id)initWithType:(BEChoreographyType)type actionsArray:(NSArray *)actions
{
    if ((self = [super init])) {
        _type = type;
        _actions = actions;
        for (id<BEAction> action in _actions) {
            action.delegate = self;
        }
        _actionCount = [actions count];
        _currentActionIndex = 0;
        if (type == BEChoreographyTypeSequence) {
            _currentAction = [_actions objectAtIndex:_currentActionIndex];
        }
    }
    return self;
}

- (id)initWithType:(BEChoreographyType)type actions:(id<BEAction>)firstAction, ...
{
    NSMutableArray *actions = [NSMutableArray arrayWithCapacity:4];
    va_list args;
    va_start(args, firstAction);
    for (id<BEAction> action = firstAction; action != nil; action = va_arg(args, id<BEAction>)) {
        if (type == BEChoreographyTypeSequence && ![action isSequenceable]) {
            NSLog(@"Unsequenceable action added to a sequence."); // TODO
        }
        [actions addObject:action];
    }
    va_end(args);
    return [self initWithType:type actionsArray:actions];
}

#pragma mark - BEAction Protocol Methods

//- (void)setup
//{
//    if (type == BEChoreographyTypeConcurrent) {
//        [actions makeObjectsPerformSelector:@selector(setup)];
//    } else {
//        [currentAction setup];
//    }
//}

- (void)play
{
    if (_type == BEChoreographyTypeConcurrent) {
        [_actions makeObjectsPerformSelector:@selector(play)];
    } else {
        [_currentAction play];
    }
}

- (void)pause
{
    if (_type == BEChoreographyTypeConcurrent) {
        [_actions makeObjectsPerformSelector:@selector(pause)];
    } else {
        [_currentAction pause];
    }
}

- (void)toggle
{
    if (_type == BEChoreographyTypeConcurrent) {
        [_actions makeObjectsPerformSelector:@selector(toggle)];
    } else {
        [_currentAction toggle];
    }
}

- (void)stop
{
    _currentActionIndex = _actionCount - 1;
    if (_type == BEChoreographyTypeConcurrent) {
        [_actions makeObjectsPerformSelector:@selector(stop)];
    } else {
        [_currentAction stop];
    }
}

- (void)reverse
{
    if (_type == BEChoreographyTypeConcurrent) {
        [_actions makeObjectsPerformSelector:@selector(reverse)];
    } else {
        [_currentAction reverse];
    }
}

- (BOOL)isSequenceable
{
    for (id<BEAction> action in _actions) {
        if (![action isSequenceable]) {
            return NO;
        }
    }
    return YES;
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    if (_type == BEChoreographyTypeConcurrent) {
        for (id<BEAction> action in _actions) {
            [action updateWithInterval:interval];
        }
    } else {
        [_currentAction updateWithInterval:interval];
    }
}

#pragma mark - BEActionDelegate Protocol Methods

- (void)actionDidStart:(id<BEAction>)action
{
    // Mmm... Maybe nothing to do here?
}

- (void)actionDidStop:(id<BEAction>)action
{
    // currentActionIndex is used as an array index in BEChoreographyTypeSequence
    // and as an executed actions counter in BEChoreographyTypeConcurrent
    if (++_currentActionIndex < _actionCount) {
        if (_type == BEChoreographyTypeSequence) {
            _currentAction = [_actions objectAtIndex:_currentActionIndex];
            // Call play explicitly because the action could have been
            // stopped if it has been already played in this sequence
            [_currentAction play];
        }
    }
    if (_currentActionIndex == _actionCount) {
        // If we are done, reset the index/counter so that we can be called again
        _currentActionIndex = 0;
        if (_type == BEChoreographyTypeSequence) {
            _currentAction = [_actions objectAtIndex:_currentActionIndex];
        }
        [self.delegate actionDidStop:self];
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"type: %@, actionCount: %d, currentActionIndex: %d\n", 
            _type == BEChoreographyTypeSequence ? @"sequence" : @"concurrent",
            _actionCount, _currentActionIndex];
}

@end
