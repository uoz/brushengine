//
//  BEAction.m
//  BrushEngine
//
//  Created by Germano Guerrini on 18/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"
#import "BEActionManager.h"

@interface BEAction ()

@property (nonatomic, readonly, weak) BEActionManager __weak *actionManager;
@property (nonatomic, readonly) BEActionDirection direction;
@property (nonatomic, readonly) BEActionState state;
@property (nonatomic, readonly) BETweeningFunctionPtr tweeningFunction;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval elapsedTime;
@property (nonatomic, readonly) NSTimeInterval timeOffset;
@property (nonatomic, readonly) BOOL needsSetup;

@end

@implementation BEAction

@synthesize delegate;

static int currentActionID = 0;

+ (int)nextActionID
{
    return currentActionID++;
}

- (id)initWithTarget:(id)target duration:(NSTimeInterval)duration direction:(BEActionDirection)direction
{
    if ((self = [super init])) {
        _actionManager = [BEActionManager sharedActionManager];
        _actionID = [BEAction nextActionID];
        _target = target;
        _duration = duration;
        _direction = direction;
        _elapsedTime = (_direction == BEActionDirectionForward) ? 0.0f : duration;
        _timeOffset = 0.0f;
        _state = BEActionStateStopped;
        _needsSetup = YES;
        self.tweeningFunctionType = BETweeningFunctionLinear;
        [_actionManager registerAction:self];
    }
    return self;
}

- (id)initWithTarget:(id)target duration:(NSTimeInterval)duration
{
    return [self initWithTarget:target duration:duration direction:BEActionDirectionForward];
}

- (id)initWithTarget:(id)target
{
    return [self initWithTarget:target duration:0.0f direction:BEActionDirectionForward];
}

- (BOOL)isReversed
{
    return _direction == BEActionDirectionBackward;
}

- (BOOL)isRunning
{
    return _state == BEActionStateRunning;
}

#pragma mark - Tweening Function Type Setter

- (void)setTweeningFunctionType:(BETweeningFunctionType)type
{
    _tweeningFunction = [BETweeningFunction functionOfType:type];
}

#pragma mark - BEAction Protocol Methods

- (void)setup
{
    _needsSetup = NO;
}

- (void)play
{
    if (_needsSetup) {
        [self setup];
    }
    _state = BEActionStateRunning;
    [self.delegate actionDidStart:self];
}

- (void)pause
{
    _state = BEActionStatePaused;
}

- (void)toggle
{
    if (_state == BEActionStateRunning) {
        [self pause];
    } else if (_state == BEActionStatePaused) {
        [self play];
    }
}

- (void)stop
{
    _state = BEActionStateStopped;
    _needsSetup = YES;
    _elapsedTime = (_direction == BEActionDirectionForward) ? 0.0f : _duration;
    // TODO Do we need a stopAction?
    [self.delegate actionDidStop:self];
}

- (void)reverse
{
    if (_direction == BEActionDirectionForward) {
        _direction = BEActionDirectionBackward;
    } else {
        _direction = BEActionDirectionForward;
    }
}

- (BOOL)isSequenceable
{
    return YES;
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    if (_state != BEActionStateRunning) {
        return;
    }
    
    if (_duration == 0.0f) {
        [self advanceToOffset:1.0f];
    } else {
        if (_direction == BEActionDirectionForward) {
            _elapsedTime += interval;
        } else {
            _elapsedTime -= interval;
        }
        _timeOffset = CLAMP(_elapsedTime / _duration, 0.0f, 1.0f);
        [self advanceToOffset:_tweeningFunction(_timeOffset)];
    }
    if ((_direction == BEActionDirectionForward && _elapsedTime >= _duration) ||
        (_direction == BEActionDirectionBackward && _elapsedTime <= 0.0f)) {
        [self stop];
    }
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    // Implemented by subclasses
}

- (void)dealloc
{
    [_actionManager unregisterAction:self];
}

@end
