//
//  BEGameObject.h
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BETarget.h"
#import "BERendering.h"
#import "BERenderingManager.h"

@interface BEGameObject : BETarget <BERendering>
{
    CGSize _size;
    GLKVector2 _pivot;
}

@property (nonatomic, readonly, weak) BERenderingManager *renderingManager;
@property (nonatomic) float rotation;
@property (nonatomic) GLKVector2 position;
@property (nonatomic) GLKVector2 scale;
@property (nonatomic) CGSize size;
@property (nonatomic) BOOL isVisible;
@property (nonatomic) GLKMatrix4 modelViewMatrix;
@property (nonatomic, weak) BEGameObject *parent;
@property (nonatomic, strong, readonly) NSMutableArray *children;
@property (nonatomic) GLKVector2 pivot; // TODO Handle protected

// --------------------------------------------------------------------------------
// Children Management
// --------------------------------------------------------------------------------

/** Add a child to the object.
 If a child has already a parent, it will be removed from it and added to the current object.
 Self-parenting is not allowed and argument must be non-nil.
 */
- (void)addChild:(BEGameObject *)child;

/** Removes the given child from the object.
 If the object is not the direct parent of the child, the method has no effect.
 */
- (void)removeChild:(BEGameObject *)child;

/** Removes the last child from the object. */
- (void)removeLastChild;

/** Removes all children from the object. */
- (void)removeAllChildren;

/** Returns the last child of the current object.
 Can be nil if the object has no children.
 */
- (BEGameObject *)getLastChild;

/** Returns the child located at the given index.
 Can be nil if the index is out of bounds.
 */
- (BEGameObject *)getChildAtIndex:(NSUInteger)index;

/** Returns YES or NO whether an object is a descendant of the current object. */
- (BOOL)isDescendantOf:(BEGameObject *)anObject;

/** Swaps the given two children.
 If one or both of them cannot be found, the method has no effect.
 */
- (void)swapChild:(BEGameObject *)child1 withChild:(BEGameObject *)child2;

// --------------------------------------------------------------------------------
// Transformation Methods
// --------------------------------------------------------------------------------

/** Returns the transformation matrix that converts the local coordinate space to the parent coordinate space. */
- (GLKMatrix4)localToParentMatrix;

/** Returns the transformation matrix that converts the parent coordinate space to the local coordinate space. */
- (GLKMatrix4)parentToLocalMatrix;

/** Returns the transformation matrix that converts the local coordinate space to the world coordinate space. */
- (GLKMatrix4)localToWorldMatrix;

/** Returns the transformation matrix that converts the world coordinate space to the local coordinate space. */
- (GLKMatrix4)worldToLocalMatrix;

/** Transforms a point from the local coordinate space to the world coordinate space.
 If the relativeToPivot flag is YES, then the point coordinates are adjusted to consider the pivot point.
 */
- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint relativeToPivot:(BOOL)relativeToPivot;

/** Transforms a point from the local coordinalte space to the world coordinate space.
 The pivot point of the object is ignored.
 */
- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint;

/** Transforms a point from the world coordinate space to the local coordinate space.
 If the relativeToPivot flag is YES, then the point coordinates are adjusted to consider the pivot point.
 */
- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint relativeToPivot:(BOOL)relativeToPivot;

/** Transforms a point from the world coordinate space to the local coordinate space.
 The pivot point of the object is ignored.
 */
- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint;

// TODO
- (void)updateTransform;
- (void)draw;

@end
