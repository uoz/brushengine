//
//  BEProfiler.m
//  BrushEngine
//
//  Created by Germano Guerrini on 07/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEProfiler.h"
#import "BEViewController.h"
#import "BESettings.h"

@interface BEProfiler ()

@property (nonatomic, readonly) NSMutableArray *textureArray;
@property (nonatomic, readonly) NSString *labelTextBuffer;
@property (nonatomic, readonly) uint objectsCount;
@property (nonatomic, readonly) uint trianglesCount;
@property (nonatomic, readonly) uint verticesCount;
@property (nonatomic, readonly) uint memoryForTextures;
@property (nonatomic, readonly) uint textureSwitchCount;

- (NSString *)stringFromFileSize:(NSInteger)size;

@end

//static uint numShaderSwitches;
//static uint numTextureBinds;
//static uint numFrameBuffersSwitches;
//static uint numUniformsSet;
//
//static uint memoryShaders;
//static uint memoryFrameBuffers;


@implementation BEProfiler

+ (BEProfiler *)sharedProfiler
{
    static BEProfiler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BEProfiler alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if ((self = [super init])) {
        _textureArray = [[NSMutableArray alloc] initWithCapacity:10];
        _labelTextBuffer = [self labelText];
        CGSize size = [_labelTextBuffer sizeWithFont:[UIFont fontWithName:@"Courier New" size:14.0f] constrainedToSize:CGSizeMake(384.0f, 1024.0f)];
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, size.width + 32.0f, size.height + 32.0f)];
        _label.numberOfLines = 0;
        _label.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        _label.font = [UIFont fontWithName:@"Courier New" size:14.0f];
        _label.textColor = [UIColor whiteColor];
    }
    return self;
}

- (NSString *)labelText
{
    _labelTextBuffer = @"";
    if (_delegate != nil) {
        _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" FPS: %.2f\n", [((BEViewController *)_delegate) currentFramesPerSecond]];
    }
    if (BE_PROFILER_COMPLETE == 0) {
        return _labelTextBuffer;
    }
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Objects: %d\n", _objectsCount];
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Triangles: %d\n", _trianglesCount];
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Vertices: %d\n", _verticesCount];
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Textures: %d\n", [_textureArray count]];
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Memory for textures: %@\n", [self stringFromFileSize:_memoryForTextures]];
    _labelTextBuffer = [_labelTextBuffer stringByAppendingFormat:@" Texture switches per frame: %d\n", _textureSwitchCount];
    return _labelTextBuffer;
}

- (void)addGameObject:(BEGameObject *)gameObject
{
    _objectsCount++;
    _trianglesCount += 2;
    _verticesCount += 4;
}

- (void)removeGameObject:(BEGameObject *)gameObject
{
    _objectsCount--;
    _trianglesCount -= 2;
    _verticesCount -= 4;
}

- (void)addTextureWithPath:(NSString *)path
{
    if (![_textureArray containsObject:path]) {
        [_textureArray addObject:path];
        _memoryForTextures += [[[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil] fileSize];
    }
}

- (void)removeTextureWithPath:(NSString *)path
{
    if ([_textureArray containsObject:path]) {
        [_textureArray removeObject:path];
        _memoryForTextures -= [[[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil] fileSize];
    }
}

- (void)resetTextureSwitchCount
{
    _textureSwitchCount = 0;
}

- (void)incrementTextureSwitchCount
{
    _textureSwitchCount++;
}

- (NSString *)stringFromFileSize:(NSInteger)size
{
    float floatSize = size;
    if (size < 1023)
        return ([NSString stringWithFormat:@"%i bytes", size]);
    floatSize /= 1024;
    if (floatSize < 1023)
        return ([NSString stringWithFormat:@"%.2f KB", floatSize]);
    floatSize /= 1024;
    if (floatSize < 1023)
        return ([NSString stringWithFormat:@"%.2f MB", floatSize]);
    floatSize /= 1024;
    
    return ([NSString stringWithFormat:@"%.2f GB", floatSize]);
}

@end
