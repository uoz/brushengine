//
//  BETranslate.m
//  BrushEngine
//
//  Created by Germano Guerrini on 24/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BETranslate.h"
#import "BEGameObject.h"

#pragma mark - BETranslate

@interface BETranslate ()

@property (nonatomic, readonly) GLKVector2 translation;
@property (nonatomic, readonly) GLKVector2 cachedTranslation;
@property (nonatomic) GLKVector2 startPosition;
@property (nonatomic) GLKVector2 endPosition;

@end

@implementation BETranslate

- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target duration:duration])) {
        _cachedTranslation = GLKVector2Make(INFINITY, INFINITY);
    }
    return self;
}

- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position
{
    return [self initWithTarget:target position:position duration:0.0f];
}

- (GLKVector2)translation
{
    if (GLKVector2AllEqualToScalar(_cachedTranslation, INFINITY)) {
        _cachedTranslation = GLKVector2Subtract(self.endPosition, self.startPosition);
    }
    return _cachedTranslation;
}

- (void)setup
{
    [super setup];
    self.startPosition = ((BEGameObject *)self.target).position;
}

- (void)stop
{
    [super stop];
    _cachedTranslation = GLKVector2Make(INFINITY, INFINITY);
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BEGameObject *)self.target).position = GLKVector2Add(self.startPosition, GLKVector2MultiplyScalar(self.translation, offset));
}

@end

#pragma mark - BETranslateTo

@implementation BETranslateTo

- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target position:position duration:duration])) {
        self.endPosition = position;
    }
    return self;
}

@end

#pragma mark - BETranslateBy

@interface BETranslateBy ()

@property (nonatomic) GLKVector2 offset;

@end

@implementation BETranslateBy

- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target position:position duration:duration])) {
        _offset = position;
    }
    return self;
}

- (void)setup
{
    [super setup];
    self.endPosition = GLKVector2Add(self.startPosition, _offset);
}

@end
