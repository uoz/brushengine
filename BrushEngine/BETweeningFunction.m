//
//  BETweeningFunction.m
//  BrushEngine
//
//  Created by Germano Guerrini on 25/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

// Based on http://www.robertpenner.com/easing/

#import "BETweeningFunction.h"

#pragma mark - Declarations

#pragma mark Linear
NSTimeInterval BETweeningFunctionPtrLinear(NSTimeInterval t);

#pragma mark Quad
NSTimeInterval BETweeningFunctionPtrQuadEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuadEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuadEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuadEaseOutIn(NSTimeInterval t);

#pragma mark Cubic
NSTimeInterval BETweeningFunctionPtrCubicEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCubicEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCubicEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCubicEaseOutIn(NSTimeInterval t);

#pragma mark Quartic
NSTimeInterval BETweeningFunctionPtrQuarticEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuarticEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuarticEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuarticEaseOutIn(NSTimeInterval t);

#pragma mark Quintyic
NSTimeInterval BETweeningFunctionPtrQuinticEaseIn(NSTimeInterval t); 
NSTimeInterval BETweeningFunctionPtrQuinticEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuinticEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrQuinticEaseOutIn(NSTimeInterval t); 

#pragma mark Sinusoidal
NSTimeInterval BETweeningFunctionPtrSinusoidalEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrSinusoidalEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrSinusoidalEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrSinusoidalEaseOutIn(NSTimeInterval t); 

#pragma mark Exponential
NSTimeInterval BETweeningFunctionPtrExponentialEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrExponentialEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrExponentialEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrExponentialEaseOutIn(NSTimeInterval t); 

#pragma mark Circular
NSTimeInterval BETweeningFunctionPtrCircularEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCircularEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCircularEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrCircularEaseOutIn(NSTimeInterval t); 

#pragma mark Elastic
NSTimeInterval BETweeningFunctionPtrElasticEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrElasticEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrElasticEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrElasticEaseOutIn(NSTimeInterval t); 

#pragma mark Back
NSTimeInterval BETweeningFunctionPtrBackEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBackEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBackEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBackEaseOutIn(NSTimeInterval t); 

#pragma mark Bounce
NSTimeInterval BETweeningFunctionPtrBounceEaseIn(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBounceEaseOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBounceEaseInOut(NSTimeInterval t);
NSTimeInterval BETweeningFunctionPtrBounceEaseOutIn(NSTimeInterval t); 

@implementation BETweeningFunction

+ (BETweeningFunctionPtr)functionOfType:(BETweeningFunctionType)type
{
    switch (type) {
        case BETweeningFunctionLinear:
            return &BETweeningFunctionPtrLinear;
        // Quad
        case BETweeningFunctionQuadEaseIn:
            return &BETweeningFunctionPtrQuadEaseIn;
        case BETweeningFunctionQuadEaseOut:
            return &BETweeningFunctionPtrQuadEaseOut;
        case BETweeningFunctionQuadEaseInOut:
            return &BETweeningFunctionPtrQuadEaseInOut;
        case BETweeningFunctionQuadEaseOutIn:
            return &BETweeningFunctionPtrQuadEaseOutIn;
        // Cubic
        case BETweeningFunctionCubicEaseIn:
            return &BETweeningFunctionPtrCubicEaseIn;
        case BETweeningFunctionCubicEaseOut:
            return &BETweeningFunctionPtrCubicEaseOut;
        case BETweeningFunctionCubicEaseInOut:
            return &BETweeningFunctionPtrCubicEaseInOut;
        case BETweeningFunctionCubicEaseOutIn:
            return &BETweeningFunctionPtrCubicEaseOutIn;
        // Quartic
        case BETweeningFunctionQuarticEaseIn:
            return &BETweeningFunctionPtrQuarticEaseIn;
        case BETweeningFunctionQuarticEaseOut:
            return &BETweeningFunctionPtrQuarticEaseOut;
        case BETweeningFunctionQuarticEaseInOut:
            return &BETweeningFunctionPtrQuarticEaseInOut;
        case BETweeningFunctionQuarticEaseOutIn:
            return &BETweeningFunctionPtrQuarticEaseOutIn;
        // Quintic
        case BETweeningFunctionQuinticEaseIn:
            return &BETweeningFunctionPtrQuinticEaseIn;
        case BETweeningFunctionQuinticEaseOut:
            return &BETweeningFunctionPtrQuinticEaseOut;
        case BETweeningFunctionQuinticEaseInOut:
            return &BETweeningFunctionPtrQuinticEaseInOut;
        case BETweeningFunctionQuinticEaseOutIn:
            return &BETweeningFunctionPtrQuinticEaseOutIn;
        // Sinusoidal
        case BETweeningFunctionSinusoidalEaseIn:
            return &BETweeningFunctionPtrSinusoidalEaseIn;
        case BETweeningFunctionSinusoidalEaseOut:
            return &BETweeningFunctionPtrSinusoidalEaseOut;
        case BETweeningFunctionSinusoidalEaseInOut:
            return &BETweeningFunctionPtrSinusoidalEaseInOut;
        case BETweeningFunctionSinusoidalEaseOutIn:
            return &BETweeningFunctionPtrSinusoidalEaseOutIn;
        // Exponential
        case BETweeningFunctionExponentialEaseIn:
            return &BETweeningFunctionPtrExponentialEaseIn;
        case BETweeningFunctionExponentialEaseOut:
            return &BETweeningFunctionPtrExponentialEaseOut;
        case BETweeningFunctionExponentialEaseInOut:
            return &BETweeningFunctionPtrExponentialEaseInOut;
        case BETweeningFunctionExponentialEaseOutIn:
            return &BETweeningFunctionPtrExponentialEaseOutIn;
        // Circular
        case BETweeningFunctionCircularEaseIn:
            return &BETweeningFunctionPtrCircularEaseIn;
        case BETweeningFunctionCircularEaseOut:
            return &BETweeningFunctionPtrCircularEaseOut;
        case BETweeningFunctionCircularEaseInOut:
            return &BETweeningFunctionPtrCircularEaseInOut;
        case BETweeningFunctionCircularEaseOutIn:
            return &BETweeningFunctionPtrCircularEaseOutIn;
        // Elastic
        case BETweeningFunctionElasticEaseIn:
            return &BETweeningFunctionPtrElasticEaseIn;
        case BETweeningFunctionElasticEaseOut:
            return &BETweeningFunctionPtrElasticEaseOut;
        case BETweeningFunctionElasticEaseInOut:
            return &BETweeningFunctionPtrElasticEaseInOut;
        case BETweeningFunctionElasticEaseOutIn:
            return &BETweeningFunctionPtrElasticEaseOutIn;
        // Back
        case BETweeningFunctionBackEaseIn:
            return &BETweeningFunctionPtrBackEaseIn;
        case BETweeningFunctionBackEaseOut:
            return &BETweeningFunctionPtrBackEaseOut;
        case BETweeningFunctionBackEaseInOut:
            return &BETweeningFunctionPtrBackEaseInOut;
        case BETweeningFunctionBackEaseOutIn:
            return &BETweeningFunctionPtrBackEaseOutIn;
        // Bounce
        case BETweeningFunctionBounceEaseIn:
            return &BETweeningFunctionPtrBounceEaseIn;
        case BETweeningFunctionBounceEaseOut:
            return &BETweeningFunctionPtrBounceEaseOut;
        case BETweeningFunctionBounceEaseInOut:
            return &BETweeningFunctionPtrBounceEaseInOut;
        case BETweeningFunctionBounceEaseOutIn:
            return &BETweeningFunctionPtrBounceEaseOutIn;
        default:
            return &BETweeningFunctionPtrLinear;
    }
}

@end

#pragma mark - Implementations

#pragma mark Linear

NSTimeInterval BETweeningFunctionPtrLinear(NSTimeInterval t)
{
    return t;
}

#pragma mark Quad

NSTimeInterval BETweeningFunctionPtrQuadEaseIn(NSTimeInterval t)
{
    return t * t;
}

NSTimeInterval BETweeningFunctionPtrQuadEaseOut(NSTimeInterval t)
{
    return -(t * (t - 2));
}

NSTimeInterval BETweeningFunctionPtrQuadEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuadEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuadEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrQuadEaseOutIn(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuadEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuadEaseIn(t);
        return (t / 2) + 0.5;
    }
}

#pragma mark Cubic

NSTimeInterval BETweeningFunctionPtrCubicEaseIn(NSTimeInterval t)
{
    return t * t * t;
}

NSTimeInterval BETweeningFunctionPtrCubicEaseOut(NSTimeInterval t)
{
    t = t - 1;
    return (t * t * t) + 1;
}

NSTimeInterval BETweeningFunctionPtrCubicEaseInOut(NSTimeInterval t)
{                   
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrCubicEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrCubicEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrCubicEaseOutIn(NSTimeInterval t)
{ 
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrCubicEaseOut(t);
        return  t / 2;
    }
    else  
    {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrCubicEaseIn(t);
        return  (t / 2) + 0.5;
    }          
}

#pragma mark Quartic

NSTimeInterval BETweeningFunctionPtrQuarticEaseIn(NSTimeInterval t)
{
    return t * t * t * t;
}

NSTimeInterval BETweeningFunctionPtrQuarticEaseOut(NSTimeInterval t)
{
    t = t - 1; 
    t = (t * t * t * t) - 1; 
    return  t * -1;
}

NSTimeInterval BETweeningFunctionPtrQuarticEaseInOut(NSTimeInterval t)
{                   
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuarticEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuarticEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrQuarticEaseOutIn(NSTimeInterval t)
{ 
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuarticEaseOut(t);
        return  t / 2;
    }
    else  
    {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuarticEaseIn(t);
        return  (t / 2) + 0.5;
    }      
}

#pragma mark Quintyic

NSTimeInterval BETweeningFunctionPtrQuinticEaseIn(NSTimeInterval t) 
{
    return t * t * t * t * t;
}

NSTimeInterval BETweeningFunctionPtrQuinticEaseOut(NSTimeInterval t)
{
    t = t - 1; 
    t = t * t * t * t * t; 
    return t + 1;
}             

NSTimeInterval BETweeningFunctionPtrQuinticEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuinticEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuinticEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrQuinticEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrQuinticEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrQuinticEaseIn(t);
        return (t / 2) + 0.5;
    }
}

#pragma mark Sinusoidal

NSTimeInterval BETweeningFunctionPtrSinusoidalEaseIn(NSTimeInterval t)
{
    return -1 * cosf(t * (M_PI / 2)) + 1;
}

NSTimeInterval BETweeningFunctionPtrSinusoidalEaseOut(NSTimeInterval t)
{
    t = sinf(t * (M_PI / 2));
    return t;           
}

NSTimeInterval BETweeningFunctionPtrSinusoidalEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrSinusoidalEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrSinusoidalEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrSinusoidalEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrSinusoidalEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrSinusoidalEaseIn(t);
        return (t / 2) + 0.5;
    }
}

#pragma mark Exponential

NSTimeInterval BETweeningFunctionPtrExponentialEaseIn(NSTimeInterval t)
{        
    return powf(2, 10 * (t - 1)) - 0.001;
}

NSTimeInterval BETweeningFunctionPtrExponentialEaseOut(NSTimeInterval t)
{
    return 1.001 * (-powf(2, -10 * t) + 1);
}

NSTimeInterval BETweeningFunctionPtrExponentialEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrExponentialEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrExponentialEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrExponentialEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrExponentialEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrExponentialEaseIn(t);
        return (t / 2) + 0.5;
    }
}

#pragma mark Circular

NSTimeInterval BETweeningFunctionPtrCircularEaseIn(NSTimeInterval t)
{
    return -1 * (sqrt(1 - (t * t)) - 1);
}

NSTimeInterval BETweeningFunctionPtrCircularEaseOut(NSTimeInterval t)
{
    return sqrt(1 - (t - 1) * (t - 1));
}

NSTimeInterval BETweeningFunctionPtrCircularEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrCircularEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrCircularEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrCircularEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrCircularEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrCircularEaseIn(t);
        return (t / 2) + 0.5;
    }
}          

#pragma mark Elastic

NSTimeInterval BETweeningFunctionPtrElasticEaseIn(NSTimeInterval t)
{
    return (-1 * powf(2, 10 * (t - 1)) * sinf(((t - 1) - 0.075) * (2 * M_PI) / 0.3));
}

NSTimeInterval BETweeningFunctionPtrElasticEaseOut(NSTimeInterval t)
{
    return 1 * powf(2, -10 * t) * sinf((t - 0.075) * (2 * M_PI) / 0.3) + 1;
}

NSTimeInterval BETweeningFunctionPtrElasticEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrElasticEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrElasticEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrElasticEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrElasticEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrElasticEaseIn(t);
        return (t / 2) + 0.5;
    }
}         

#pragma mark Back

NSTimeInterval BETweeningFunctionPtrBackEaseIn(NSTimeInterval t)
{
    return t * t * ((1.7016 + 1) * t - 1.7016);
}

NSTimeInterval BETweeningFunctionPtrBackEaseOut(NSTimeInterval t)
{
    return (t - 1) * (t - 1) * ((1.7016 + 1) * (t - 1) + 1.7016) + 1;
}

NSTimeInterval BETweeningFunctionPtrBackEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrBackEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrBackEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrBackEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrBackEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrBackEaseIn(t);
        return (t / 2) + 0.5;
    }
}   

#pragma mark Bounce

NSTimeInterval BETweeningFunctionPtrBounceEaseIn(NSTimeInterval t)
{
    t = 1 - t;
    if (t < 1 / 2.75)
        t = 7.5625 * t * t;
    else if (t < 2 / 2.75) {
        t = t - (1.5 / 2.75);
        t = 7.5625 * t * t + 0.75;
    }
    else if (t < 2.5 / 2.75) {
        t = t - (2.25 / 2.75);
        t = 7.5625 * t * t + 0.9375;
    } else {
        t = t - (2.625 / 2.75);
        t = 7.5625 * t * t + 0.984375;
    }
    return 1 - t;
}

NSTimeInterval BETweeningFunctionPtrBounceEaseOut(NSTimeInterval t)
{
    if (t < 1/2.75) {
        t = 7.5625 * t * t;
    } else if (t < 2/2.75) {
        t = t - (1.5 / 2.75);
        t = 7.5625 * t * t + 0.75;
    } else if (t < 2.5/2.75) {
        t = t - (2.25 / 2.75);
        t = 7.5625 * t * t + 0.9375;
    } else {
        t = t - (2.625 / 2.75);
        t = 7.5625 * t * t + 0.984375;
    }
    return t;
}

NSTimeInterval BETweeningFunctionPtrBounceEaseInOut(NSTimeInterval t)
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrBounceEaseIn(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrBounceEaseOut(t);
        return (t / 2) + 0.5;
    }
}

NSTimeInterval BETweeningFunctionPtrBounceEaseOutIn(NSTimeInterval t) 
{
    if (t <= 0.5) {
        t = t * 2;
        t = BETweeningFunctionPtrBounceEaseOut(t);
        return t / 2;
    } else {
        t = (t - 0.5) * 2;
        t = BETweeningFunctionPtrBounceEaseIn(t);
        return (t / 2) + 0.5;
    }
}