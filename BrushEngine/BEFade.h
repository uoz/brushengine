//
//  BEFade.h
//  BrushEngine
//
//  Created by Germano Guerrini on 11/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

/** Base class that defines a transformation of the alpha parameter of a BESprite instance.
 A BEFade object with a duration of 0 will result in a instant change of the transparency of the target object
 rather than a smooth transition.
 */
@interface BEFade : BEAction
- (id)initWithTarget:(BEGameObject *)target alpha:(float)alpha duration:(NSTimeInterval)duration;
- (id)initWithTarget:(BEGameObject *)target alpha:(float)alpha;
@end

/** An action that performs a variation of a BESprite instance from its current alpha value to full opacity.
 */
@interface BEFadeIn : BEFade
- (id)initWithTarget:(BEGameObject *)target duration:(NSTimeInterval)duration;
@end

/** An action that performs a variation of a BESprite instance from its current alpha value to full transparency.
 */
@interface BEFadeOut : BEFade
- (id)initWithTarget:(BEGameObject *)target duration:(NSTimeInterval)duration;
@end

