//
//  BEDrawingKit.h
//  BrushEngine
//
//  Created by Germano Guerrini on 04/06/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#ifndef __BE_DRAWING_KIT_H
#define __BE_DRAWING_KIT_H

#include "BETypes.h"

#ifdef __cplusplus
extern "C" {
#endif

    inline void BEDrawLine(GLKVector2 start, GLKVector2 end);
    inline void BEDrawQuad(BETexturedColoredQuad quad);
    inline void BEDrawRect(CGRect rect);
    inline void BEDrawLineWithColor(GLKVector2 start, GLKVector2 end, GLKVector4 color);
    inline void BEDrawQuadWithColor(BETexturedColoredQuad quad, GLKVector4 color);
    inline void BEDrawRectWithColor(CGRect rect, GLKVector4 color);

#ifdef __cplusplus
}
#endif

#endif
