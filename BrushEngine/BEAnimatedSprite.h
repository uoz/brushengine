//
//  BEAnimatedSprite.h
//  BrushEngine
//
//  Created by Germano Guerrini on 13/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//
#import "BESprite.h"

@class BEAnimation;
@class BEAnimationFrame;
@protocol BEAction;

@interface BEAnimatedSprite : BESprite

@property (nonatomic, weak) BEAnimation *currentAnimation;

- (id)initWithAnimations:(NSDictionary *)animations setCurrent:(BEAnimation *)animation;
- (id)initWithAnimation:(BEAnimation *)animation;

- (void)addAnimation:(BEAnimation *)animation;

- (BEAnimation *)animationWithKey:(NSString *)key;

- (void)updateToCurrentAnimationFrame;

- (id<BEAction>)playAnimationWithKey:(NSString *)key;

@end
