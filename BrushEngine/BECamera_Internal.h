//
//  BECamera_Internal.h
//  BrushEngine
//
//  Created by Germano Guerrini on 27/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BECamera.h"

@interface BECamera (Internal)

@property (nonatomic, readonly) CGRect innerFrame;
@property (nonatomic, readonly) CGRect outerFrame;

@end
