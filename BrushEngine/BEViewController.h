//
//  BEViewController.h
//  BrushEngine
//
//  Created by Germano Guerrini on 26/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BERenderingManager.h"
#import "BEActionManager.h"

@interface BEViewController : GLKViewController

@property (nonatomic, weak, readonly) BERenderingManager *renderingManager;
@property (nonatomic, weak, readonly) BEActionManager *actionManager;
@property (nonatomic, readonly) float currentFramesPerSecond;

@end
