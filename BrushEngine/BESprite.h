//
//  BESprite.h
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEGameObject.h"
#import "BETypes.h"

@class BETextureAtlas;
@class BESpriteBatch;

@interface BESprite : BEGameObject

@property (nonatomic, strong) GLKBaseEffect *effect;
@property (nonatomic, strong) GLKTextureInfo *texture;
@property (nonatomic) CGRect textureCoordinates;
@property (nonatomic) GLKVector4 color;
@property (nonatomic) float alpha;
@property (nonatomic, readonly) BETexturedColoredQuad quad;
@property (nonatomic, weak) BESpriteBatch *batch;

- (id)initWithTexture:(GLKTextureInfo *)texture textureCoordinates:(CGRect)textureCoordinates;
- (id)initWithTexture:(GLKTextureInfo *)texture;
- (id)initWithContentsOfFile:(NSString *)fileName textureCoordinates:(CGRect)textureCoordinates;
- (id)initWithContentsOfFile:(NSString *)fileName;
- (id)initWithTextureAtlas:(BETextureAtlas *)textureAtlas key:(NSString *)key;
- (id)initWithBatch:(BESpriteBatch *)batch key:(NSString *)key;

@end
