//
//  BEDictionaryAddictions.m
//  BrushEngine
//
//  Created by Germano Guerrini on 17/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEDictionaryAddictions.h"

@implementation NSDictionary (NSDictionaryAddictions)

- (int)intValueFromObjectForKey:(NSString *)key
{
    NSNumber *value = [self objectForKey:key];
    return [value intValue];
}

- (float)floatValueFromObjectForKey:(NSString *)key
{
    NSNumber *value = [self objectForKey:key];
    return [value floatValue];
}

@end
