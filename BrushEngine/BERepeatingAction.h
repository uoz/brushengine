//
//  BERepeatingAction.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

/** Container class that allows the repetition of a BEAction object.
 
 An action can be repeated for a finite or an infinite number of times. In the latter case, it won't be sequenceable.
 
 @see BEChoreography 
 */
@interface BERepeatingAction : NSObject <BEAction, BEActionDelegate>

/** Initializes an action that will be repeated for the given number of times. */
- (id)initWithAction:(id<BEAction>)action repeatCount:(NSUInteger)count;

/** Initializes an action that will be repeated forever. */
- (id)initWithAction:(id<BEAction>)action;

@end
