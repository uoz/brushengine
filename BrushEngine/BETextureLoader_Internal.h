//
//  BETextureLoader_Internal.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BETextureLoader.h"

@interface BETextureLoader (Internal)

+ (NSUInteger)loadedTextureCount;

@end
