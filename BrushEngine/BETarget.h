//
//  BETarget.h
//  BrushEngine
//
//  Created by Germano Guerrini on 21/12/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BETarget : NSObject

/** The unique target identifier. */
@property (nonatomic, readonly) int targetID;

/** Reset the current targetID to 0. */
+ (void)reset;

@end
