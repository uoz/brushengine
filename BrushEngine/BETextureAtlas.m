//
//  BETextureAtlas.m
//  BrushEngine
//
//  Created by Germano Guerrini on 08/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BETextureAtlas.h"
#import "BETextureLoader.h"

@interface BETextureAtlas ()

@property (nonatomic, readonly) NSDictionary *subTextures;

@end

@implementation BETextureAtlas

- (id)initWithTexture:(GLKTextureInfo *)texture controlFile:(NSString *)controlFile
{
    if ((self = [super init])) {
        _texture = texture;
        [self parseControlFile:controlFile];
    }
    return self;
}

- (id)initWithContentsOfFile:(NSString *)fileName controlFile:(NSString *)controlFile;
{
    GLKTextureInfo *texture = [BETextureLoader textureWithContentsOfFile:fileName];
    return [self initWithTexture:texture controlFile:controlFile];
}

- (BOOL)containsKey:(NSString *)key
{
    return [_subTextures objectForKey:key] != nil;
}

- (CGRect)coordsForKey:(NSString *)key
{
    NSDictionary *subTexture = [_subTextures objectForKey:key];
    if (subTexture) {
        return CGRectFromString([subTexture objectForKey:@"textureRect"]);
    }
    NSLog(@"subTexture not found: '%@'", key); // TODO Handle it
    return CGRectZero;
}

- (CGSize)sizeForKey:(NSString *)key
{
    NSDictionary *subTexture = [_subTextures objectForKey:key];
    if (subTexture) {
        return CGSizeFromString([subTexture objectForKey:@"spriteSize"]);
    }
    NSLog(@"subTexture not found: '%@'", key); // TODO Handle it
    return CGSizeZero;
}

- (void)parseControlFile:(NSString *)controlFile
{
    NSDictionary *properties = [NSDictionary dictionaryWithContentsOfFile:controlFile];
    _subTextures = [NSDictionary dictionaryWithDictionary:[properties objectForKey:@"frames"]];
}

@end
