//
//  BEAnimationFrame.h
//  BrushEngine
//
//  Created by Germano Guerrini on 12/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * A lightweight object used to model a single frame of a sprite animation.
 */
@interface BEAnimationFrame : NSObject

@property (nonatomic, readonly, strong) NSString *key;
@property (nonatomic, readonly) NSTimeInterval offset;

- (id)initWithKey:(NSString *)key offset:(NSTimeInterval)offset;

+ (id)animationFrameWithKey:(NSString *)key offset:(NSTimeInterval)offset;

@end
