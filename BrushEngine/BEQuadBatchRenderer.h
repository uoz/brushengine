//
//  BEQuadBatchRenderer.h
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEGameObject.h"
#import "BETypes.h"

/** Low level class that handles the rendering of multiple quads.
 
 A batch renderer draws in a single OpenGL call an array of quads that share the same texture and
 blending options.
 Once instantiated, it manages an interleaved vertex array of BETexturedColoredQuad objects, submitting
 to OpenGL only the currently active ones, that are kept at the beginning of the array to optimize
 performance.
 */
@interface BEQuadBatchRenderer : BEGameObject
{
    /** The capacity of the batch renderer. It is not possible to add more object than that. */
    NSUInteger _capacity;
    
    /** Current number of active quads. It is used to submit only a portion of the IVA to OpenGL. */
    NSUInteger _activeQuadsCount;
    
    /** The texture used by the batch renderer. */
    GLKTextureInfo *_texture;
    
    /** The GLKit effect used by the batch renderer. */
    GLKBaseEffect *_effect;
    
    /** The interleaved vertex array of quads. */
    BETexturedColoredQuad *_quads;
    
    /** The array of indices use to submit the glDrawElements command. */
    GLushort *_indices;
    
    /** The blending source constant. */
    int _blendFuncSource;
    
    /** The blending destination constant. */
    int _blendFuncDestination;
}
@property (nonatomic, readonly) GLKTextureInfo *texture;

/** Initializes a batch render with a capacity and a texture. */
- (id)initWithCapacity:(NSUInteger)capacity texture:(GLKTextureInfo *)texture;

/** Initializes the interleaved vertex array. Useful to set some static quads parameters. */
- (void)initQuads;

/** Adds an active quad after the last currently active quad into the interleaved vertex array. */
- (int)addQuad:(BETexturedColoredQuad)quad;

/** Replaces the quad at the specified index of the interleaved vertex array with a new one. */
- (void)replaceQuadAtIndex:(NSUInteger)index withQuad:(BETexturedColoredQuad)quad;

/** Disables a quad of the interleaved vertex array.
 *
 * It throws an NSRangeException if the index is out of range and an assertion if the IVA
 * is currently empty.
 * It does nothing if the quad is already disabled.
 */
- (void)disableQuadAtIndex:(NSUInteger)index;


/** Disables all quads at once. */
- (void)disableAllQuads;

@end
