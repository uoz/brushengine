//
//  BEQuadBatchRenderer.m
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEQuadBatchRenderer.h"
#import "BEQuadBatchRenderer_Internal.h"
#import "BEOpenGLStateCache.h"
#import "BESettings.h"
#import "BEEffectLoader.h"
#import "BEProfiler.h"
#import "BEUtils.h"

@interface BEQuadBatchRenderer ()

@property (nonatomic, readonly) NSUInteger capacity;
@property (nonatomic, readonly) GLuint vertexBufferObject;
@property (nonatomic, readonly) GLuint indexBufferObject;
@property (nonatomic, readonly) GLuint vertexArrayObject;

- (void)initIndices;
- (void)initVertexArrayObject;

@end

@implementation BEQuadBatchRenderer

- (id)initWithCapacity:(NSUInteger)capacity texture:(GLKTextureInfo *)texture
{
    if ((self = [super init])) {
        _capacity = capacity;
        _activeQuadsCount = 0;
        
        _quads = calloc(_capacity, sizeof(BETexturedColoredQuad));
        _indices = calloc(_capacity * 6, sizeof(GLushort));
        
        [self initQuads];
        [self initIndices];
        [self initVertexArrayObject];
        
        _texture = texture;
        
        _effect = [BEEffectLoader effectWithKey:BE_DEFAULT_EFFECT];
        _effect.transform.projectionMatrix = self.renderingManager.projectionMatrix; // TODO Move it to draw?
        _effect.texture2d0.envMode = GLKTextureEnvModeModulate; /* TODO Experiment to see how expensive it is. */
        
        _blendFuncSource = BE_DEFAULT_BLEND_SRC;
        _blendFuncDestination = BE_DEFAULT_BLEND_DST;
    }
    return self;
}

- (void)initQuads
{
    // Hook for subclasses
}

- (int)addQuad:(BETexturedColoredQuad)quad
{
    if (_activeQuadsCount < _capacity) {
        memcpy(_quads + _activeQuadsCount, quad.vertices, sizeof(BETexturedColoredQuad));
        return _activeQuadsCount++;
    }
    BEDebugLog(@"Maximum batch renderer capacity hit.");
    return -1; // TODO Make constant
}

- (void)replaceQuadAtIndex:(NSUInteger)index withQuad:(BETexturedColoredQuad)quad
{
    if (index < _capacity) {
        memcpy(_quads + index, quad.vertices, sizeof(BETexturedColoredQuad));
    } else {
        [[NSException exceptionWithName:NSRangeException reason:@"Index out of batch range" userInfo:nil] raise];
    }
}

- (void)disableQuadAtIndex:(NSUInteger)index
{
    if (index < _activeQuadsCount) {
        _quads[index] = _quads[_activeQuadsCount--];
    } else if (index < _capacity) {
        BEDebugLog(@"Disabling an already inactive quad");
    } else {
        [[NSException exceptionWithName:NSRangeException reason:@"Index out of batch range" userInfo:nil] raise];
    }
}

- (void)disableAllQuads
{
    _activeQuadsCount = 0;
}

- (void)dealloc
{
    free(_quads);
    free(_indices);
}

#pragma mark - BERendering Protocol Methods

- (void)draw
{
    if (_activeQuadsCount == 0) {
        return;
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(BETexturedColoredQuad) * _activeQuadsCount, _quads);
    glBindBuffer(GL_ARRAY_BUFFER, 0);    
    
    _effect.texture2d0.name = _texture.name;
    _effect.transform.modelviewMatrix = GLKMatrix4Identity;
    [_effect prepareToDraw];
    
    glBindVertexArrayOES(_vertexArrayObject);
    BEglBlendFunc(_blendFuncSource, _blendFuncDestination);
    glDrawElements(GL_TRIANGLES, _activeQuadsCount * 6, GL_UNSIGNED_SHORT, (void *)0);
    glBindVertexArrayOES(0);

#ifdef BE_PROFILER_ENABLED
    [[BEProfiler sharedProfiler] incrementTextureSwitchCount];
#endif
}

#pragma mark - Private Methods

- (void)initIndices
{
    for (int i = 0; i < _capacity; ++i) {
        _indices[i * 6 + 0] = i * 4 + 0;
        _indices[i * 6 + 1] = i * 4 + 1;
        _indices[i * 6 + 2] = i * 4 + 2;
        _indices[i * 6 + 3] = i * 4 + 2;
        _indices[i * 6 + 4] = i * 4 + 3;
        _indices[i * 6 + 5] = i * 4 + 1;
    }
}

- (void)initVertexArrayObject
{
    glGenVertexArraysOES(1, &_vertexArrayObject);
    glBindVertexArrayOES(_vertexArrayObject);
    
    glGenBuffers(1, &_vertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(BETexturedColoredQuad) * _capacity, _quads, GL_DYNAMIC_DRAW);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, position));
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, textureCoords));
    
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, color));
    
    glGenBuffers(1, &_indexBufferObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * _capacity * 6, _indices, GL_STATIC_DRAW);
    
    glBindVertexArrayOES(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);   
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

- (BETexturedColoredQuad)quadAtIndex:(NSUInteger)index
{
    
    if (index >= _capacity) {
        [[NSException exceptionWithName:NSRangeException reason:@"Index out of batch range" userInfo:nil] raise];
    }
    return _quads[index];
}

@end