//
//  BEEffectLoader.m
//  BrushEngine
//
//  Created by Germano Guerrini on 07/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEEffectLoader.h"
#import "BESettings.h"

static NSMutableDictionary *_cache;

@implementation BEEffectLoader

+ (void)initCache
{
    _cache = [NSMutableDictionary dictionaryWithCapacity:BE_DEFAULT_EFFECT_CACHE_SIZE];
}

+ (void)initialize
{
    if (self == [BEEffectLoader class]) {
        [self initCache];
    }
}

+ (GLKBaseEffect *)effectWithKey:(NSString *)key
{
    GLKBaseEffect *effect;
    
    // Check if cache is initialized (might have been niled)
    if (_cache == nil) {
        [self initCache];
    }
    
    // Check if we already loaded this effect
    if ((effect = [_cache objectForKey:key])) {
        return effect;
    }
    
    /* TODO Handle other effects, maybe with a dictionary. */
    effect = [[GLKBaseEffect alloc] init];
    effect.label = key;
    [_cache setObject:effect forKey:key];
    return effect;
}

+ (void)purgeCachedEffects
{
    _cache = nil;
}

@end
