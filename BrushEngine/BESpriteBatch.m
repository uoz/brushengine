//
//  BESpriteBatch.m
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BESpriteBatch.h"
#import "BEUtils.h"
#import "BESprite.h"
#import "BETextureAtlas.h"


@interface BESpriteBatch ()

@property (nonatomic, strong) NSMutableArray *descendants;

@end

@implementation BESpriteBatch

- (id)initWithCapacity:(NSUInteger)capacity textureAtlas:(BETextureAtlas *)textureAtlas
{
    if ((self = [super initWithCapacity:capacity texture:textureAtlas.texture])) {
        _textureAtlas = textureAtlas;
        _descendants = [NSMutableArray arrayWithCapacity:capacity];
    }
    return self;
}

- (void)addChild:(BESprite *)child
{
    [self addDescendant:child];
    [super addChild:(BEGameObject *)child];
}

- (void)removeChild:(BESprite *)child
{
    NSUInteger index = [self.children indexOfObject:child];
    if (index != NSNotFound) {
        [self disableQuadAtIndex:index];
        [super removeChild:(BEGameObject *)child]; // TODO mmm
        // TODO [self removeSpriteFromBatch:aChild];
    }
}

- (void)updateTransform
{
    // We, as a node, can be transformed as well
    [super updateTransform];
    
    GLKVector3 *positions = calloc(4, sizeof(GLKVector3));
    
    // As we cannot transform every single sprite through the shader, we do that on the CPU
    [self.descendants enumerateObjectsUsingBlock:^(BESprite *descendant, NSUInteger idx, BOOL *stop) {

        // First, update the modelViewMatrix of the current sprite
        [descendant updateTransform];
                
        // To reuse GLKit optimized multiplication, we pretend we have 3D points
        for (size_t i = 0; i < 4; i++) {
            positions[i] = GLKVector3MakeWithVector2(descendant.quad.vertices[i].position, 0.0f);
        }
        GLKMatrix4MultiplyVector3ArrayWithTranslation(descendant.modelViewMatrix, positions, 4);
        
        // Lastly, update the interleaved vertex array element for the current sprite
        for (size_t i = 0; i < 4; i++) {
            _quads[idx].vertices[i].position      = GLKVector2Make(positions[i].x, positions[i].y);
            _quads[idx].vertices[i].color         = descendant.quad.vertices[i].color;
            _quads[idx].vertices[i].textureCoords = descendant.quad.vertices[i].textureCoords;
        }        
    }];
    free(positions), positions = NULL;
}

- (void)render
{
    // If an object is not visible, neither will its children
    if (!self.isVisible) {
        return;
    }
    
    GLKMatrixStackPush(self.renderingManager.matrixStack);
    
    [self updateTransform];
    [self draw];
    // Don't call render on children
    
    GLKMatrixStackPop(self.renderingManager.matrixStack);
}

- (void)addDescendant:(BESprite *)descendant
{
    NSAssert([descendant isKindOfClass:[BESprite class]],
             @"Only BESprite instances can be added to a BESpriteBatch.");
    NSAssert(descendant.texture.name == _textureAtlas.texture.name,
             @"Cannot add a BESprite with a different texture.");
    if ([self addQuad:descendant.quad] != -1) { // TODO Make a constant
        [_descendants addObject:descendant];
    }
}

@end
