//
//  BEChoreography.h
//  BrushEngine
//
//  Created by Germano Guerrini on 26/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

typedef enum {
    BEChoreographyTypeConcurrent = 0,
    BEChoreographyTypeSequence
} BEChoreographyType;

/** Container class that allows the composition of multiple objects that conform to BEAction protocol.
 
 There are two different types of composition:
 
 - *Concurrent:* all actions will be started at once. The duration of the longest action will be the overall duration. This type is specified by `BEChoreographyTypeConcurrent` constant.
 - *Sequence:* actions will be performed in the provided order. The overall duration will be the sum of the durations of the actions. This type is specified by `BEChoreographyTypeSequence` constant.
 
 @warning *Important:* Adding a BERepeatingAction with an infinite number of repetitions to a `BEChoreographyTypeSequence` choreography is not advisable. 
 Also, given that each object conforming to BEAction protocol has a single delegate, adding the same object at different level of
 the choreography may lead to unexpected behaviour.
 */
@interface BEChoreography : NSObject <BEAction, BEActionDelegate>

/** Initializes a choreography of the given type with the provided array of actions. */
- (id)initWithType:(BEChoreographyType)type actionsArray:(NSArray *)actions;

/** Initializes a choreography of the given type with the actions in the argument list. */
- (id)initWithType:(BEChoreographyType)type actions:(id<BEAction>)firstAction, ... NS_REQUIRES_NIL_TERMINATION;

@end
