//
//  BEAnimation.h
//  BrushEngine
//
//  Created by Germano Guerrini on 11/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@class BETextureAtlas;
@class BEAnimationFrame;

@interface BEAnimation : NSObject

@property (nonatomic, readonly) NSString *key;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSUInteger frameCount;
@property (nonatomic, strong) BETextureAtlas *atlas;
@property (nonatomic, strong) BEAnimationFrame *currentFrame;
@property (nonatomic, readonly) CGRect textureCoordinatesForCurrentFrame;
@property (nonatomic, readonly) CGSize sizeForCurrentFrame;

//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas frames:(NSArray *)frames;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range delay:(NSTimeInterval)delay options:(BEAnimationOptions)options;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range delay:(NSTimeInterval)delay;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range frequency:(NSUInteger)frequency options:(BEAnimationOptions)options;
- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range frequency:(NSUInteger)frequency;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range duration:(NSTimeInterval)duration options:(BEAnimationOptions)options;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range duration:(NSTimeInterval)duration;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys delay:(NSTimeInterval)delay options:(BEAnimationOptions)options;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys delay:(NSTimeInterval)delay;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys frequency:(NSUInteger)frequency options:(BEAnimationOptions)options;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys frequency:(NSUInteger)frequency;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys duration:(NSTimeInterval)duration options:(BEAnimationOptions)options;
//- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keys:(NSArray *)keys duration:(NSTimeInterval)duration;
- (BEAnimationFrame *)frameAtIndex:(NSUInteger)index;

@end
