//
//  BETarget.m
//  BrushEngine
//
//  Created by Germano Guerrini on 21/12/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BETarget.h"

@implementation BETarget

static int currentTargetID = 0;

+ (int)nextTargetID
{
    return currentTargetID++;
}

- (id)init
{
    if ((self = [super init])) {
        _targetID = [BETarget nextTargetID];
    }
    return self;
}

+ (void)reset
{
    currentTargetID = 0;
}

@end
