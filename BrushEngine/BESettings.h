//
//  BESettings.h
//  BrushEngine
//
//  Created by Germano Guerrini on 07/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#ifndef __BE_SETTINGS_H
#define __BE_SETTINGS_H

#define BE_PREFERRED_FRAME_RATE 60

#define BE_DEFAULT_EFFECT_CACHE_SIZE 4
#define BE_DEFAULT_EFFECT @"baseEffect"

#define BE_DEFAULT_TEXTURE_CACHE_SIZE 16

// #define BE_PROFILER_ENABLED
#define BE_PROFILER_COMPLETE 1
#define BE_PROFILER_TICK 1.0f

#define BE_ENABLE_GL_STATE_CACHING 1
#define BE_DEFAULT_BLEND_SRC GL_SRC_ALPHA
#define BE_DEFAULT_BLEND_DST GL_ONE_MINUS_SRC_ALPHA

#define BE_DRAW_CAMERA_DEBUG

#endif /* __BE_SETTINGS_H */
