//
//  BELayer.h
//  BrushEngine
//
//  Created by Germano Guerrini on 01/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEGameObject.h"

typedef enum {
    BELayerDepthBackground = 0,
    BELayerDepthBackgroundParticles = 1,
    BELayerDepthStaticEntities = 2,
    BELayerDepthStaticEntitiesParticles = 3,
    BELayerDepthMonsters = 4,
    BELayerDepthMonstersParticles = 5,
    BELayerDepthForeground = 6,
    BELayerDepthForegroundParticles = 7,
    BELayerDepthHUD = 8
} BELayerDepth;

#define BE_DEFAULT_LAYER_COUNT BELayerDepthHUD + 1

@interface BELayer : BEGameObject

@end
