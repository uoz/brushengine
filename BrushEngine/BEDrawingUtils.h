//
//  BEDrawingUtils.h
//  BrushEngine
//
//  Created by Germano Guerrini on 21/05/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BETypes.h"

@interface BEDrawingUtils : NSObject

+ (void)drawQuad:(BETexturedColoredQuad)quad;
+ (void)drawLineFrom:(GLKVector2)start to:(GLKVector2)end;

@end
