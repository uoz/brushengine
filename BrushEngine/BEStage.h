//
//  BEStage.h
//  BrushEngine
//
//  Created by Germano Guerrini on 01/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BERendering.h"
#import "BELayer.h"

@interface BEStage : NSObject <BERendering>
- (void)addLayer:(BELayer *)layer atDepth:(BELayerDepth)depth;
- (void)addObject:(id<BERendering>)object atDepth:(BELayerDepth)depth;
- (BELayer *)layerAtDepth:(BELayerDepth)depth;
@end
