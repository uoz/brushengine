//
//  BETextureLoader.m
//  BrushEngine
//
//  Created by Germano Guerrini on 05/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BETextureLoader.h"
#import "BETextureLoader_Internal.h"
#import "BESettings.h"
#import "BEUtils.h"
#import "BEProfiler.h"

static NSMutableDictionary *cache;

@implementation BETextureLoader

+ (void)initCache
{
    cache = [NSMutableDictionary dictionaryWithCapacity:BE_DEFAULT_TEXTURE_CACHE_SIZE];
}

+ (void)initialize
{
    if (self == [BETextureLoader class]) {
        [self initCache];
    }
}

+ (GLKTextureInfo *)textureWithContentsOfFile:(NSString *)fileName options:(NSDictionary *)options
{
    // Check if cache is initialized (might have been niled)
    if (cache == nil) {
        [self initCache];
    }
    
    // Check if we already loaded this texture
    GLKTextureInfo *texture;
    if ((texture = [cache objectForKey:fileName])) {
        return texture;
    }
    
    // Otherwise load it, save it in the cache and return it
    NSError *error;
    texture = [GLKTextureLoader textureWithContentsOfFile:fileName options:options error:&error];
    if (texture == nil) {
        BEDebugLog(@"Error loading texture: %@", [error localizedDescription]);
    } else {
        [cache setObject:texture forKey:fileName];
#ifdef BE_PROFILER_ENABLED
        [[BEProfiler sharedProfiler] addTextureWithPath:fileName];
#endif
    }    
    return texture;
}

+ (void)deleteTextureWithContentsOfFile:(NSString *)fileName
{
    GLKTextureInfo *texture;
    if ((texture = [cache objectForKey:fileName])) {
        GLuint textureName = texture.name;
        glDeleteTextures(1, &textureName);
        [cache removeObjectForKey:fileName];
    }
}

+ (GLKTextureInfo *)textureWithContentsOfFile:(NSString *)fileName
{
    return [self textureWithContentsOfFile:fileName options:nil];
}

+ (void)purgeCachedTextures
{
    [cache enumerateKeysAndObjectsUsingBlock:^(id key, GLKTextureInfo *texture, BOOL *stop) {
        GLuint textureName = texture.name;
        glDeleteTextures(1, &textureName);
    }];
    cache = nil;
}

@end

#pragma mark - Internal

@implementation BETextureLoader (Internal)

+ (NSUInteger)loadedTextureCount
{
    if (cache != nil) {
        return [cache count];
    }
    return 0;
}

@end
