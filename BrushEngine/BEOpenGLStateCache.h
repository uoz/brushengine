//
//  BEOpenGLStateCache.h
//  BrushEngine
//
//  Created by Germano Guerrini on 02/06/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//
#ifndef __BE_OPENGL_STATE_CACHE_H
#define __BE_OPENGL_STATE_CACHE_H

#import <Foundation/Foundation.h>

#ifdef __cplusplus
extern "C" {
#endif

void BEglBlendFunc(GLenum src, GLenum dst);
void BEsetAlphaBlending(BOOL flag);

#ifdef __cplusplus
}
#endif

#endif /* __BE_OPENGL_STATE_CACHE_H */