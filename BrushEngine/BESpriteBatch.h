//
//  BESpriteBatch.h
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEQuadBatchRenderer.h"

@class BESprite;
@class BETextureAtlas;
// TODO: Fix docs
/** Manages an array of sprites that share the same texture and effect.
 
 Subclassing BEQuadBatchRenderer, it draws all the sprites in a single OpenGL draw command,
 but instead of referencing a simple texture object, it uses a BETextureAtlas.
 
 Typically, a BESpriteBatch has to be initialized with a given capacity and a texture atlas object.
 After that, BESprite objects can be added to the batch by initializing them with the batch and a key
 of the texture atlas:
 
 BETextureAtlas *atlas = [[BETextureAtlas alloc] initWithContentsOfFile:@"atlas.png" controlFile:@"atlas.plist"];
 BESpriteBatch *batch = [[BESpriteBatch alloc] initWithCapacity:128 textureAtlas:atlas];
 [[BESprite alloc] initWithBatch:batch key:@"monster.png"];
 */
@interface BESpriteBatch : BEQuadBatchRenderer

@property (nonatomic, strong) BETextureAtlas *textureAtlas;

/** Initializes a sprite batch with a capacity and a texture atlas. */
- (id)initWithCapacity:(NSUInteger)capacity textureAtlas:(BETextureAtlas *)textureAtlas;

- (void)addDescendant:(BESprite *)descendant;

@end
