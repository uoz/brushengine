//
//  BECameraShake.m
//  BrushEngine
//
//  Created by Germano Guerrini on 20/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BECameraShake.h"
#import "BECamera.h"

#define MAX_DISPLACEMENT 100

@interface BECameraShake ()

@property (nonatomic, readonly) BEShakeType type;
@property (nonatomic, readonly) float magnitude;
@property (nonatomic, readonly) float xComponent;
@property (nonatomic, readonly) float yComponent;

@end

@implementation BECameraShake

- (id)initWithCamera:(BECamera *)camera type:(BEShakeType)type magnitude:(float)magnitude duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:camera duration:duration])) {
        _type = type;
        _magnitude = CLAMP(magnitude, 0.0f, 1.0f);
        _xComponent = 0.0f;
        _yComponent = 0.0f;
    }
    return self;
}

- (void)stop
{
    [super stop];
    ((BECamera *)self.target).shake = GLKVector2Make(0.0f, 0.0f);
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    switch (_type) {
        case BEShakeTypeVertical:
            _xComponent = 0.0f;
            _yComponent = _magnitude * RANDOM_MINUS_1_1 * MAX_DISPLACEMENT;
            break;
        case BEShakeTypeHorizontal:
            _xComponent = _magnitude * RANDOM_MINUS_1_1 * MAX_DISPLACEMENT;
            _yComponent = 0.0f;
            break;
        default:
            _xComponent = _magnitude * RANDOM_MINUS_1_1 * MAX_DISPLACEMENT;
            _yComponent = _magnitude * RANDOM_MINUS_1_1 * MAX_DISPLACEMENT;
    }
    ((BECamera *)self.target).shake = GLKVector2Make(_xComponent, _yComponent);
}

@end
