//
//  BETranslate.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

// --------------------------------------------------------------------------------
// BEScale
// --------------------------------------------------------------------------------

/** Abstract base class that defines a transformation of the position parameter of a `BEGameObject` instance.
 
 A `BETranslate` object with a duration of 0 will result in a "teleportation" of the target object rather than a smooth transition.
 */
@interface BETranslate : BEAction
{
@protected
    GLKVector2 _startPosition;
    GLKVector2 _endPosition;
}

/// --------------------------------------------------------------------------------
/// @name Initialization
/// --------------------------------------------------------------------------------

/** Initializes a new translating action.
 
 This is the designated initializer.
 
 @param target The object the action will be translated.
 @param position The position vector used by the action to calculate the final position.
 @param duration The duration of the action in seconds.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position duration:(NSTimeInterval)duration;

/** Initializes a new instantaneous translating action.
 
 @param target The object the action will be translated. 
 @param position The position vector used by the action to calculate the final position.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target position:(GLKVector2)position;

@end

// --------------------------------------------------------------------------------
// BETranslateTo
// --------------------------------------------------------------------------------

/** An action that performs a translation of a `BEGameObject` instance from its current position to the given position. 
 Repeating this action will not translate the target object any further.
 */
@interface BETranslateTo : BETranslate
@end

// --------------------------------------------------------------------------------
// BETranslateBy
// --------------------------------------------------------------------------------

/** An action that performs a translation of a `BEGameObject` instance from its current position by the given position. 
 Repeating this action will continue to translate the target object.
 */
@interface BETranslateBy : BETranslate
@end
