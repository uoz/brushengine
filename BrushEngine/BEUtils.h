//
//  BEUtils.h
//  BrushEngine
//
//  Created by Germano Guerrini on 06/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#ifndef __BE_UTILS_H
#define __BE_UTILS_H

#define BE_DEBUG_MODE

#ifdef BE_DEBUG_MODE
#define BEDebugLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define BEDebugLog( s, ... ) 
#endif

#define CLAMP(x, min, max)      (((x) > (max)) ? (max) : (((x) < (min)) ? (min) : (x)))

#define DEGREES_TO_RADIANS(x)   ((x) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(x)   ((x) / M_PI * 180.0)

#define ARC4RANDOM_MAX          0x100000000
#define RANDOM_0_1              (((float)arc4random() / ARC4RANDOM_MAX * 1.0f))
#define RANDOM_MINUS_1_1        (((float)arc4random() / ARC4RANDOM_MAX * 2.0f) - 1)

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#endif
