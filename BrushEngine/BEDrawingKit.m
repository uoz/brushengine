//
//  BEDrawingKit.c
//  BrushEngine
//
//  Created by Germano Guerrini on 04/06/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEDrawingKit.h"
#import "BEEffectLoader.h"
#import "BEOpenGLStateCache.h"
#import "BESettings.h"

void BEDrawLineWithColor(GLKVector2 start, GLKVector2 end, GLKVector4 color)
{
    GLKBaseEffect *effect = [BEEffectLoader effectWithKey:BE_DEFAULT_EFFECT];
    GLKVector2 vertices[2] = {
        {start.x, start.y},
        {end.x, end.y}
    };
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, (void *)vertices);
    glDisableVertexAttribArray(GLKVertexAttribColor);
    
    effect.useConstantColor = GL_TRUE;
    effect.constantColor = color;
    [effect prepareToDraw];
    
    BEsetAlphaBlending(NO);
    glDrawArrays(GL_LINES, 0, 2);
    BEsetAlphaBlending(YES);
    
    glDisableVertexAttribArray(GLKVertexAttribPosition);
}

void BEDrawLine(GLKVector2 start, GLKVector2 end)
{
    BEDrawLineWithColor(start, end, GLKVector4Make(0.0f, 1.0f, 0.0f, 1.0f));
}

void BEDrawQuadWithColor(BETexturedColoredQuad quad, GLKVector4 color)
{
    BEDrawLineWithColor(quad.tlVertex.position, quad.trVertex.position, color);
    BEDrawLineWithColor(quad.trVertex.position, quad.brVertex.position, color);
    BEDrawLineWithColor(quad.brVertex.position, quad.blVertex.position, color);
    BEDrawLineWithColor(quad.blVertex.position, quad.tlVertex.position, color);
}

void BEDrawQuad(BETexturedColoredQuad quad)
{
    BEDrawLine(quad.tlVertex.position, quad.trVertex.position);
    BEDrawLine(quad.trVertex.position, quad.brVertex.position);
    BEDrawLine(quad.brVertex.position, quad.blVertex.position);
    BEDrawLine(quad.blVertex.position, quad.tlVertex.position);
}

void BEDrawRectWithColor(CGRect rect, GLKVector4 color)
{
    // Top
    BEDrawLineWithColor(GLKVector2FromCGPoint(rect.origin),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y)), color);
    // Right
    BEDrawLineWithColor(GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y)),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y + rect.size.height)), color);
    // Bottom
    BEDrawLineWithColor(GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y + rect.size.height)),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x,
                                                 rect.origin.y + rect.size.height)), color);
    // Left
    BEDrawLineWithColor(GLKVector2FromCGPoint(CGPointMake(rect.origin.x,
                                                 rect.origin.y + rect.size.height)),
               GLKVector2FromCGPoint(rect.origin), color);
}


void BEDrawRect(CGRect rect)
{
    // Top
    BEDrawLine(GLKVector2FromCGPoint(rect.origin),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y)));
    // Right
    BEDrawLine(GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y)),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y + rect.size.height)));
    // Bottom
    BEDrawLine(GLKVector2FromCGPoint(CGPointMake(rect.origin.x + rect.size.width,
                                                 rect.origin.y + rect.size.height)),
               GLKVector2FromCGPoint(CGPointMake(rect.origin.x,
                                                 rect.origin.y + rect.size.height)));
    // Left
    BEDrawLine(GLKVector2FromCGPoint(CGPointMake(rect.origin.x,
                                                 rect.origin.y + rect.size.height)),
               GLKVector2FromCGPoint(rect.origin));
}
