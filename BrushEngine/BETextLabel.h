//
//  BETextLabel.h
//  BrushEngine
//
//  Created by Germano Guerrini on 28/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BESpriteBatch.h"

@class BEFontTextureAtlas;

@interface BETextLabel : BESpriteBatch

@property (nonatomic, readonly) NSString *label;
@property (nonatomic, readonly) BEFontTextureAtlas *fontTexture;

- (id)initWithLabel:(NSString *)label fontTexture:(BEFontTextureAtlas *)fontTexture;

@end
