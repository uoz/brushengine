//
//  BEProfiler.h
//  BrushEngine
//
//  Created by Germano Guerrini on 07/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@class BEGameObject;

@interface BEProfiler : NSObject

@property (nonatomic, readonly) UILabel *label;
@property (nonatomic) GLKViewController *delegate;

+ (BEProfiler *)sharedProfiler;

- (NSString *)labelText;

- (void)addGameObject:(BEGameObject *)gameObject;
- (void)removeGameObject:(BEGameObject *)gameObject;

- (void)addTextureWithPath:(NSString *)path;
- (void)removeTextureWithPath:(NSString *)path;

- (void)resetTextureSwitchCount;
- (void)incrementTextureSwitchCount;

@end
