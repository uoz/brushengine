//
//  BEAnimatedSprite_Internal.h
//  BrushEngine
//
//  Created by Germano Guerrini on 03/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEAnimatedSprite.h"

@interface BEAnimatedSprite (Internal)

- (BOOL)containsAnimation:(BEAnimation *)animation;
- (BOOL)animationsFromSingleAtlas:(NSDictionary *)animations;

@end
