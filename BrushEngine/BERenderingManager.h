//
//  BERenderingManager.h
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BETypes.h"
#import "BERendering.h"

@class BECamera;
@class BEStage;

@interface BERenderingManager : NSObject <BERendering>

@property (nonatomic, readonly) GLKMatrixStackRef matrixStack;
@property (nonatomic, readonly) GLKMatrix4 projectionMatrix;
@property (nonatomic, strong) BECamera *camera;
@property (nonatomic, strong) BEStage *currentStage;

/** Returns the shared instance of the class. */
+ (BERenderingManager *)sharedRenderingManager;

@end
