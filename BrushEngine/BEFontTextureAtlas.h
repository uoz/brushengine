//
//  BEFontTextureAtlas.h
//  BrushEngine
//
//  Created by Germano Guerrini on 28/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BETextureAtlas.h"

typedef struct {
    int charID;
    int x;
    int y;
    int width;
    int height;
    int xOffset;
    int yOffset;
    int xAdvance;
    float scale;
} BEGlyph;

typedef struct {
    int lineHeight;
    int base;
} BEFontProperties;

@interface BEFontTextureAtlas : BETextureAtlas

- (BEGlyph)glyphForChar:(unichar)character;
- (CGRect)coordsForChar:(unichar)character;
- (CGSize)sizeForChar:(unichar)character;

@end
