//
//  BEActionManager.h
//  BrushEngine
//
//  Created by Germano Guerrini on 13/03/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BEAction.h"

@class BETarget;

@interface BEActionManager : NSObject <BEAction>

/** Returns the shared instance of the class. */
+ (BEActionManager *)sharedActionManager;

/** Registers the action. 
 Must be called within the [BEAction init] method, after the action has been assigned an actionID.
 Note that there is no check against multiple registrations for the same action, which will
 certainly cause trouble.
 */
- (void)registerAction:(BEAction *)action;

/** Unregisters the action. */
- (void)unregisterAction:(BEAction *)action;

/** Unregisters all actions with the given target. */
- (void)unregisterActionsWithTarget:(id)target;

/** Unregisters all actions. */
- (void)unregisterAllActions;

/** Runs the play method on every registered action with the given target. */
- (void)playActionsWithTarget:(id)target;

/** Runs the pause method on every registered action with the given target. */
- (void)pauseActionsWithTarget:(id)target;

/** Runs the stop method on every registered action with the given target. */
- (void)stopActionsWithTarget:(id)target;

/** Pauses only running actions of a target.
 It returns an NSSet which can be used to resume the action through the playActionsWithTarget method.
 */
- (NSSet *)pauseRunningActionsWithTarget:(id)target;

/** Plays the actions from the set that point to the given target.
 Used together with the pauseRunningActionWithTarget method to toggle actions for a target.
 */
- (void)playActions:(NSSet *)actions withTarget:(id)target;

@end