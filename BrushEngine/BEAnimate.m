//
//  BEAnimate.m
//  BrushEngine
//
//  Created by Germano Guerrini on 19/04/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEAnimate.h"
#import "BEAnimatedSprite.h"
#import "BEAnimationFrame.h"
#import "BEAnimation.h"

@interface BEAnimate ()

@property (nonatomic, readonly) BEAnimation *animation;
@property (nonatomic, readonly) BEAnimationFrame *oldFrame;
@property (nonatomic, readonly) BEAnimationFrame *currentFrame;
@property (nonatomic, readonly) NSUInteger currentFrameIndex;
@property (nonatomic, readonly) NSUInteger lastFrameIndex;

@end

@implementation BEAnimate

- (id)initWithTarget:(BEAnimatedSprite *)target animationKey:(NSString *)key
{
    _animation = [(BEAnimatedSprite *)target animationWithKey:key];
    if (_animation == nil) {
        BEDebugLog(@"Warning: BEAnimatedSprite has no animation with key '%@'", key);
        return nil;
    }
    if ((self = [super initWithTarget:target duration:_animation.duration])) {
        // A little optimization. We could set this property in the setup method,
        // but actually it should never change, so we calculate it only once.
        _lastFrameIndex = _animation.frameCount - 1;
    }
    return self;
}

- (void)setup
{
    [super setup];
    // Set the current frame index at the beginning or at the end of the frames array,
    // depending on the direction.
    _currentFrameIndex = ([self isReversed]) ? _lastFrameIndex : 0;
    _currentFrame = [_animation frameAtIndex:_currentFrameIndex];
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    _oldFrame = _currentFrame;
    
    // TODO Currently we lost the first frame for forward animations and the last one for backward ones.
    if (![self isReversed]) {
        while (offset > _currentFrame.offset && _currentFrameIndex != _lastFrameIndex) {
            if (_currentFrameIndex < _lastFrameIndex) {
                _currentFrame = [_animation frameAtIndex:++_currentFrameIndex];
            }
        }
    } else {
        while (offset < _currentFrame.offset && _currentFrameIndex != 0) {
            if (_currentFrameIndex > 0) {
                _currentFrame = [_animation frameAtIndex:--_currentFrameIndex];
            }
        }
    }

    if (_currentFrame != _oldFrame) {
        _animation.currentFrame = _currentFrame;
        [((BEAnimatedSprite *)self.target) updateToCurrentAnimationFrame];
    }
}

@end
