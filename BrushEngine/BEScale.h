//
//  BEScale.h
//  BrushEngine
//
//  Created by Germano Guerrini on 27/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

// --------------------------------------------------------------------------------
// BEScale
// --------------------------------------------------------------------------------

/** Abstract base class that defines a transformation of the scale parameter of a `BEGameObject` instance.
 
 A `BEScale` object with a duration of `0` will result in a instant resize of the target object rather than a smooth transition.
 */
@interface BEScale : BEAction
{
@protected
    GLKVector2 _startScale;
    GLKVector2 _endScale;
}

/// --------------------------------------------------------------------------------
/// @name Initialization
/// --------------------------------------------------------------------------------

/** Initializes a new scaling action.
 
 This is the designated initializer.
 
 @param target The object the action will be scaled.
 @param scale The scale vector used by the action to calculate the `resize` vector.
 @param duration The duration of the action in seconds.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale duration:(NSTimeInterval)duration;

/** Initializes a new instantaneous scaling action.
 
 @param target The object the action will be scaled.
 @param scale The scale vector used by the action to calculate the `resize` vector.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale;

@end

// --------------------------------------------------------------------------------
// BEScaleTo
// --------------------------------------------------------------------------------

/** An action that performs a resize of a `BEGameObject` instance from its current scale to the given scale. 
 Repeating this action will not resize the target object any further.
 */
@interface BEScaleTo : BEScale
@end

// --------------------------------------------------------------------------------
// BEScaleBy
// --------------------------------------------------------------------------------

/** An action that performs a resize of a `BEGameObject` instance from its current scale by the given scale. 
 Repeating this action will continue to resize the target object (exponentially!).
 */
@interface BEScaleBy : BEScale
@end

// --------------------------------------------------------------------------------
// BEFlip
// --------------------------------------------------------------------------------

typedef enum {
    BEFlipAxisX,
    BEFlipAxisY,
    BEFlipAxisXY
} BEFlipAxis;

/** An action that performs an instant flip of a `BEGameObject` instance. 
 To choose the axis along which the object should flip, use one of the `BEFlipAxis` constant.
 */
@interface BEFlip : BEScaleBy

/// --------------------------------------------------------------------------------
/// @name Initialization
/// --------------------------------------------------------------------------------

/** Initializes a new flipping action.
 
 @param target The object the action will flip.
 @param axis The axis over which the object will be flipped.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target flipAxis:(BEFlipAxis)axis;

@end
