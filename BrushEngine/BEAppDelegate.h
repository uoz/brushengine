//
//  BEAppDelegate.h
//  BrushEngine
//
//  Created by Germano Guerrini on 26/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "TestViewController.h"

@interface BEAppDelegate : UIResponder <UIApplicationDelegate> 

@property (strong, nonatomic) UIWindow *window;

@end
