//
//  BEAnimationFrame.m
//  BrushEngine
//
//  Created by Germano Guerrini on 12/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAnimationFrame.h"

@implementation BEAnimationFrame

- (id)initWithKey:(NSString *)key offset:(NSTimeInterval)offset
{
    if ((self = [super init])) {
        _key = key;
        _offset = offset;
    }
    return self;
}

+ (id)animationFrameWithKey:(NSString *)key offset:(NSTimeInterval)offset
{
    return [[self alloc] initWithKey:key offset:offset];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"key: %@, offset: %f\n", _key, _offset];
}

@end
