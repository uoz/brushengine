//
//  BEView.h
//  BrushEngine
//
//  Created by Germano Guerrini on 31/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface BEView : GLKView

@end
