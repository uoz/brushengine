//
//  BEAnimatedSprite.m
//  BrushEngine
//
//  Created by Germano Guerrini on 13/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAnimatedSprite.h"
#import "BEAnimatedSprite_Internal.h"
#import "BEAnimation.h"
#import "BEAnimationFrame.h"
#import "BEAnimate.h"
#import "BERepeatingAction.h"
#import "BETextureAtlas.h"
#import "BEAnimate.h"

@interface BEAnimatedSprite ()

@property (nonatomic, readonly) NSMutableDictionary *animations;

@end

@implementation BEAnimatedSprite

- (id)initWithAnimations:(NSDictionary *)animations setCurrent:(BEAnimation *)animation
{
    NSAssert([self animationsFromSingleAtlas:animations], @"Animations are from different atlas");
    NSAssert([animations objectForKey:animation.key], @"Current animation not found");
    if ((self = [super initWithTextureAtlas:animation.atlas key:animation.currentFrame.key])) {
        _animations = [animations mutableCopy];
        _currentAnimation = animation;
    }
    return self;
}

- (id)initWithAnimation:(BEAnimation *)animation
{
    NSDictionary *animations = [NSMutableDictionary dictionaryWithObject:animation forKey:animation.key];
    return [self initWithAnimations:animations setCurrent:animation];
}

- (void)addAnimation:(BEAnimation *)animation
{
    NSAssert(animation.atlas.texture == self.texture, @"Cannot add an animation with a different atlas");
    [_animations setObject:animation forKey:animation.key];
}

- (void)setCurrentAnimation:(BEAnimation *)animation
{
    if (![self containsAnimation:animation]) {
        // TODO Log or assert?
        return;
    }
          
    if (_currentAnimation != animation) {
        _currentAnimation = [self animationWithKey:animation.key];
        self.size = [_currentAnimation.atlas sizeForKey:_currentAnimation.currentFrame.key];;
    }
}

- (BEAnimation *)animationWithKey:(NSString *)key
{
    return [_animations objectForKey:key];
}

- (void)updateToCurrentAnimationFrame
{
    self.textureCoordinates = [_currentAnimation textureCoordinatesForCurrentFrame];
}

- (id<BEAction>)playAnimationWithKey:(NSString *)key
{
    self.currentAnimation = [self animationWithKey:key];
    BEAnimate *animate = [[BEAnimate alloc] initWithTarget:self animationKey:key];
    BERepeatingAction *action = [[BERepeatingAction alloc] initWithAction:animate];
    [action play];
    return action;
}

@end

#pragma mark - Internal

@implementation BEAnimatedSprite (Internal)

- (BOOL)containsAnimation:(BEAnimation *)animation
{
    return [self animationWithKey:animation.key] != nil;
}

- (BOOL)animationsFromSingleAtlas:(NSDictionary *)animations
{
    __block BETextureAtlas *atlas = nil;
    __block BOOL ret_val = YES;
    [animations enumerateKeysAndObjectsUsingBlock:^(id key, BEAnimation *animation, BOOL *stop) {
        if (atlas == nil) {
            atlas = animation.atlas;
        } else if (atlas != animation.atlas) {
            ret_val = NO;
            *stop = YES;
        }
    }];
    return ret_val;
}

@end
