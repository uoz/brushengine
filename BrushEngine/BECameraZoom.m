//
//  BECameraZoom.m
//  BrushEngine
//
//  Created by Germano Guerrini on 18/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BECameraZoom.h"
#import "BECamera.h"

@interface BECameraZoom ()

@property (nonatomic, readonly) float startZoom;
@property (nonatomic, readonly) float endZoom;
@property (nonatomic, readonly) float variance;
@property (nonatomic, readonly) float cachedVariance;

@end

@implementation BECameraZoom

- (id)initWithCamera:(BECamera *)camera zoom:(float)zoom duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:camera duration:duration])) {
        _endZoom = zoom;
        _cachedVariance = INFINITY;
    }
    return self;
}

- (id)initWithCamera:(BECamera *)camera zoom:(float)zoom
{
    return [self initWithCamera:camera zoom:zoom duration:0.0f];
}

- (float)variance
{
    if (_cachedVariance == INFINITY) {
        _cachedVariance = _endZoom - _startZoom;
    }
    return _cachedVariance;
}

- (void)setup
{
    [super setup];
    _startZoom = ((BECamera *)self.target).zoom;
}

- (void)stop
{
    [super stop];
    _cachedVariance = INFINITY;
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BECamera *)self.target).zoom = _startZoom + self.variance * offset;
}

@end
