//
//  BERendering.h
//  BrushEngine
//
//  Created by Germano Guerrini on 28/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BERendering <NSObject>

@required
- (void)updateWithInterval:(NSTimeInterval)anInterval;
- (void)render;
@end
