//
//  BEDictionaryAddictions.h
//  BrushEngine
//
//  Created by Germano Guerrini on 17/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionaryAddictions)

- (int)intValueFromObjectForKey:(NSString *)key;
- (float)floatValueFromObjectForKey:(NSString *)key;

@end
