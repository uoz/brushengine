//
//  BEAnimation.m
//  BrushEngine
//
//  Created by Germano Guerrini on 11/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAnimation.h"
#import "BETextureAtlas.h"
#import "BEAnimationFrame.h"
#import "BEUtils.h"

@interface BEAnimation ()

@property (nonatomic, readonly) NSMutableArray *frames;
@property (nonatomic, readonly) NSTimeInterval elapsedTime;
@property (nonatomic, readonly) NSUInteger lastFrameIndex;
@property (nonatomic, readonly) NSUInteger currentFrameIndex;

@end

@implementation BEAnimation

#pragma mark - Initializers

- (id)initWithKey:(NSString *)key atlas:(BETextureAtlas *)atlas keyFormat:(NSString *)format keyRange:(NSRange)range frequency:(NSUInteger)frequency
{
    if ((self = [super init])) {
        NSUInteger numberOfExpectedFrames = range.length - range.location + 1;
        NSUInteger numberOfActualFrames = 0;
        NSTimeInterval frameDuration = 1.0f / frequency;
        NSTimeInterval frameOffset = 1.0f / numberOfExpectedFrames;
        NSTimeInterval currentFrameOffset;
        NSString *frameKey;
        _frames = [NSMutableArray arrayWithCapacity:numberOfExpectedFrames];
        for (NSUInteger i = range.location; i <= range.location + range.length - 1; i++) {
            frameKey = [NSString stringWithFormat:format, i];
            if ([atlas containsKey:frameKey]) {
                numberOfActualFrames++;
                currentFrameOffset = frameOffset * (i - range.location + 1);
                [_frames addObject:[BEAnimationFrame animationFrameWithKey:frameKey offset:currentFrameOffset]];
            } else {
                BEDebugLog(@"Atlas %@ does not contain a frame with key %@", atlas, frameKey);
            }
        }
        _duration = frameDuration * numberOfActualFrames;
        _currentFrame = [_frames objectAtIndex:0];
        _frameCount = [_frames count];
        _key = key;
        _atlas = atlas;
    }
    return self;
}

- (BEAnimationFrame *)frameAtIndex:(NSUInteger)index
{   
    if (index < _frameCount) {
        return [_frames objectAtIndex:index];
    }
    return nil;
}

- (CGRect)textureCoordinatesForCurrentFrame
{
    return [_atlas coordsForKey:_currentFrame.key];
}

- (CGSize)sizeForCurrentFrame
{
    return [_atlas sizeForKey:_currentFrame.key];
}

@end
