//
//  BEColorize.h
//  BrushEngine
//
//  Created by Germano Guerrini on 03/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEAction.h"

@interface BEColorize : BEAction

- (id)initWithTarget:(BEGameObject *)target color:(GLKVector4)color duration:(NSTimeInterval)duration;
- (id)initWithTarget:(BEGameObject *)target color:(GLKVector4)color;

@end
