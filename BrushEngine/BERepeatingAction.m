//
//  BERepeatingAction.m
//  BrushEngine
//
//  Created by Germano Guerrini on 24/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BERepeatingAction.h"

#define REPEAT_FOREVER -1

@interface BERepeatingAction ()

@property (nonatomic, readonly) id<BEAction> innerAction;
@property (nonatomic, readonly) NSUInteger repeatCount;
@property (nonatomic, readonly) NSUInteger repeatLeft;

@end

@implementation BERepeatingAction

@synthesize delegate;

- (id)initWithAction:(id<BEAction>)action repeatCount:(NSUInteger)count
{
    if ((self = [super init])) {
        _innerAction = action;
        ((BEAction *)_innerAction).delegate = self;
        _repeatLeft = _repeatCount = count;
    }
    return self;
}

- (id)initWithAction:(id<BEAction>)action
{
    return [self initWithAction:action repeatCount:REPEAT_FOREVER];
}

#pragma mark - BEAction Protocol Methods

//- (void)setup
//{
//    [innerAction setup];
//}

- (void)play
{
    [_innerAction play];
}

- (void)pause
{
    [_innerAction pause];
}

- (void)toggle
{
    [_innerAction toggle];
}

- (void)stop
{
    [_innerAction stop];
}

- (void)reverse
{
    [_innerAction reverse];
}

- (BOOL)isSequenceable
{
    return _repeatCount != REPEAT_FOREVER;
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    [_innerAction updateWithInterval:interval];
}

#pragma mark - BEActionDelegate Protocol Methods

- (void)actionDidStart:(id<BEAction>)action
{
    // Mmm... Maybe nothing to do here?
}

- (void)actionDidStop:(id<BEAction>)action
{
    if (_repeatLeft == 0) {
        if (_repeatCount == REPEAT_FOREVER) {
            // The action has been stopped programmatically
            _repeatLeft = _repeatCount;
        }
        [self.delegate actionDidStop:self];
    } else if (_repeatLeft > 1) {
        if (_repeatCount != REPEAT_FOREVER) {
            _repeatLeft--;
        }
        [_innerAction play];
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"action: %@, repeatCount: %d, repeatLeft: %d\n", 
            _innerAction, _repeatCount, _repeatLeft];
}


@end
