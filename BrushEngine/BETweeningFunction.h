//
//  BETweeningFunction.h
//  BrushEngine
//
//  Created by Germano Guerrini on 25/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    // Linear
    BETweeningFunctionLinear = 0,
    // Quad
    BETweeningFunctionQuadEaseIn,
    BETweeningFunctionQuadEaseOut,
    BETweeningFunctionQuadEaseInOut,
    BETweeningFunctionQuadEaseOutIn,
    // Cubic
    BETweeningFunctionCubicEaseIn,
    BETweeningFunctionCubicEaseOut,
    BETweeningFunctionCubicEaseInOut,
    BETweeningFunctionCubicEaseOutIn,
    // Quartic
    BETweeningFunctionQuarticEaseIn,
    BETweeningFunctionQuarticEaseOut,
    BETweeningFunctionQuarticEaseInOut,
    BETweeningFunctionQuarticEaseOutIn,
    // Quintic
    BETweeningFunctionQuinticEaseIn,
    BETweeningFunctionQuinticEaseOut,
    BETweeningFunctionQuinticEaseInOut,
    BETweeningFunctionQuinticEaseOutIn,
    // Sinusoidal
    BETweeningFunctionSinusoidalEaseIn,
    BETweeningFunctionSinusoidalEaseOut,
    BETweeningFunctionSinusoidalEaseInOut,
    BETweeningFunctionSinusoidalEaseOutIn,
    // Exponential
    BETweeningFunctionExponentialEaseIn,
    BETweeningFunctionExponentialEaseOut,
    BETweeningFunctionExponentialEaseInOut,
    BETweeningFunctionExponentialEaseOutIn,
    // Circular
    BETweeningFunctionCircularEaseIn,
    BETweeningFunctionCircularEaseOut,
    BETweeningFunctionCircularEaseInOut,
    BETweeningFunctionCircularEaseOutIn,
    // Elastic
    BETweeningFunctionElasticEaseIn,
    BETweeningFunctionElasticEaseOut,
    BETweeningFunctionElasticEaseInOut,
    BETweeningFunctionElasticEaseOutIn,
    // Back
    BETweeningFunctionBackEaseIn,
    BETweeningFunctionBackEaseOut,
    BETweeningFunctionBackEaseInOut,
    BETweeningFunctionBackEaseOutIn,
    // Bounce
    BETweeningFunctionBounceEaseIn,
    BETweeningFunctionBounceEaseOut,
    BETweeningFunctionBounceEaseInOut,
    BETweeningFunctionBounceEaseOutIn,
} BETweeningFunctionType;

typedef NSTimeInterval (*BETweeningFunctionPtr) (NSTimeInterval offset);

@interface BETweeningFunction : NSObject

+ (BETweeningFunctionPtr)functionOfType:(BETweeningFunctionType)type;

@end
