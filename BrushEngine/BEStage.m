//
//  BEStage.m
//  BrushEngine
//
//  Created by Germano Guerrini on 01/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEStage.h"

@interface BEStage ()

@property (nonatomic, readonly) NSMutableArray *layers;

@end


@implementation BEStage
- (id)init
{
    if ((self = [super init])) {
        _layers = [NSMutableArray arrayWithCapacity:BE_DEFAULT_LAYER_COUNT];
        for (int i = 0; i < BE_DEFAULT_LAYER_COUNT; i++) {
            [_layers addObject:[NSNull null]];
        }
    }
    return self;
}

- (void)addLayer:(BELayer *)layer atDepth:(BELayerDepth)depth
{
    [_layers replaceObjectAtIndex:depth withObject:layer];
}

- (void)addObject:(id<BERendering>)object atDepth:(BELayerDepth)depth
{
    if ([_layers count] >= depth) {
        BELayer *destinationLayer = [_layers objectAtIndex:depth];
        [destinationLayer addChild:object];
    } else {
        NSLog(@"ERROR - Adding an object to a uninitialized layer.");
    }
}

- (BELayer *)layerAtDepth:(BELayerDepth)depth
{
    if ([_layers count] >= depth) {
        id layer = [_layers objectAtIndex:depth];
        if (layer != (id)[NSNull null]) {
            return (BELayer *)layer;
        } else {
            NSLog(@"ERROR - Retrieving a nil layer.");
            return nil;
        }
    } else {
        NSLog(@"ERROR - Retrieving a layer out of bounds.");
        return nil;
    }
}

#pragma mark - BERendering Protocol Methods

- (void)updateWithInterval:(NSTimeInterval)interval
{
    for (BELayer *layer in _layers) {
        if (layer != (id)[NSNull null]) {
            [layer updateWithInterval:interval];
        }
    }
}

- (void)render
{
    for (BELayer *layer in _layers) {
        if (layer != (id)[NSNull null]) {
            [layer render];
        }
    }
}

@end
