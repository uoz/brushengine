//
//  BECameraShake.h
//  BrushEngine
//
//  Created by Germano Guerrini on 20/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEAction.h"

@class BECamera;

typedef enum {
    BEShakeTypeHorizontal = 0,
    BEShakeTypeVertical,
    BEShakeTypeOblique
} BEShakeType;

@interface BECameraShake : BEAction

- (id)initWithCamera:(BECamera *)camera type:(BEShakeType)type magnitude:(float)magnitude duration:(NSTimeInterval)duration;

@end
