//
//  BETextureLoader.h
//  BrushEngine
//
//  Created by Germano Guerrini on 05/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface BETextureLoader : NSObject

+ (GLKTextureInfo *)textureWithContentsOfFile:(NSString *)fileName options:(NSDictionary *)options;
+ (GLKTextureInfo *)textureWithContentsOfFile:(NSString *)fileName;
+ (void)deleteTextureWithContentsOfFile:(NSString *)fileName;
+ (void)purgeCachedTextures;

@end
