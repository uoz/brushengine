//
//  BEGameObject.m
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEGameObject.h"
#import "BESettings.h"
#import "BEUtils.h"
#import "BETypes.h"
#import "BEProfiler.h"

@interface BEGameObject ()

@property (nonatomic, readonly) GLKMatrix4 localMatrix;
@property (nonatomic, readonly) BOOL localMatrixNeedsUpdate;
//@property (nonatomic, readonly) float width;
//@property (nonatomic, readonly) float height;

@end

@implementation BEGameObject

- (id)init
{
    if ((self = [super init])) {
        _renderingManager = [BERenderingManager sharedRenderingManager];
        _modelViewMatrix = GLKMatrix4Identity; // TODO Remove?
        _localMatrix = GLKMatrix4Identity;
        _position = GLKVector2Zero;
        _rotation = 0.0f;
        _scale = GLKVector2Make(1.0f, 1.0f);
        _pivot = GLKVector2Zero;
        _size = CGSizeZero;
        _isVisible = YES;
        _children = nil;
    }
    return self;
}

#pragma mark - Trasformation Parameters Setters

- (void)setPosition:(GLKVector2)position
{
    if (!GLKVector2AllEqualToVector2(position, _position)) {
        _position = position;
        _localMatrixNeedsUpdate = YES;
    }
}

- (void)setPivot:(GLKVector2)pivot
{
    if (!GLKVector2AllEqualToVector2(pivot, _pivot)) {
        _pivot = pivot;
        _localMatrixNeedsUpdate = YES;
    }
}

- (void)setScale:(GLKVector2)scale
{
    if (!GLKVector2AllEqualToVector2(scale, _scale)) {
        _scale = scale;
        _localMatrixNeedsUpdate = YES;
    }
}

- (void)setRotation:(float)rotation
{
    if (rotation != _rotation) {
        _rotation = rotation;
        _localMatrixNeedsUpdate = YES;
    }
}

#pragma mark - Children Management

- (void)addChild:(BEGameObject *)child
{
    NSAssert(child != nil, @"Cannot add a nil child");
    NSAssert(child != self, @"Cannot add self as a child.");
    
    // Lazy instantiation
    if (_children == nil) {
        _children = [[NSMutableArray alloc] init];
    }
    
    // Remove child from its current parent
    if (child.parent != nil) {
        [child.parent removeChild:child];
    }
    
    // Finally, add child
    child.parent = self;
    [_children addObject:child];

#ifdef BE_PROFILER_ENABLED
    [[BEProfiler sharedProfiler] addGameObject:self];
#endif
}

- (void)removeChild:(BEGameObject *)child
{
    // No-op if we are not the parent
    if (child.parent == self) {
        child.parent = nil;
        [_children removeObject:child];
    } else {
        BEDebugLog(@"Tried to remove a child from the wrong parent.");
    }
}

- (void)removeLastChild
{
    [[self getLastChild] setParent:nil];
    [_children removeLastObject];
}

- (void)removeAllChildren
{
    [_children makeObjectsPerformSelector:@selector(setParent:) withObject:nil];
    [_children removeAllObjects];
}

- (BEGameObject *)getLastChild
{
    return (BEGameObject *)[_children lastObject];
}

- (BEGameObject *)getChildAtIndex:(NSUInteger)index
{
    if (index < [_children count]) {
        return (BEGameObject *)[_children objectAtIndex:index];
    }
    return nil;
}

- (BOOL)isDescendantOf:(BEGameObject *)object
{
    return _parent == object || [_parent isDescendantOf:object];
}

- (void)swapChild:(BEGameObject *)child1 withChild:(BEGameObject *)child2
{
    NSUInteger index1 = [_children indexOfObject:child1];
    NSUInteger index2 = [_children indexOfObject:child2];
    if (index1 != NSNotFound && index2 != NSNotFound) {
        [_children exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
    } else {
        BEDebugLog(@"Could not found children to swap.");
    }
}

#pragma mark - Transformation Methods

- (GLKMatrix4)localToParentMatrix
{
    if (_localMatrixNeedsUpdate) {
        // Start with an identity matrix
        _localMatrix = GLKMatrix4Identity;
        
        // Center on pivot point
        if (!GLKVector2AllEqualToScalar(_pivot, 0.0f)) {
            _localMatrix = GLKMatrix4Multiply(GLKMatrix4MakeTranslation(-_pivot.x, -_pivot.y, 0.0f), _localMatrix);
        }
        // Scale
        if (!GLKVector2AllEqualToScalar(_scale, 1.0f)) {
            _localMatrix = GLKMatrix4Multiply(GLKMatrix4MakeScale(_scale.x, _scale.y, 1.0f), _localMatrix);
        }
        // Rotate
        if (_rotation != 0.0f) {
            _localMatrix = GLKMatrix4Multiply(GLKMatrix4MakeZRotation(_rotation), _localMatrix);
        }
        // Translate
        _localMatrix = GLKMatrix4Multiply(GLKMatrix4MakeTranslation(_position.x, _position.y, 0.0f), _localMatrix);
        
        _localMatrixNeedsUpdate = NO;
    }
    return _localMatrix;
}

- (GLKMatrix4)parentToLocalMatrix
{
    return GLKMatrix4Invert([self localToParentMatrix], nil);
}

- (GLKMatrix4)localToWorldMatrix
{
    GLKMatrix4 matrix = [self localToParentMatrix];
    // Go up till the root
    for (BEGameObject *nextParent = _parent; nextParent != nil; nextParent = nextParent.parent) {
        matrix = GLKMatrix4Multiply([nextParent localToParentMatrix], matrix);
    }
    return matrix;
}

- (GLKMatrix4)worldToLocalMatrix
{
    return GLKMatrix4Invert([self localToWorldMatrix], nil);
}

- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint relativeToPivot:(BOOL)relativeToPivot
{
    GLKVector2 worldPoint = GLKMatrix4MultiplyVector2WithTranslation([self localToWorldMatrix], localPoint);
    if (relativeToPivot) {
        worldPoint = GLKVector2Add(worldPoint, _pivot);
    }
    return worldPoint;
}

- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint
{
    return [self localPointToWorldSpace:localPoint relativeToPivot:NO];
}

- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint relativeToPivot:(BOOL)relativeToPivot
{
    GLKVector2 localPoint = GLKMatrix4MultiplyVector2WithTranslation([self worldToLocalMatrix], worldPoint);
    if (relativeToPivot) {
        localPoint = GLKVector2Subtract(localPoint, _pivot);
    }
    return localPoint;
}

- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint
{
    return [self worldPointToLocalSpace:worldPoint relativeToPivot:NO];
}

#pragma mark - Rendering Protocol Methods

- (void)updateTransform
{
    GLKMatrixStackMultiplyMatrix4(_renderingManager.matrixStack, [self localToParentMatrix]);
    _modelViewMatrix = GLKMatrixStackGetMatrix4(_renderingManager.matrixStack);
}

- (void)render
{
    // If an object is not visible, neither will its children
    if (!_isVisible) {
        return;
    }

    GLKMatrixStackPush(_renderingManager.matrixStack);
    
    [self updateTransform];
    [self draw];
    [_children makeObjectsPerformSelector:@selector(render)];
    
    GLKMatrixStackPop(_renderingManager.matrixStack);
}

- (void)draw
{
    // Implemented by subclasses
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    for (id child in _children) {
        [child updateWithInterval:interval];
    }
    // Implemented by subclasses
}

@end
