//
//  BEOpenGLStateCache.m
//  BrushEngine
//
//  Created by Germano Guerrini on 02/06/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEOpenGLStateCache.h"
#import "BESettings.h"

#ifdef BE_ENABLE_GL_STATE_CACHING
static GLenum _cachedBlendSrc = -1;
static GLenum _cachedBlendDst = -1;
#endif // BE_ENABLE_GL_STATE_CACHING

void BEglBlendFunc(GLenum src, GLenum dst)
{
#ifdef BE_ENABLE_GL_STATE_CACHING
    if (src != _cachedBlendSrc || dst != _cachedBlendDst) {
        _cachedBlendSrc = src;
        _cachedBlendDst = dst;
        glBlendFunc(src, dst);
    }
#else
    glBlendFunc(src, dst);
#endif // BE_ENABLE_GL_STATE_CACHING
}

void BEsetAlphaBlending(BOOL flag)
{
    if (flag) {
        glEnable(GL_BLEND);
#ifdef BE_ENABLE_GL_STATE_CACHING
        BEglBlendFunc(BE_DEFAULT_BLEND_SRC, BE_DEFAULT_BLEND_DST);
#else
        glBlendFunc(BE_DEFAULT_BLEND_SRC, BE_DEFAULT_BLEND_DST);
#endif // BE_ENABLE_GL_STATE_CACHING
    } else {
        glDisable(GL_BLEND);
    }
}

