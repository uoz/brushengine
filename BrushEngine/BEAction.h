//
//  BEAction.h
//  BrushEngine
//
//  Created by Germano Guerrini on 18/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BEUtils.h"
#import "BETweeningFunction.h"

@class BEGameObject;
@class BEAction;
@protocol BEAction;

// --------------------------------------------------------------------------------
// Type definitions
// --------------------------------------------------------------------------------

typedef enum {
    BEActionStateRunning = 0,
    BEActionStatePaused,
    BEActionStateStopped
} BEActionState;

typedef enum {
    BEActionDirectionForward = 0,
    BEActionDirectionBackward
} BEActionDirection;

// --------------------------------------------------------------------------------
// Protocols
// --------------------------------------------------------------------------------

/** Delegate protocol for the action system.
 
 The delegate must implement methods to run any necessary set up of the action
 parameters when an action starts or stops.
 */
@protocol BEActionDelegate <NSObject>
@required

/** Called when an action gets started, after it has been setup and the playAction method
 of the action manager has been called. 
 If an action has been stopped, this method will get called if it gets played again.
 */
- (void)actionDidStart:(id<BEAction>)action;

/** Called every time an action gets stopped, after the pauseAction (TODO should be stopAction?) method
 of the action manager has been called.
 */
- (void)actionDidStop:(id<BEAction>)action;

@end

/** Action protocol.
 
 It defines the basic methods that conforming classes must implement. 
 Only the core classes conforms directly to the protocol.
 */
@protocol BEAction <NSObject>
@property (strong, nonatomic) id<BEActionDelegate> delegate;
@required

/** Plays the action. */
- (void)play;

/** Suspends the action. */
- (void)pause;

/** If the action is running it suspends it and viceversa. */
- (void)toggle;

/** Stops the action. Depending on the type of action, some parameters might be reset. */
- (void)stop;

/** Update the action using the given time interval. 
 Some actions can use a tweening function to manipulate the way time affects the change of
 the parameters involved with the action if they have a non-zero duration. 
 @warning This method is implement only by the core classes, other classes should avoid overriding.
 */
- (void)updateWithInterval:(NSTimeInterval)interval;

@optional

/** Changes the current direction of the action. */
- (void)reverse;

/** Returns `YES` or `NO` whether the action can be looped through a sequence.
 Generally, only infinite loops are not sequenceable.
 */
- (BOOL)isSequenceable;

/** Performs any action setup needed before the action is played. 
 This method gets also called at the beginning of every loop in a repeating action. 
 Subclasses must always call the super method too.
 */
- (void)setup;

/** Moves the action to an offset that is relative to the entire duration of the action. 
 A time offset equal to `0` means the beginning of the action, while a value of `1` means
 the end of the action.
 */
- (void)advanceToOffset:(NSTimeInterval)offset;

@end

// --------------------------------------------------------------------------------
// Interface
// --------------------------------------------------------------------------------

/** Abstract base class for all action objects.
 
 Actions handle time based transformations of numeric object variables,
 interpolating between the starting and the ending value over a time interval. 
 The linearity of the interpolation can be changed using one of the provided `BETweeningFunctionType`. 
 Actions can also be instantaneous, in which case no interpolation will occur.
 */
@interface BEAction : NSObject <BEAction>

/** The unique action identifier. */
@property (nonatomic, readonly) int actionID;

/** The target object of the action. */
@property (nonatomic, readonly) id target;

/** The interpolation function used by the action. The default value is `BETweeningFunctionLinear`. */
@property (nonatomic) BETweeningFunctionType tweeningFunctionType;

/** Whether the action is moving forward or backward. */
@property (nonatomic, readonly) BOOL isReversed;

/// --------------------------------------------------------------------------------
/// @name Initialization
/// --------------------------------------------------------------------------------

/** Initializes a new action.
 
 This is the designated initializer.
 
 @param target The object the action will perform on.
 @param duration The duration of the action in seconds.
 @param direction The `BEActionDirection` constant.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(id)target duration:(NSTimeInterval)duration direction:(BEActionDirection)direction;

/** Initializes a new forward action.
  
 @param target The object the action will perform on.
 @param duration The duration of the action in seconds.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(id)target duration:(NSTimeInterval)duration;

/** Initializes a new forward action with a duration of 0 seconds.
 
 @param target The object the action will perform on.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(id)target;

/** Returns `YES` if the action is currently being played, `NO` otherwise. */
- (BOOL)isRunning;

@end
