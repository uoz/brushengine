//
//  BETypes.h
//  BrushEngine
//
//  Created by Germano Guerrini on 28/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//
#ifndef __BE_TYPES_H
#define __BE_TYPES_H

#import <GLKit/GLKit.h>

#ifdef __cplusplus
extern "C" {
#endif

#pragma mark - BEColoredVertex

typedef struct {
    GLKVector2 position;
    GLKVector4 color;
} BEColoredVertex;

static inline BEColoredVertex BEColoredVertexMake(GLKVector2 position, GLKVector4 color);
static inline BEColoredVertex BEColoredVertexMake(GLKVector2 position, GLKVector4 color)
{
    BEColoredVertex cv;
    cv.position = position;
    cv.color = color;
    return cv;
}

static inline NSString * NSStringFromBEColoredVertex(BEColoredVertex vertex);
static inline NSString * NSStringFromBEColoredVertex(BEColoredVertex vertex)
{
    return [NSString stringWithFormat:@"p: {%f, %f}, c: {%f, %f, %f, %f}", 
            vertex.position.x, vertex.position.y,
            vertex.color.r, vertex.color.g, vertex.color.b, vertex.color.a];
}

#pragma mark - BETexturedColoredVertex

typedef struct {
    GLKVector2 position;
    GLKVector2 textureCoords;
    GLKVector4 color;
} __attribute__((__packed__)) BETexturedColoredVertex;

static inline BETexturedColoredVertex BETexturedColoredVertexMake(GLKVector2 position, GLKVector4 color, GLKVector2 textureCoords);
static inline BETexturedColoredVertex BETexturedColoredVertexMake(GLKVector2 position, GLKVector4 color, GLKVector2 textureCoords)
{
    BETexturedColoredVertex tcv;
    tcv.position = position;
    tcv.color = color;
    tcv.textureCoords = textureCoords;
    return tcv;
}

static inline NSString * NSStringFromBETexturedColoredVertex(BETexturedColoredVertex vertex);
static inline NSString * NSStringFromBETexturedColoredVertex(BETexturedColoredVertex vertex)
{
    return [NSString stringWithFormat:@"p: {%f, %f}, c: {%f, %f, %f, %f}, t: {%f, %f}", 
        vertex.position.x, vertex.position.y,
        vertex.color.r, vertex.color.g, vertex.color.b, vertex.color.a,
        vertex.textureCoords.s, vertex.textureCoords.t];
}

#pragma mark - BEColoredQuad

typedef union {
    struct {
        BEColoredVertex tlVertex;
        BEColoredVertex trVertex;
        BEColoredVertex blVertex;
        BEColoredVertex brVertex;
    };
    BEColoredVertex vertices[4];
} BEColoredQuad;

static inline BEColoredQuad BEColoredQuadMake(BEColoredVertex tlVertex, BEColoredVertex trVertex, 
                                              BEColoredVertex brVertex, BEColoredVertex blVertex);
static inline BEColoredQuad BEColoredQuadMake(BEColoredVertex tlVertex, BEColoredVertex trVertex, 
                                              BEColoredVertex brVertex, BEColoredVertex blVertex)
{
    BEColoredQuad cq = { tlVertex, trVertex, brVertex, blVertex };
    return cq;
}

static inline NSString * NSStringFromBEColoredQuad(BEColoredQuad quad);
static inline NSString * NSStringFromBEColoredQuad(BEColoredQuad quad)
{
    return [NSString stringWithFormat:@"v1: %@\nv2: %@\nv3: %@\nv4: %@", 
            NSStringFromBEColoredVertex(quad.vertices[0]), 
            NSStringFromBEColoredVertex(quad.vertices[1]),
            NSStringFromBEColoredVertex(quad.vertices[2]),
            NSStringFromBEColoredVertex(quad.vertices[3])];
}

#pragma mark - BETexturedColoredQuad

typedef union {
    struct {
        BETexturedColoredVertex tlVertex;
        BETexturedColoredVertex trVertex;
        BETexturedColoredVertex blVertex;
        BETexturedColoredVertex brVertex;
    };
    BETexturedColoredVertex vertices[4];
} BETexturedColoredQuad;

static inline BETexturedColoredQuad BETexturedColoredQuadMake(BETexturedColoredVertex tlVertex,
                                                              BETexturedColoredVertex trVertex,
                                                              BETexturedColoredVertex blVertex,
                                                              BETexturedColoredVertex brVertex);
static inline BETexturedColoredQuad BETexturedColoredQuadMake(BETexturedColoredVertex tlVertex,
                                                              BETexturedColoredVertex trVertex,
                                                              BETexturedColoredVertex blVertex,
                                                              BETexturedColoredVertex brVertex)
{
    BETexturedColoredQuad tcq = { tlVertex, trVertex, blVertex, brVertex };
    return tcq;
}

static inline NSString * NSStringFromBETexturedColoredQuad(BETexturedColoredQuad quad);
static inline NSString * NSStringFromBETexturedColoredQuad(BETexturedColoredQuad quad)
{
    return [NSString stringWithFormat:@"v1: %@\nv2: %@\nv3: %@\nv4: %@", 
                NSStringFromBETexturedColoredVertex(quad.vertices[0]), 
                NSStringFromBETexturedColoredVertex(quad.vertices[1]),
                NSStringFromBETexturedColoredVertex(quad.vertices[2]),
                NSStringFromBETexturedColoredVertex(quad.vertices[3])];
}

#pragma mark - GLKit Extensions

static const GLKVector2 GLKVector2Zero = {0.0f, 0.0f};
static const GLKVector3 GLKVector3Zero = {0.0f, 0.0f, 0.0f};
static const GLKVector4 GLKVector4Zero = {0.0f, 0.0f, 0.0f, 0.0f};

static const GLKVector4 BEColorRed = {1.0, 0.0f, 0.0f, 1.0f};
static const GLKVector4 BEColorGreen = {0.0, 1.0f, 0.0f, 1.0f};
static const GLKVector4 BEColorBlu = {0.0, 0.0f, 1.0f, 1.0f};
static const GLKVector4 BEColorBlack = {0.0, 0.0f, 0.0f, 1.0f};
static const GLKVector4 BEColorWhite = {1.0, 1.0f, 1.0f, 1.0f};
    
static inline GLKVector3 GLKVector3MakeWithVector2(GLKVector2 vector, float z)
{
    GLKVector3 v = { vector.v[0], vector.v[1], z };
    return v;
}

static inline GLKVector2 GLKVector3GetVector2(GLKVector3 vector)
{
    GLKVector2 v = { vector.v[0], vector.v[1] };
    return v;
}

static inline GLKVector2 GLKVector2MakeWithDictionary(NSDictionary *dict, NSArray *keys)
{
    NSNumber *x = [dict objectForKey:[keys objectAtIndex:0]];
    NSNumber *y = [dict objectForKey:[keys objectAtIndex:1]];
    return GLKVector2Make([x floatValue], [y floatValue]);
}

static inline GLKVector3 GLKVector3MakeWithDictionary(NSDictionary *dict, NSArray *keys)
{
    NSNumber *x = [dict objectForKey:[keys objectAtIndex:0]];
    NSNumber *y = [dict objectForKey:[keys objectAtIndex:1]];
    NSNumber *z = [dict objectForKey:[keys objectAtIndex:2]];
    return GLKVector3Make([x floatValue], [y floatValue], [z floatValue]);
}

static inline GLKVector4 GLKVector4MakeWithDictionary(NSDictionary *dict, NSArray *keys)
{
    NSNumber *x = [dict objectForKey:[keys objectAtIndex:0]];
    NSNumber *y = [dict objectForKey:[keys objectAtIndex:1]];
    NSNumber *z = [dict objectForKey:[keys objectAtIndex:2]];
    NSNumber *w = [dict objectForKey:[keys objectAtIndex:3]];
    return GLKVector4Make([x floatValue], [y floatValue], [z floatValue], [w floatValue]);
}

/* Assumes 1 in the z and w components. */
static inline GLKVector2 GLKMatrix4MultiplyVector2WithTranslation(GLKMatrix4 matrixLeft, GLKVector2 vectorRight)
{
    GLKVector3 v3 = GLKMatrix4MultiplyVector3WithTranslation(matrixLeft, GLKVector3Make(vectorRight.v[0], vectorRight.v[1], 1.0f));
    return GLKVector2Make(v3.v[0], v3.v[1]);
}

static inline GLKVector2 GLKVector2FromCGPoint(CGPoint point)
{
    return GLKVector2Make(point.x, point.y);
}

static inline CGPoint CGPointFromGLKVector2(GLKVector2 vector)
{
    return CGPointMake(vector.x, vector.y);
}

#ifdef __cplusplus
}
#endif

#endif /* __BE_TYPES_H */