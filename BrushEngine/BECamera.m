//
//  BECamera.m
//  BrushEngine
//
//  Created by Germano Guerrini on 03/03/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BECamera.h"
#import "BECamera_Internal.h"
#import "BESettings.h"
#import "BEGameObject.h"
#import "BEUtils.h"
#import "BETypes.h"
#import "BEDrawingKit.h"

@interface BECamera ()

@property (nonatomic, readonly, weak) BEGameObject *target;
@property (nonatomic, readonly) CGSize viewportSize;
@property (nonatomic, readonly) CGSize worldSize;
@property (nonatomic, readonly) CGRect innerFrame;
@property (nonatomic, readonly) CGRect outerFrame;
@property (nonatomic, readonly) float rotation;
@property (nonatomic, readonly) GLKVector2 translation;
@property (nonatomic, readonly) GLKVector2 minOuterFrameBound;
@property (nonatomic, readonly) GLKVector2 maxOuterFrameBound;
@property (nonatomic, readonly) BOOL viewMatrixNeedsUpdate;
@property (nonatomic, readonly) BOOL projectionMatrixNeedsUpdate;
@property (nonatomic, readonly) GLKMatrix4 orthographicMatrix;
@property (nonatomic, readonly) GLKMatrix4 cachedProjectionMatrix;
@property (nonatomic, readonly) GLKMatrix4 cachedViewMatrix;

- (void)focusTargetOnInnerFrameMiddlePoint;
- (void)clampTranslationToOuterFrame;

@end

@implementation BECamera

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize outerFrame:(CGRect)outerFrame innerFrame:(CGRect)innerFrame
{
    if ((self = [super init])) {
        _target = target;
        _viewportSize = viewportSize;
        _worldSize = worldSize;
        // The outerFrame must be contained into the viewport
        CGRect viewport = CGRectMake(0.0f, 0.0f, viewportSize.width, viewportSize.height);
        if (CGRectIsNull(outerFrame)) {
            _outerFrame = viewport;
        } else {
            // TODO This is probably not safe enough, we should be more restrictive
            _outerFrame = CGRectContainsRect(viewport, outerFrame) ? outerFrame : viewport;
        }
        // The innerFrame must be contained into the outerFrame
        if (CGRectIsNull(innerFrame)) {
            // By default, the innerFrame is a degenerated CGRect in the middle of the screen
            _innerFrame = CGRectMake(viewportSize.width / 2, viewportSize.height / 2, 0, 0);
        } else {
            _innerFrame = CGRectContainsRect(_outerFrame, innerFrame) ? innerFrame : _outerFrame;
        }
        _zoom = 1.0f;
        _rotation = 0.0f;
        _shake = GLKVector2Zero;
        _translation = GLKVector2Zero;
        
        _minOuterFrameBound.x = 2 * _viewportSize.width -
                                2 * _outerFrame.size.width -
                                2 * _outerFrame.origin.x -
                                _worldSize.width;
        _minOuterFrameBound.y = 2 * _viewportSize.height -
                                2 * _outerFrame.size.height -
                                2 * _outerFrame.origin.y -
                                _worldSize.height;
        _maxOuterFrameBound.x = - _outerFrame.origin.x;
        _maxOuterFrameBound.y = - _outerFrame.origin.y;
        
        _orthographicMatrix = GLKMatrix4MakeOrtho(0, _viewportSize.width, _viewportSize.height, 0.0f, -1.0f, 1.0f);
        if (_target != nil) {
            [self focusTargetOnInnerFrameMiddlePoint];
        }
        _cachedViewMatrix = GLKMatrix4MakeTranslation(_translation.x, _translation.y, 0.0f);
        _cachedProjectionMatrix = GLKMatrix4Multiply(_orthographicMatrix, _cachedViewMatrix);
    }
    return self;
}

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize innerFrame:(CGRect)innerFrame
{
    return [self initWithViewportSize:viewportSize target:target worldSize:worldSize outerFrame:CGRectNull innerFrame:innerFrame];
}

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize outerFrame:(CGRect)outerFrame
{
    return [self initWithViewportSize:viewportSize target:target worldSize:worldSize outerFrame:outerFrame innerFrame:CGRectNull];
}

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize
{
    return [self initWithViewportSize:viewportSize target:target worldSize:worldSize outerFrame:CGRectNull innerFrame:CGRectNull];
}

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target
{
    return [self initWithViewportSize:viewportSize target:target worldSize:viewportSize outerFrame:CGRectNull innerFrame:CGRectNull];
}

- (id)initWithViewportSize:(CGSize)viewportSize
{
    return [self initWithViewportSize:viewportSize target:nil worldSize:viewportSize outerFrame:CGRectNull innerFrame:CGRectNull];
}

- (void)setShake:(GLKVector2)shake
{
    if (!GLKVector2AllEqualToVector2(shake, _shake)) {
        _shake = shake;
        _viewMatrixNeedsUpdate = YES;
    }
}

- (void)setZoom:(float)zoom
{
    if (zoom != _zoom) {
        _zoom = zoom;
        _minOuterFrameBound = GLKVector2MultiplyScalar(_minOuterFrameBound, _zoom);
        _maxOuterFrameBound = GLKVector2MultiplyScalar(_maxOuterFrameBound, _zoom);
        _viewMatrixNeedsUpdate = YES;
    }
}

- (GLKMatrix4)projectionMatrix
{
    if (_projectionMatrixNeedsUpdate) {
        _cachedProjectionMatrix = GLKMatrix4Multiply(_orthographicMatrix, _cachedViewMatrix);
        _projectionMatrixNeedsUpdate = NO;
    }
    return _cachedProjectionMatrix;
}

- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint
{
    return GLKVector2Subtract(GLKVector2DivideScalar(localPoint, _zoom), _translation);
}

- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint
{
    return GLKVector2Add(GLKVector2MultiplyScalar(worldPoint, _zoom), _translation);
}

#pragma mark - BERendering Protocol Methods

- (void)render
{
#ifdef BE_DRAW_CAMERA_DEBUG
    CGAffineTransform transform = CGAffineTransformMakeTranslation(-_translation.x, -_translation.y);
    BEDrawRectWithColor(CGRectApplyAffineTransform(_innerFrame, transform), BEColorGreen);
    BEDrawRectWithColor(CGRectApplyAffineTransform(_outerFrame, transform), BEColorRed);
#endif
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    if (_target == nil) {
        return;
    }
    // TODO We should consider a target which is a child of a moving father. Should we invert its matrix?
    CGPoint localTargetPosition = CGPointFromGLKVector2([self worldPointToLocalSpace:_target.position]);
    
    if (!CGRectContainsPoint(_innerFrame, localTargetPosition)) {
        // Calculate a new matrix
        _translation.x = CLAMP(_translation.x,
                               _innerFrame.origin.x - _target.position.x, 
                               _innerFrame.origin.x + _innerFrame.size.width - _target.position.x);
        _translation.y = CLAMP(_translation.y,
                               _innerFrame.origin.y - _target.position.y,
                               _innerFrame.origin.y + _innerFrame.size.height - _target.position.y);
        _viewMatrixNeedsUpdate = YES;
    }
    
    if (_viewMatrixNeedsUpdate) {
        [self clampTranslationToOuterFrame];
        _cachedViewMatrix = GLKMatrix4MakeTranslation(_translation.x + _shake.x, _translation.y + _shake.y, 0.0f);
        if (_rotation != 0.0f) {
            _cachedViewMatrix = GLKMatrix4Multiply(GLKMatrix4MakeZRotation(GLKMathDegreesToRadians(_rotation)), _cachedViewMatrix);
        }
        if (_zoom != 1.0f) {
            _cachedViewMatrix = GLKMatrix4Multiply(GLKMatrix4MakeScale(_zoom, _zoom, 1.0f), _cachedViewMatrix);
        }
        _viewMatrixNeedsUpdate = NO;
        _projectionMatrixNeedsUpdate = YES;
    }
}

#pragma mark - Private Methods

- (void)focusTargetOnInnerFrameMiddlePoint
{
    _translation.x = _innerFrame.origin.x + _innerFrame.size.width / 2 - _target.position.x;
    _translation.y = _innerFrame.origin.y + _innerFrame.size.height / 2 - _target.position.y;
    [self clampTranslationToOuterFrame];
}

- (void)clampTranslationToOuterFrame
{
    _translation.x = CLAMP(_translation.x, _minOuterFrameBound.x, _maxOuterFrameBound.x);
    _translation.y = CLAMP(_translation.y, _minOuterFrameBound.y, _maxOuterFrameBound.y);
}

@end

#pragma mark - Internal

@implementation BECamera (Internal)

@dynamic innerFrame;
@dynamic outerFrame;

@end
