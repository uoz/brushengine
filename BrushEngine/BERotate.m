//
//  BERotate.m
//  BrushEngine
//
//  Created by Germano Guerrini on 25/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BERotate.h"
#import "BEGameObject.h"

#pragma mark - BERotate

@interface BERotate ()

@property (nonatomic, readonly) float rotation;
@property (nonatomic, readonly) float cachedRotation;
@property (nonatomic) float startAngle;
@property (nonatomic) float endAngle;

@end

@implementation BERotate

- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target duration:duration])) {
        _cachedRotation = INFINITY;
    }
    return self;
}

- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle
{
    return [self initWithTarget:target angleInRadians:angle duration:0.0f];
}

- (id)initWithTarget:(BEGameObject *)target angleInDegrees:(float)angle duration:(NSTimeInterval)duration
{
    return [self initWithTarget:target angleInRadians:DEGREES_TO_RADIANS(angle) duration:duration];
}

- (id)initWithTarget:(BEGameObject *)target angleInDegrees:(float)angle
{
    return [self initWithTarget:target angleInRadians:DEGREES_TO_RADIANS(angle) duration:0.0f];
}

- (float)rotation
{
    if (_cachedRotation == INFINITY) {
        _cachedRotation = self.endAngle - self.startAngle;
    }
    return _cachedRotation;
}

- (void)setup
{
    [super setup];
    self.startAngle = ((BEGameObject *)self.target).rotation;
}

- (void)stop
{
    [super stop];
    _cachedRotation = INFINITY;
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BEGameObject *)self.target).rotation = self.startAngle + self.rotation * offset;
}

@end

#pragma mark - BERotateTo

@implementation BERotateTo

- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target angleInRadians:angle duration:duration])) {
        // TODO We should % 360
        super.endAngle = angle;
    }
    return self;
}

@end

#pragma mark - BERotateBy

@interface BERotateBy ()

@property (nonatomic, readonly) float offset;

@end

@implementation BERotateBy

- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target angleInRadians:angle duration:duration])) {
        // TODO We should % 360
        _offset = angle;
    }
    return self;
}

- (void)setup
{
    [super setup];
    self.endAngle = self.startAngle + _offset;
}

@end