//
//  BECameraZoom.h
//  BrushEngine
//
//  Created by Germano Guerrini on 18/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEAction.h"

@class BECamera;

@interface BECameraZoom : BEAction

- (id)initWithCamera:(BECamera *)camera zoom:(float)zoom duration:(NSTimeInterval)duration;
- (id)initWithCamera:(BECamera *)camera zoom:(float)zoom;

@end
