//
//  main.m
//  BrushEngine
//
//  Created by Germano Guerrini on 26/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BEAppDelegate class]));
    }
}
