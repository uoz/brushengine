//
//  BEEffectLoader.h
//  BrushEngine
//
//  Created by Germano Guerrini on 07/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface BEEffectLoader : NSObject

+ (GLKBaseEffect *)effectWithKey:(NSString *)key;
+ (void)purgeCachedEffects;

@end
