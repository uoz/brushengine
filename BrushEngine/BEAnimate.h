//
//  BEAnimate.h
//  BrushEngine
//
//  Created by Germano Guerrini on 19/04/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEAction.h"

@class BEAnimatedSprite;

/** An action that performs the BEAnimation identified by key on the given BEAnimatedSprite instance. */
@interface BEAnimate : BEAction


/** Initializes a new action.
  
 @param target The `BEAnimatedSprite` the action will be performed on.
 @param key The key of the `BEAnimation` to perform.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEAnimatedSprite *)target animationKey:(NSString *)key;

@end
