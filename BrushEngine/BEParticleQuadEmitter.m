//
//  BEParticleQuadEmitter.m
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEParticleQuadEmitter.h"
#import "BETextureLoader.h"
#import "BEDictionaryAddictions.h"
#import "BEUtils.h"

#define MAXIMUM_UPDATE_RATE 25

@interface BEParticleQuadEmitter ()
{
    BEParticle *particles;
    BEParticle *currentParticle;
    int particleIndex;
    GLenum particleBlendSrc;
    GLenum particleBlendDst;
}
- (BOOL)addParticle;
- (void)initParticle:(BEParticle *)particle;
- (void)parseConfigData:(NSData *)configData error:(NSError **)errorPtr;
@end

@implementation BEParticleQuadEmitter

@synthesize sourcePosition;

- (id)initParticleEmitterWithFile:(NSString *)fileName
{
    if (fileName == nil) {
        BEDebugLog(@"Could not locate file %@", fileName);
        return nil;
    }
    NSError *error = nil;
    NSData *configData = [NSData dataWithContentsOfFile:fileName
                                                options:NSDataReadingUncached
                                                  error:&error];
    if (configData == nil) {
        BEDebugLog(@"%@", [error description]);
        return nil;
    }
    
    [self parseConfigData:configData error:&error];
    
    if ((self = [super initWithCapacity:maxParticles texture:_texture])) {
        particles = malloc(sizeof(BEParticle) * maxParticles);
        currentParticle = malloc(sizeof(BEParticle));
        // TODO We should not assert but throws an exception
        NSAssert(particles, @"ERROR - ParticleEmitter: Could not allocate arrays.");
        active = YES;        
        elapsedTime = 0;
        _blendFuncSource = particleBlendSrc;
        _blendFuncDestination = particleBlendDst;
    }
    return self;    
}

- (void)stopParticleEmitter
{
    active = NO;
	elapsedTime = 0;
	emitCounter = 0;    
}

- (void)initQuads
{
    // Set up texture coordinates for all particles as these will not change.
    for (int i = 0; i < maxParticles; i++) {
        _quads[i].vertices[0].textureCoords = GLKVector2Make(0.0f, 0.0f);
        _quads[i].vertices[1].textureCoords = GLKVector2Make(1.0f, 0.0f);
        _quads[i].vertices[2].textureCoords = GLKVector2Make(0.0f, 1.0f);
        _quads[i].vertices[3].textureCoords = GLKVector2Make(1.0f, 1.0f);
    }
}

- (void)dealloc
{
    free(currentParticle);
    free(particles);
}

#pragma mark - BERendering Protocol Methods

- (void)updateWithInterval:(NSTimeInterval)interval
{
    if (active && emissionRate) {
		float rate = 1.0f / emissionRate;
		emitCounter += interval;
		while (_activeQuadsCount < maxParticles && emitCounter > rate) {
			[self addParticle];
			emitCounter -= rate;
		}
        
		elapsedTime += interval;
		if (duration != -1 && duration < elapsedTime) {
            [self stopParticleEmitter];
        }
	}
	
	// Reset the particle index before updating the particles in this emitter
	particleIndex = 0;
    
    // Loop through all the particles updating their location and color
	while (particleIndex < _activeQuadsCount) {
        
		// Get the particle for the current particle index
		currentParticle = &particles[particleIndex];
        
        // FIX 1
        // Reduce the life span of the particle
        currentParticle->timeToLive -= interval;
		
		// If the current particle is alive then update it
		if (currentParticle->timeToLive > 0) {
			
			// If maxRadius is greater than 0 then the particles are going to spin otherwise
			// they are effected by speed and gravity
			if (emitterType == BEParticleEmitterTypeRadial) {
                
                // FIX 2
                // Update the angle of the particle from the sourcePosition and the radius.  This is only
				// done of the particles are rotating
				currentParticle->angle += currentParticle->degreesPerSecond * interval;
				currentParticle->radius -= currentParticle->radiusDelta;
                
				GLKVector2 tmp;
				tmp.x = sourcePosition.x - cosf(currentParticle->angle) * currentParticle->radius;
				tmp.y = sourcePosition.y - sinf(currentParticle->angle) * currentParticle->radius;
				currentParticle->position = tmp;
                
				if (currentParticle->radius < minRadius) {
                    currentParticle->timeToLive = 0;
                }
			} else if (emitterType == BEParticleEmitterTypeGravity) {
				GLKVector2 tmp, radial, tangential;
                
                radial = GLKVector2Zero;
                /* TODO Makes sense? */
                GLKVector2 diff = GLKVector2Subtract(currentParticle->startPos, GLKVector2Zero);
                
                currentParticle->position = GLKVector2Subtract(currentParticle->position, diff);
                
                if (currentParticle->position.x || currentParticle->position.y)
                    radial = GLKVector2Normalize(currentParticle->position);
                
                tangential.x = radial.x;
                tangential.y = radial.y;
                radial = GLKVector2MultiplyScalar(radial, currentParticle->radialAcceleration);
                
                GLfloat newy = tangential.x;
                tangential.x = -tangential.y;
                tangential.y = newy;
                tangential = GLKVector2MultiplyScalar(tangential, currentParticle->tangentialAcceleration);
                
				tmp = GLKVector2Add(GLKVector2Add(radial, tangential), gravity);
                tmp = GLKVector2MultiplyScalar(tmp, interval);
				currentParticle->direction = GLKVector2Add(currentParticle->direction, tmp);
				tmp = GLKVector2MultiplyScalar(currentParticle->direction, interval);
				currentParticle->position = GLKVector2Add(currentParticle->position, tmp);
                currentParticle->position = GLKVector2Add(currentParticle->position, diff);
			}
			
			// Update the particles color
			currentParticle->color.r += currentParticle->deltaColor.r;
			currentParticle->color.g += currentParticle->deltaColor.g;
			currentParticle->color.b += currentParticle->deltaColor.b;
			currentParticle->color.a += currentParticle->deltaColor.a;
			
			// Update the particle size
			currentParticle->particleSize += currentParticle->particleSizeDelta;
            
            // Update the rotation of the particle
            currentParticle->rotation += (currentParticle->rotationDelta * interval);
            
            // As we are rendering the particles as quads, we need to define 6 vertices for each particle
            GLfloat halfSize = currentParticle->particleSize * 0.5f;
            
            // If a rotation has been defined for this particle then apply the rotation to the vertices that define
            // the particle
            if (currentParticle->rotation) {
                float x1 = -halfSize;
                float y1 = -halfSize;
                float x2 = halfSize;
                float y2 = halfSize;
                float x = currentParticle->position.x;
                float y = currentParticle->position.y;
                float r = GLKMathDegreesToRadians(currentParticle->rotation);
                float cr = cosf(r);
                float sr = sinf(r);
                float ax = x1 * cr - y1 * sr + x;
                float ay = x1 * sr + y1 * cr + y;
                float bx = x2 * cr - y1 * sr + x;
                float by = x2 * sr + y1 * cr + y;
                float cx = x2 * cr - y2 * sr + x;
                float cy = x2 * sr + y2 * cr + y;
                float dx = x1 * cr - y2 * sr + x;
                float dy = x1 * sr + y2 * cr + y;
                
                _quads[particleIndex].vertices[0].position.x = ax;
                _quads[particleIndex].vertices[0].position.y = ay;
                _quads[particleIndex].vertices[0].color = currentParticle->color;
                
                _quads[particleIndex].vertices[1].position.x = bx;
                _quads[particleIndex].vertices[1].position.y = by;
                _quads[particleIndex].vertices[1].color = currentParticle->color;
                
                _quads[particleIndex].vertices[2].position.x = dx;
                _quads[particleIndex].vertices[2].position.y = dy;
                _quads[particleIndex].vertices[2].color = currentParticle->color;
                
                _quads[particleIndex].vertices[3].position.x = cx;
                _quads[particleIndex].vertices[3].position.y = cy;
                _quads[particleIndex].vertices[3].color = currentParticle->color;
                
            } else {
                // Using the position of the particle, work out the four vertices for the quad that will hold the particle
                // and load those into the quads array.
                _quads[particleIndex].vertices[0].position.x = currentParticle->position.x - halfSize;
                _quads[particleIndex].vertices[0].position.y = currentParticle->position.y - halfSize;
                _quads[particleIndex].vertices[0].color = currentParticle->color;
                
                _quads[particleIndex].vertices[1].position.x = currentParticle->position.x + halfSize;
                _quads[particleIndex].vertices[1].position.y = currentParticle->position.y - halfSize;
                _quads[particleIndex].vertices[1].color = currentParticle->color;
                
                _quads[particleIndex].vertices[2].position.x = currentParticle->position.x - halfSize;
                _quads[particleIndex].vertices[2].position.y = currentParticle->position.y + halfSize;
                _quads[particleIndex].vertices[2].color = currentParticle->color;
                
                _quads[particleIndex].vertices[3].position.x = currentParticle->position.x + halfSize;
                _quads[particleIndex].vertices[3].position.y = currentParticle->position.y + halfSize;
                _quads[particleIndex].vertices[3].color = currentParticle->color;
            }
            
			// Update the particle and vertex counters
			particleIndex++;
		} else {
			// As the particle is not alive anymore replace it with the last active particle 
			// in the array and reduce the count of particles by one.  This causes all active particles
			// to be packed together at the start of the array so that a particle which has run out of
			// life will only drop into this clause once
            [self disableQuadAtIndex:particleIndex];
			if (particleIndex != _activeQuadsCount - 1) {
                particles[particleIndex] = particles[_activeQuadsCount - 1];
            }
		}
	}
}

#pragma mark - Private Methods

- (BOOL)addParticle
{
    // If we have already reached the maximum number of particles then do nothing
	if (_activeQuadsCount == maxParticles) {
        return NO;
    }
	
	// Take the next particle out of the particle pool we have created and initialize it
	BEParticle *particle = &particles[_activeQuadsCount];
	[self initParticle:particle];
	
	// Increment the particle count
	_activeQuadsCount++;
	
	// Return YES to show that a particle has been created
	return YES;
}

- (void)initParticle:(BEParticle *)particle
{
    // Init the position of the particle.  This is based on the source position of the particle emitter
	// plus a configured variance.  The RANDOM_MINUS_1_1 macro allows the number to be both positive
	// and negative
	particle->position.x = sourcePosition.x + sourcePositionVariance.x * RANDOM_MINUS_1_1;
	particle->position.y = sourcePosition.y + sourcePositionVariance.y * RANDOM_MINUS_1_1;
    particle->startPos.x = sourcePosition.x;
    particle->startPos.y = sourcePosition.y;
	
	// Init the direction of the particle.  The newAngle is calculated using the angle passed in and the
	// angle variance.
	float newAngle = GLKMathDegreesToRadians(angle + angleVariance * RANDOM_MINUS_1_1);
	
	// Create a new Vector2f using the newAngle
	GLKVector2 vector = GLKVector2Make(cosf(newAngle), sinf(newAngle));
	
	// Calculate the vectorSpeed using the speed and speedVariance which has been passed in
	float vectorSpeed = speed + speedVariance * RANDOM_MINUS_1_1;
	
	// The particles direction vector is calculated by taking the vector calculated above and
	// multiplying that by the speed
	particle->direction = GLKVector2MultiplyScalar(vector, vectorSpeed);
	
	// Set the default diameter of the particle from the source position
	particle->radius = maxRadius + maxRadiusVariance * RANDOM_MINUS_1_1;
	particle->radiusDelta = (maxRadius / particleLifeSpan) * (1.0 / MAXIMUM_UPDATE_RATE);
	particle->angle = GLKMathDegreesToRadians(angle + angleVariance * RANDOM_MINUS_1_1);
	particle->degreesPerSecond = GLKMathDegreesToRadians(rotatePerSecond + rotatePerSecondVariance * RANDOM_MINUS_1_1);
    
    particle->radialAcceleration = radialAcceleration;
    particle->tangentialAcceleration = tangentialAcceleration;
	
	// Calculate the particles life span using the life span and variance passed in
	particle->timeToLive = MAX(0, particleLifeSpan + particleLifeSpanVariance * RANDOM_MINUS_1_1);
	
	// Calculate the particle size using the start and finish particle sizes
	GLfloat particleStartSize = startParticleSize + startParticleSizeVariance * RANDOM_MINUS_1_1;
	GLfloat particleFinishSize = finishParticleSize + finishParticleSizeVariance * RANDOM_MINUS_1_1;
	particle->particleSizeDelta = ((particleFinishSize - particleStartSize) / particle->timeToLive) * (1.0 / MAXIMUM_UPDATE_RATE);
	particle->particleSize = MAX(0, particleStartSize);
	
	// Calculate the color the particle should have when it starts its life.  All the elements
	// of the start color passed in along with the variance are used to calculate the star color
	GLKVector4 start = {0, 0, 0, 0};
	start.r = startColor.r + startColorVariance.r * RANDOM_MINUS_1_1;
	start.g = startColor.g + startColorVariance.g * RANDOM_MINUS_1_1;
	start.b = startColor.b + startColorVariance.b * RANDOM_MINUS_1_1;
	start.a = startColor.a + startColorVariance.a * RANDOM_MINUS_1_1;
	
	// Calculate the color the particle should be when its life is over.  This is done the same
	// way as the start color above
	GLKVector4 end = GLKVector4Zero;
	end.r = finishColor.r + finishColorVariance.r * RANDOM_MINUS_1_1;
	end.g = finishColor.g + finishColorVariance.g * RANDOM_MINUS_1_1;
	end.b = finishColor.b + finishColorVariance.b * RANDOM_MINUS_1_1;
	end.a = finishColor.a + finishColorVariance.a * RANDOM_MINUS_1_1;
	
	// Calculate the delta which is to be applied to the particles color during each cycle of its
	// life.  The delta calculation uses the life span of the particle to make sure that the 
	// particles color will transition from the start to end color during its life time.  As the game
	// loop is using a fixed delta value we can calculate the delta color once saving cycles in the 
	// update method
	particle->color = start;
	particle->deltaColor.r = ((end.r - start.r) / particle->timeToLive) * (1.0 / MAXIMUM_UPDATE_RATE);
	particle->deltaColor.g = ((end.g - start.g) / particle->timeToLive) * (1.0 / MAXIMUM_UPDATE_RATE);
	particle->deltaColor.b = ((end.b - start.b) / particle->timeToLive) * (1.0 / MAXIMUM_UPDATE_RATE);
	particle->deltaColor.a = ((end.a - start.a) / particle->timeToLive) * (1.0 / MAXIMUM_UPDATE_RATE);
    
    // Calculate the rotation
    GLfloat startA = rotationStart + rotationStartVariance * RANDOM_MINUS_1_1;
    GLfloat endA = rotationEnd + rotationEndVariance * RANDOM_MINUS_1_1;
    particle->rotation = startA;
    particle->rotationDelta = (endA - startA) / particle->timeToLive;
}

- (void)parseConfigData:(NSData *)configData error:(NSError **)errorPtr
{
    NSError *error = nil;
 	NSDictionary *config = [NSJSONSerialization JSONObjectWithData:configData
                                                           options:NSJSONReadingMutableLeaves
                                                             error:&error];
    if (error) {
        BEDebugLog(@"%@", [error description]);
        // TODO Handle errorPtr
    }
    
    NSArray *positionKeys = [NSArray arrayWithObjects:@"x", @"y", nil];
    NSArray *colorKeys = [NSArray arrayWithObjects:@"r", @"g", @"b", @"a", nil];
    
    _texture = [BETextureLoader textureWithContentsOfFile:[config objectForKey:@"texture"]];
    
    emitterType = [config intValueFromObjectForKey:@"emitterType"];
    
	sourcePosition = GLKVector2MakeWithDictionary([config objectForKey:@"sourcePosition"], positionKeys);
    sourcePositionVariance = GLKVector2MakeWithDictionary([config objectForKey:@"sourcePositionVariance"], positionKeys);
    
    speed = [config floatValueFromObjectForKey:@"speed"];
    speedVariance = [config floatValueFromObjectForKey:@"speedVariance"];
    particleLifeSpan = [config floatValueFromObjectForKey:@"particleLifeSpan"];
    particleLifeSpanVariance = [config floatValueFromObjectForKey:@"particleLifeSpanVariance"];
    angle = [config floatValueFromObjectForKey:@"angle"];
    angleVariance = [config floatValueFromObjectForKey:@"angleVariance"];
    
    gravity = GLKVector2MakeWithDictionary([config objectForKey:@"gravity"], positionKeys);
    
    radialAcceleration = [config floatValueFromObjectForKey:@"radialAcceleration"];
    radialAccelVariance = [config floatValueFromObjectForKey:@"radialAccelVariance"];
    
    tangentialAcceleration  = [config floatValueFromObjectForKey:@"tangentialAcceleration"];
    tangentialAccelVariance = [config floatValueFromObjectForKey:@"tangentialAccelVariance"];
    
    startColor = GLKVector4MakeWithDictionary([config objectForKey:@"startColor"], colorKeys);
    startColorVariance = GLKVector4MakeWithDictionary([config objectForKey:@"startColorVariance"], colorKeys);
    finishColor = GLKVector4MakeWithDictionary([config objectForKey:@"finishColor"], colorKeys);
    finishColorVariance = GLKVector4MakeWithDictionary([config objectForKey:@"finishColorVariance"], colorKeys);
    
    maxParticles = [config floatValueFromObjectForKey:@"maxParticles"];
	startParticleSize = [config floatValueFromObjectForKey:@"startParticleSize"];
	startParticleSizeVariance = [config floatValueFromObjectForKey:@"startParticleSizeVariance"];
	finishParticleSize = [config floatValueFromObjectForKey:@"finishParticleSize"];
	finishParticleSizeVariance = [config floatValueFromObjectForKey:@"finishParticleSizeVariance"];
    
    duration = [config floatValueFromObjectForKey:@"duration"];
    particleBlendSrc = [config intValueFromObjectForKey:@"blendFuncSource"];
    particleBlendDst = [config intValueFromObjectForKey:@"blendFuncDestination"];
    
    maxRadius = [config floatValueFromObjectForKey:@"maxRadius"];
    maxRadiusVariance = [config floatValueFromObjectForKey:@"maxRadiusVariance"];
    radiusSpeed = [config floatValueFromObjectForKey:@"radiusSpeed"];
    minRadius = [config floatValueFromObjectForKey:@"minRadius"];
    rotatePerSecond = [config floatValueFromObjectForKey:@"rotatePerSecond"];
    rotatePerSecondVariance = [config floatValueFromObjectForKey:@"rotatePerSecondVariance"];
    
    rotationStart = [config floatValueFromObjectForKey:@"rotationStart"];
    rotationStartVariance = [config floatValueFromObjectForKey:@"rotationStartVariance"];
    rotationEnd = [config floatValueFromObjectForKey:@"rotationEnd"];
    rotationEndVariance = [config floatValueFromObjectForKey:@"rotationEndVariance"];
	
	// Calculate the emission rate
	emissionRate = maxParticles / particleLifeSpan;
}

@end
