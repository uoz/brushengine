//
//  BESprite.m
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BESprite.h"
#import "BESettings.h"
#import "BEUtils.h"
#import "BEOpenGLStateCache.h"
#import "BETextureAtlas.h"
#import "BEEffectLoader.h"
#import "BETextureLoader.h"
#import "BESpriteBatch.h"
#import "BEProfiler.h"
#import "BEDrawingKit.h"

@interface BESprite ()

@property (nonatomic, readonly) GLuint vertexBuffer;
@property (nonatomic, readonly) BOOL sizeNeedsUpdate;
@property (nonatomic, readonly) BOOL colorNeedsUpdate;
@property (nonatomic, readonly) BOOL alphaNeedsUpdate;
@property (nonatomic, readonly) BOOL textureNeedsUpdate;
@property (nonatomic, readonly) BOOL textureCoordinatesNeedsUpdate;
@property (nonatomic, readonly) BOOL isVertexBufferInitizialized;
@property (nonatomic, readonly) CGRect cachedUvMapping;

- (void)initVertexBuffer;
- (void)deleteVertexBuffer;
- (void)updatePivot;
- (void)initQuad;
- (void)updateQuad;
- (CGRect)uvMapping;

@end


@implementation BESprite

// Resolve some clash
@synthesize alpha = _alpha;

#pragma mark - Init Methods

- (id)initWithTexture:(GLKTextureInfo *)texture textureCoordinates:(CGRect)textureCoordinates
{
    if ((self = [super init])) {
        _isVertexBufferInitizialized = NO;
        _cachedUvMapping = CGRectNull;
        
        /* TODO check if sizes are pow of 2. */

        _texture = texture;
        _textureCoordinates = textureCoordinates;
        _size = textureCoordinates.size;
        _alpha = 1.0f;
        _color = GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
        _effect = [BEEffectLoader effectWithKey:BE_DEFAULT_EFFECT];
        [self updatePivot];
        [self initQuad];
        [self initVertexBuffer];
    }
    return self;
}

- (id)initWithTexture:(GLKTextureInfo *)texture
{
    CGRect textureCoordinates = CGRectMake(0.0f, 0.0f, texture.width, texture.height);
    return [self initWithTexture:texture textureCoordinates:textureCoordinates];
}

- (id)initWithContentsOfFile:(NSString *)fileName textureCoordinates:(CGRect)textureCoordinates
{
    GLKTextureInfo *texture = [BETextureLoader textureWithContentsOfFile:fileName];
    return [self initWithTexture:texture textureCoordinates:textureCoordinates];
}

- (id)initWithContentsOfFile:(NSString *)fileName
{
    GLKTextureInfo *texture = [BETextureLoader textureWithContentsOfFile:fileName];
    return [self initWithTexture:texture];
}

- (id)initWithTextureAtlas:(BETextureAtlas *)textureAtlas key:(NSString *)key
{
    return [self initWithTexture:textureAtlas.texture
              textureCoordinates:[textureAtlas coordsForKey:key]];
}

- (id)initWithBatch:(BESpriteBatch *)batch key:(NSString *)key
{
    if ((self = [self initWithTexture:batch.textureAtlas.texture
                   textureCoordinates:[batch.textureAtlas coordsForKey:key]])) {
        self.batch = batch;
    }
    return self;
}

#pragma mark - Parameters Setters

- (void)setBatch:(BESpriteBatch *)batch
{
    NSAssert(_batch == nil, @"Cannot reassign the batch ivar of a BESprite");
    NSAssert(batch.texture.name == _texture.name, @"Cannot assign a batch with a different texture.");
    _batch = batch;
    [_batch addDescendant:self];
    [self deleteVertexBuffer];
}

- (void)setColor:(GLKVector4)color
{
    if (!GLKVector4AllEqualToVector4(_color, color)) {
        _color = color;
        _colorNeedsUpdate = YES;
    }
}

- (void)setAlpha:(float)alpha
{
    float clampedAlpha = CLAMP(alpha, 0.0f, 1.0f);
    if (clampedAlpha != _alpha) {
        _alpha = clampedAlpha;
        _alphaNeedsUpdate = YES;
        self.isVisible = (_alpha == 0.0f) ? NO : YES;
    }
}

- (void)setSize:(CGSize)size
{
    if (!CGSizeEqualToSize(size, _size)) {
        _size = size;
        [self updatePivot];
        _sizeNeedsUpdate = YES;
    }
}

- (void)setTexture:(GLKTextureInfo *)texture
{
    if (texture != _texture) {
        _texture = texture;
        _textureNeedsUpdate = YES;
        _textureCoordinatesNeedsUpdate = YES;
    }
}

- (void)setTextureCoordinates:(CGRect)textureCoordinates
{
    if (!CGRectEqualToRect(textureCoordinates, _textureCoordinates)) {
        _textureCoordinates = textureCoordinates;
        _textureCoordinatesNeedsUpdate = YES;
    }
}

#pragma mark - Rendering Protocol Methods

- (void)updateTransform
{
    [self updateQuad];
    if (_batch == nil) {
        [super updateTransform];
    } else {
        self.modelViewMatrix = [self localToWorldMatrix];
    }
}

- (void)draw
{
    if (_batch != nil || !_isVertexBufferInitizialized) {
        return;
    }
    
    _effect.texture2d0.name = self.texture.name;
    _effect.texture2d0.envMode = GLKTextureEnvModeModulate; /* TODO Experiment to see how expensive it is. */
    _effect.light0.enabled = GL_FALSE;
    _effect.transform.projectionMatrix = self.renderingManager.projectionMatrix;
    _effect.transform.modelviewMatrix = self.modelViewMatrix;
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(BETexturedColoredQuad), &_quad);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, position));
    
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, color));
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(BETexturedColoredVertex), (void *)offsetof(BETexturedColoredVertex, textureCoords));
    
    [_effect prepareToDraw];
    
    BEglBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //BEDrawQuad(_quad); TODO Flag
    
#ifdef BE_PROFILER_ENABLED
    [[BEProfiler sharedProfiler] incrementTextureSwitchCount];
#endif
}

#pragma mark - Private Methods

- (void)initVertexBuffer
{
    if (_batch == nil && !_isVertexBufferInitizialized) {
        glGenBuffers(1, &_vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(BETexturedColoredQuad), &_quad, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        _isVertexBufferInitizialized = YES;
    }
}

- (void)deleteVertexBuffer
{
    if (_batch != nil && _isVertexBufferInitizialized) {
        glDeleteBuffers(1, &_vertexBuffer);
        _isVertexBufferInitizialized = NO;
    }
}

- (void)updatePivot
{
    _pivot = GLKVector2Make(_size.width / 2, _size.height / 2);
}

- (void)initQuad
{
    CGRect uv = [self uvMapping];
    GLKVector4 blackColor = GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
    _quad = BETexturedColoredQuadMake(
        BETexturedColoredVertexMake(GLKVector2Make(0.0f, 0.0f),
                                    blackColor, GLKVector2Make(uv.origin.x, uv.origin.y)),
        BETexturedColoredVertexMake(GLKVector2Make(_size.width, 0.0f),
                                    blackColor, GLKVector2Make(uv.size.width, uv.origin.y)),
        BETexturedColoredVertexMake(GLKVector2Make(0.0f, _size.height),
                                    blackColor, GLKVector2Make(uv.origin.x, uv.size.height)),
        BETexturedColoredVertexMake(GLKVector2Make(_size.width, _size.height),
                                    blackColor, GLKVector2Make(uv.size.width, uv.size.height))
    );
}

- (void)updateQuad
{
    if (_sizeNeedsUpdate) {
        _quad.tlVertex.position = GLKVector2Make(0.0f, 0.0f);
        _quad.trVertex.position = GLKVector2Make(_size.width, 0.0f);
        _quad.blVertex.position = GLKVector2Make(0.0f, _size.height);
        _quad.brVertex.position = GLKVector2Make(_size.width, _size.height);
        _sizeNeedsUpdate = NO;
    }
    
    if (_colorNeedsUpdate) {
        _quad.tlVertex.color = _color;
        _quad.trVertex.color = _color;
        _quad.blVertex.color = _color;
        _quad.brVertex.color = _color;
        _colorNeedsUpdate = NO;
    }
    
    if (_alphaNeedsUpdate) {
        _quad.tlVertex.color.a = _alpha;
        _quad.trVertex.color.a = _alpha;
        _quad.blVertex.color.a = _alpha;
        _quad.brVertex.color.a = _alpha;
        _alphaNeedsUpdate = NO;
    }
    
    if (_textureCoordinatesNeedsUpdate) {
        CGRect uv = [self uvMapping];
        _quad.tlVertex.textureCoords.x = uv.origin.x;
        _quad.tlVertex.textureCoords.y = uv.origin.y;
        _quad.trVertex.textureCoords.x = uv.size.width;
        _quad.trVertex.textureCoords.y = uv.origin.y;
        _quad.blVertex.textureCoords.x = uv.origin.x;
        _quad.blVertex.textureCoords.y = uv.size.height;
        _quad.brVertex.textureCoords.x = uv.size.width;
        _quad.brVertex.textureCoords.y = uv.size.height;
        _textureCoordinatesNeedsUpdate = NO;
    }
}

- (CGRect)uvMapping
{
    if (CGRectIsNull(_cachedUvMapping) || _textureNeedsUpdate || _textureCoordinatesNeedsUpdate) {
        float uOrigin = _textureCoordinates.origin.x / _texture.width;
        float vOrigin = _textureCoordinates.origin.y / _texture.height;
        
        float uSize = (_textureCoordinates.origin.x + _textureCoordinates.size.width) / _texture.width;
        float vSize = (_textureCoordinates.origin.y + _textureCoordinates.size.height) / _texture.height;
        
        _cachedUvMapping = CGRectMake(uOrigin, vOrigin, uSize, vSize);
        _textureNeedsUpdate = NO;
        _textureCoordinatesNeedsUpdate = NO;
    }
    return _cachedUvMapping;
}

@end