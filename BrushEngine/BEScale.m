//
//  BEScale.m
//  BrushEngine
//
//  Created by Germano Guerrini on 27/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEScale.h"
#import "BEGameObject.h"

#pragma mark - BEScale

@interface BEScale ()

@property (nonatomic, readonly) GLKVector2 resize;
@property (nonatomic, readonly) GLKVector2 cachedResize;
@property (nonatomic) GLKVector2 startScale;
@property (nonatomic) GLKVector2 endScale;

@end

@implementation BEScale

- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target duration:duration])) {
        _cachedResize = GLKVector2Make(INFINITY, INFINITY);
    }
    return self;
}

- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale
{
    return [self initWithTarget:target scale:scale duration:0.0f];
}

- (GLKVector2)resize
{
    if (GLKVector2AllEqualToScalar(_cachedResize, INFINITY)) {
        _cachedResize = GLKVector2Subtract(self.endScale, self.startScale);
    }
    return _cachedResize;
}

- (void)setup
{
    [super setup];
    self.startScale = ((BEGameObject *)self.target).scale;
}

- (void)stop
{
    [super stop];
    _cachedResize = GLKVector2Make(INFINITY, INFINITY);
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BEGameObject *)self.target).scale = GLKVector2Add(self.startScale, GLKVector2MultiplyScalar(self.resize, offset));
}

@end

#pragma mark - BEScaleTo

@implementation BEScaleTo

- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target scale:scale duration:duration])) {
        self.endScale = scale;
    }
    return self;
}

@end

#pragma mark - BEScaleBy

@interface BEScaleBy ()

@property (nonatomic, readonly) GLKVector2 factor;

@end

@implementation BEScaleBy

- (id)initWithTarget:(BEGameObject *)target scale:(GLKVector2)scale duration:(NSTimeInterval)duration
{
    if ((self = [super initWithTarget:target scale:scale duration:duration])) {
        _factor = scale;
    }
    return self;
}

- (void)setup
{
    [super setup];
    self.endScale = GLKVector2Multiply(self.startScale, _factor);
}

@end

#pragma mark - BEFlip

@implementation BEFlip

- (id)initWithTarget:(BEGameObject *)target flipAxis:(BEFlipAxis)axis
{
    switch (axis) {
        case BEFlipAxisX:
            return [super initWithTarget:target scale:GLKVector2Make(-1.0f, 1.0f)];
        case BEFlipAxisY:
            return [super initWithTarget:target scale:GLKVector2Make(1.0f, -1.0f)];
        default:
            return [super initWithTarget:target scale:GLKVector2Make(-1.0f, -1.0f)];
    }
}

@end