//
//  BEColorize.m
//  BrushEngine
//
//  Created by Germano Guerrini on 03/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEColorize.h"
#import "BESprite.h"

@interface BEColorize ()

@property (nonatomic, readonly) GLKVector4 startColor;
@property (nonatomic, readonly) GLKVector4 endColor;
@property (nonatomic, readonly) GLKVector4 variance;
@property (nonatomic, readonly) GLKVector4 cachedVariance;

@end

@implementation BEColorize

- (id)initWithTarget:(BEGameObject *)target color:(GLKVector4)color duration:(NSTimeInterval)duration
{
    if (![target respondsToSelector:@selector(color)]) {
        // TODO Better logging
        NSLog(@"BEColorize: you should pass a BESprite or a subclass of it");
        return nil;
    }
    if ((self = [super initWithTarget:target duration:duration])) {
        _endColor = color;
        _cachedVariance = GLKVector4Make(INFINITY, INFINITY, INFINITY, INFINITY);
    }
    return self;
}

- (id)initWithTarget:(BEGameObject *)target color:(GLKVector4)color
{
    return [self initWithTarget:target color:color duration:0.0f];
}

- (GLKVector4)variance
{
    if (GLKVector4AllEqualToScalar(_cachedVariance, INFINITY)) {
        _cachedVariance = GLKVector4Subtract(_endColor, _startColor);
    }
    return _cachedVariance;
}

- (void)setup
{
    [super setup];
    _startColor = ((BESprite *)self.target).color;
}

- (void)stop
{
    [super stop];
    _cachedVariance = GLKVector4Make(INFINITY, INFINITY, INFINITY, INFINITY);
}

- (void)advanceToOffset:(NSTimeInterval)offset
{
    ((BESprite *)self.target).color = GLKVector4Add(_startColor, GLKVector4MultiplyScalar(self.variance, offset));
}

@end
