//
//  BEActionManager.m
//  BrushEngine
//
//  Created by Germano Guerrini on 13/03/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

//
// http://www.ultrajoke.net/2011/08/integers-in-your-collections-nsnumbers-not-my-friend/

/*
 [target.targetID]; [action, ...]
 [0] : [action1, action2]
 [1] : [action3]
 */


#import "BEActionManager.h"
#import "BEActionManager_Internal.h"
#import "BETarget.h"

#define BE_ACTION_ARRAY_INITIAL_CAPACITY 16

@interface BEActionManager ()

@property (nonatomic, readonly) CFMutableDictionaryRef targetDict;
@property (nonatomic, readonly) BEActionState state;
@property (nonatomic, readonly) SEL playActionSelector;
@property (nonatomic, readonly) SEL pauseActionSelector;
@property (nonatomic, readonly) SEL stopActionSelector;

@end

@implementation BEActionManager

@synthesize delegate;

+ (BEActionManager *)sharedActionManager
{
    static BEActionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BEActionManager alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if ((self = [super init])) {
        _targetDict = CFDictionaryCreateMutable(NULL, 0, NULL, &kCFTypeDictionaryValueCallBacks);
        _playActionSelector = NSSelectorFromString(@"play");
        _pauseActionSelector = NSSelectorFromString(@"pause");
        _stopActionSelector = NSSelectorFromString(@"stop");
        _state = BEActionStateRunning;
    }
    return self;
}

#pragma mark - Registering Methods

- (void)registerAction:(BEAction *)action
{
    CFMutableArrayRef actionArray = NULL;
    int key = ((BETarget *)action.target).targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        CFArrayAppendValue(actionArray, (__bridge const void *)(action));
    } else {
        actionArray = CFArrayCreateMutable(NULL, BE_ACTION_ARRAY_INITIAL_CAPACITY, &kCFTypeArrayCallBacks);
        CFArrayAppendValue(actionArray, (__bridge const void *)(action));
        CFDictionarySetValue(_targetDict, (void *)key, actionArray);
    }
}

- (void)unregisterAction:(BEAction *)action
{
    CFMutableArrayRef actionArray = NULL;
    int key = ((BETarget *)action.target).targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        CFRange range = CFRangeMake(0, CFArrayGetCount(actionArray));
        CFIndex index = CFArrayGetFirstIndexOfValue(actionArray, range, (__bridge const void *)(action));
        if (index != -1) {
            CFArrayRemoveValueAtIndex(actionArray, index);
        }
    }
}

- (void)unregisterActionsWithTarget:(id)target
{
    int key = ((BETarget *)target).targetID;
    CFDictionaryRemoveValue(_targetDict, (void *)key);
    // TODO Check if actions are deallocated
}

- (void)unregisterAllActions
{
    CFDictionaryRemoveAllValues(_targetDict);
    // TODO Check if actions are deallocated
}

#pragma mark - BEAction-like Methods

- (void)play
{
    _state = BEActionStateRunning;
}

- (void)pause
{
    _state = BEActionStatePaused;
}

- (void)stop
{
    _state = BEActionStateStopped;
    // TODO Dealloc?
}

- (void)toggle
{
    if (_state == BEActionStateRunning) {
        [self pause];
    } else if (_state == BEActionStatePaused) {
        [self play];
    }
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    if (_state == BEActionStateRunning) {
        CFDictionaryApplyFunction(_targetDict, updateTargetWithInterval, (void *)&interval);
    }
}

#pragma mark - Target Manipulation Methods

- (void)playActionsWithTarget:(id)target
{
    [self performSelector:_playActionSelector onActionsWithTarget:target];
}

- (void)pauseActionsWithTarget:(id)target
{
    [self performSelector:_pauseActionSelector onActionsWithTarget:target];
}

- (void)stopActionsWithTarget:(id)target
{
    [self performSelector:_stopActionSelector onActionsWithTarget:target];
}

- (NSSet *)pauseRunningActionsWithTarget:(id)target
{
    NSMutableSet *set = [NSMutableSet setWithCapacity:16];
    CFMutableArrayRef actionArray = NULL;
    int key = ((BETarget *)target).targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        CFIndex count = CFArrayGetCount(actionArray);
        for (CFIndex i = 0; i < count; i++) {
            BEAction *currentAction = (BEAction *)CFArrayGetValueAtIndex(actionArray, i);
            if ([currentAction isRunning]) {
                [currentAction pause];
                [set addObject:currentAction];
            }
        }
    }
    return set;
}

- (void)playActions:(NSSet *)actions withTarget:(id)target
{
    CFMutableArrayRef actionArray = NULL;
    int key = ((BETarget *)target).targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        CFIndex count = CFArrayGetCount(actionArray);
        for (CFIndex i = 0; i < count; i++) {
            BEAction *currentAction = (BEAction *)CFArrayGetValueAtIndex(actionArray, i);
            if ([actions containsObject:currentAction]) {
                [currentAction play];
            }
        }
    }
}

#pragma mark - Private Methods

- (void)performSelector:(SEL)selector onActionsWithTarget:(id)target
{
    CFMutableArrayRef actionArray = NULL;
    int key = ((BETarget *)target).targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        CFIndex count = CFArrayGetCount(actionArray);
        for (CFIndex i = 0; i < count; i++) {
            SuppressPerformSelectorLeakWarning([(BEAction *)CFArrayGetValueAtIndex(actionArray, i) performSelector:selector]);
        }
    }
}

static void updateTargetWithInterval(const void *targetID, const void *actionArray, void *interval)
{
    CFIndex count = CFArrayGetCount(actionArray);
    for (CFIndex i = 0; i < count; i++) {
        [(BEAction *)CFArrayGetValueAtIndex(actionArray, i) updateWithInterval:*(NSTimeInterval *)interval];
    }
}

@end

#pragma mark - Internal

@implementation BEActionManager (Internal)

- (NSUInteger)registeredTargetCount
{
    return CFDictionaryGetCount(_targetDict);
}

- (NSUInteger)actionsWithTargetCount:(BETarget *)target
{
    CFMutableArrayRef actionArray = NULL;
    int key = target.targetID;
    if (CFDictionaryGetValueIfPresent(_targetDict, (void *)key, (void *)&actionArray)) {
        return CFArrayGetCount(actionArray);
    }
    return 0;
}

@end