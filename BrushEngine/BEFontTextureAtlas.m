//
//  BEFontTextureAtlas.m
//  BrushEngine
//
//  Created by Germano Guerrini on 28/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEFontTextureAtlas.h"

@interface BEFontTextureAtlas ()

@property (nonatomic, readonly) BEGlyph *glyphs;
@property (nonatomic, readonly) BEFontProperties fontProperties;

- (BEGlyph)glyphForKey:(NSString *)key;
- (void)parseCommonAttributes:(NSString *)line intoProperties:(BEFontProperties *)properties;
- (void)parseGlyphLine:(NSString *)line intoGlyph:(BEGlyph *)glyph;

@end

@implementation BEFontTextureAtlas

- (id)initWithTexture:(GLKTextureInfo *)texture controlFile:(NSString *)controlFile
{
    return [super initWithTexture:texture controlFile:controlFile];
}

- (void)parseControlFile:(NSString *)controlFile
{
    NSString *content = [NSString stringWithContentsOfFile:controlFile encoding:NSASCIIStringEncoding error:nil];
    NSArray *lines = [content componentsSeparatedByString:@"\n"];
    
    _glyphs = calloc(256, sizeof(BEGlyph));
    
    [lines enumerateObjectsUsingBlock:^(NSString *line, NSUInteger idx, BOOL *stop) {
        if ([line hasPrefix:@"common"]) {
            [self parseCommonAttributes:line intoProperties:&_fontProperties];
        } else if ([line hasPrefix:@"char id"]) {
            BEGlyph glyph;
            [self parseGlyphLine:line intoGlyph:&glyph];
            _glyphs[glyph.charID - 32] = glyph;
        }
    }];
}

- (BEGlyph)glyphForChar:(unichar)character
{
    return _glyphs[(int)character - 32];
}

- (CGRect)coordsForChar:(unichar)character
{
    BEGlyph glyph = [self glyphForChar:character];
    return CGRectMake(glyph.x, glyph.y, glyph.width, glyph.height);
}

- (CGSize)sizeForChar:(unichar)character
{
    BEGlyph glyph = [self glyphForChar:character];
    return CGSizeMake(glyph.width, glyph.height);
}

- (void)dealloc
{
    free(_glyphs);
}

#pragma mark - Override

// This method has been implemented for completeness, but coordsForChar is to prefer.
- (CGRect)coordsForKey:(NSString *)key
{
    BEGlyph glyph = [self glyphForKey:key];
    return CGRectMake(glyph.x, glyph.y, glyph.width, glyph.height);
}

// This method has been implemented for completeness, but sizeForChar is to prefer.
- (CGSize)sizeForKey:(NSString *)key
{
    BEGlyph glyph = [self glyphForKey:key];
    return CGSizeMake(glyph.width, glyph.height);
}

#pragma mark - Private Methods

- (BEGlyph)glyphForKey:(NSString *)key
{
    return _glyphs[(int)[key characterAtIndex:0] - 32];
}

- (void)parseCommonAttributes:(NSString *)line intoProperties:(BEFontProperties *)properties
{
    NSScanner *scanner = [NSScanner scannerWithString:line];
    
    NSString *lineHeight;
    NSString *base;
    
    [scanner scanString:@"lineHeight=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&lineHeight];
    
    [scanner scanString:@"base=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&base];
    
    properties->lineHeight = [lineHeight intValue];
    properties->base = [base intValue];
}

- (void)parseGlyphLine:(NSString *)line intoGlyph:(BEGlyph *)glyph
{
    NSScanner *scanner = [NSScanner scannerWithString:line];
    
    NSString *charID;
    NSString *x;
    NSString *y;
    NSString *width;
    NSString *height;
    NSString *xOffset;
    NSString *yOffset;
    NSString *xAdvance;

    [scanner scanString:@"char id=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&charID];
    
    [scanner scanString:@"x=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&x];

    [scanner scanString:@"y=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&y];

    [scanner scanString:@"width=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&width];

    [scanner scanString:@"height=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&height];

    [scanner scanString:@"xoffset=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&xOffset];

    [scanner scanString:@"yoffset=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&yOffset];

    [scanner scanString:@"xadvance=" intoString:NULL];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:&xAdvance];
    
    glyph->charID = [charID intValue];
    glyph->x = [x intValue];
    glyph->y = [y intValue];
    glyph->width = [width intValue];
    glyph->height = [height intValue];
    glyph->xOffset = [xOffset intValue];
    glyph->yOffset = [yOffset intValue];
    glyph->xAdvance = [xAdvance intValue];
}

@end
