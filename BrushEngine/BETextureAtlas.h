//
//  BETextureAtlas.h
//  BrushEngine
//
//  Created by Germano Guerrini on 08/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface BETextureAtlas : NSObject

@property (nonatomic, strong) GLKTextureInfo *texture;

- (id)initWithTexture:(GLKTextureInfo *)texture controlFile:(NSString *)controlFile;
- (id)initWithContentsOfFile:(NSString *)fileName controlFile:(NSString *)controlFile;
- (void)parseControlFile:(NSString *)controlfile;

- (BOOL)containsKey:(NSString *)key;

- (CGRect)coordsForKey:(NSString *)key;
- (CGSize)sizeForKey:(NSString *)key;

@end
