//
//  BERenderingManager.m
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BERenderingManager.h"
#import "BEStage.h"
#import "BECamera.h"

@implementation BERenderingManager

+ (BERenderingManager *)sharedRenderingManager
{
    static BERenderingManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BERenderingManager alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if ((self = [super init])) {
        _matrixStack = GLKMatrixStackCreate(CFAllocatorGetDefault());
        _camera = [[BECamera alloc] initWithViewportSize:[[UIScreen mainScreen] bounds].size];
    }
    return self;
}

- (GLKMatrix4)projectionMatrix
{
    if (_camera == nil) {
        return GLKMatrix4Identity;
    }
    return _camera.projectionMatrix;
}

- (void)updateWithInterval:(NSTimeInterval)interval
{
    [_currentStage updateWithInterval:interval];
    [_camera updateWithInterval:interval];
}

- (void)render
{
    glClearColor(0.00f, 0.65f, 0.65f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    [_currentStage render];
    [_camera render];
}

@end
