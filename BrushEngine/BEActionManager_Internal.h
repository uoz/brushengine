//
//  BEActionManager_Internal.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEActionManager.h"

@interface BEActionManager (Internal)

/** Returns the number of registered targets. */
- (NSUInteger)registeredTargetCount;

/** Returns the number of registerd actions with the given target. */
- (NSUInteger)actionsWithTargetCount:(BETarget *)target;

@end
