//
//  BEParticleQuadEmitter.h
//  BrushEngine
//
//  Created by Germano Guerrini on 19/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEQuadBatchRenderer.h"

typedef enum {
    BEParticleEmitterTypeGravity = 0,
    BEParticleEmitterTypeRadial = 1
} BEParticleEmitterType;

typedef struct {
	GLKVector2 position;
	GLKVector2 direction;
    GLKVector2 startPos;
	GLKVector4 color;
	GLKVector4 deltaColor;
    GLfloat rotation;
    GLfloat rotationDelta;
    GLfloat radialAcceleration;
    GLfloat tangentialAcceleration;
	GLfloat radius;
	GLfloat radiusDelta;
	GLfloat angle;
	GLfloat degreesPerSecond;
	GLfloat particleSize;
	GLfloat particleSizeDelta;
	GLfloat timeToLive;    
} BEParticle;

@interface BEParticleQuadEmitter : BEQuadBatchRenderer
{
    BEParticleEmitterType emitterType;
    
	GLKVector2 sourcePosition, sourcePositionVariance;			
	GLfloat angle, angleVariance;								
	GLfloat speed, speedVariance;	
    GLfloat radialAcceleration, tangentialAcceleration;
    GLfloat radialAccelVariance, tangentialAccelVariance;
	GLKVector2 gravity;	
	GLfloat particleLifeSpan, particleLifeSpanVariance;			
	GLKVector4 startColor, startColorVariance;						
	GLKVector4 finishColor, finishColorVariance;
	GLfloat startParticleSize, startParticleSizeVariance;
	GLfloat finishParticleSize, finishParticleSizeVariance;
	GLuint maxParticles; // capacity
	GLfloat emissionRate;
	GLfloat emitCounter;	
	GLfloat elapsedTime;
	GLfloat duration;
    GLfloat rotationStart, rotationStartVariance;
    GLfloat rotationEnd, rotationEndVariance;
        
    GLfloat maxRadius;				
    GLfloat maxRadiusVariance;		
    GLfloat radiusSpeed;			
    GLfloat minRadius;				
    GLfloat rotatePerSecond;		
    GLfloat rotatePerSecondVariance;
    
    BOOL active;
}
@property (nonatomic) GLKVector2 sourcePosition;

- (id)initParticleEmitterWithFile:(NSString *)fileName;
- (void)stopParticleEmitter;

@end
