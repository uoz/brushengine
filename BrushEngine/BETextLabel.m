//
//  BETextLabel.m
//  BrushEngine
//
//  Created by Germano Guerrini on 28/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BETextLabel.h"
#import "BEFontTextureAtlas.h"
#import "BESprite.h"

@interface BETextLabel ()

@property (nonatomic, readonly) NSUInteger labelLength;

//- (void)addChar:(unichar)aChar;

@end

@implementation BETextLabel

- (id)initWithLabel:(NSString *)label fontTexture:(BEFontTextureAtlas *)fontTexture
{
    _label = label;
    _labelLength = [label length];
    _fontTexture = fontTexture;
    
    if ((self = [super initWithCapacity:_labelLength textureAtlas:fontTexture])) {
        unichar currentChar;
        NSString *currentCharKey;
        for (NSUInteger i = 0; i < _labelLength; i++) {
            currentChar = [_label characterAtIndex:i];
            currentCharKey = [NSString stringWithCharacters:&currentChar length:1];
            BESprite *sprite = [[BESprite alloc] initWithBatch:self key:currentCharKey];
            sprite.parent = self;
            sprite.position = GLKVector2Make(30 * i, 0); // TODO User font texture info to position 
        }
    }
    return self;
}

@end
