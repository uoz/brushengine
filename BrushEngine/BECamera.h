//
//  BECamera.h
//  BrushEngine
//
//  Created by Germano Guerrini on 03/03/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BETarget.h"
#import "BERendering.h"

@class BEGameObject;

@interface BECamera : BETarget <BERendering>

@property (nonatomic) GLKVector2 shake;
@property (nonatomic) float zoom;
@property (nonatomic, readonly) GLKMatrix4 projectionMatrix;

- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize outerFrame:(CGRect)outerFrame innerFrame:(CGRect)anInnerFrame;
- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize innerFrame:(CGRect)anInnerFrame;
- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize outerFrame:(CGRect)outerFrame;
- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target worldSize:(CGSize)worldSize;
- (id)initWithViewportSize:(CGSize)viewportSize target:(BEGameObject *)target;
- (id)initWithViewportSize:(CGSize)viewportSize;

- (GLKVector2)localPointToWorldSpace:(GLKVector2)localPoint;
- (GLKVector2)worldPointToLocalSpace:(GLKVector2)worldPoint;

@end
