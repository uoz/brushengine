//
//  BEQuadBatchRenderer_Internal.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/05/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BEQuadBatchRenderer ()

@property (nonatomic, readonly) NSUInteger activeQuadsCount;

- (BETexturedColoredQuad)quadAtIndex:(NSUInteger)index;

@end
