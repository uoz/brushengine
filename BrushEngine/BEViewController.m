//
//  BEViewController.m
//  BrushEngine
//
//  Created by Germano Guerrini on 26/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEUtils.h"
#import "BEViewController.h"
#import "BESettings.h"
#import "BEView.h"
#import "BEEffectLoader.h"
#import "BETextureLoader.h"
#import "BEProfiler.h"
#import "BEOpenGLStateCache.h"

@interface BEViewController ()

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) UILabel *profilerLabel;

- (UIInterfaceOrientation)initialInterfaceOrientation;
- (void)setupGL;
- (void)tearDownGL;
- (void)drawProfilerLabel;

@end

static float _framesPreviouslyDisplayed;

@implementation BEViewController

- (id)init
{
    if ((self = [super init]))
    {
        _renderingManager = [BERenderingManager sharedRenderingManager];
        _actionManager = [BEActionManager sharedActionManager];
        
#ifdef BE_PROFILER_ENABLED
        BEProfiler *profiler = [BEProfiler sharedProfiler];
        profiler.delegate = self;
        _profilerLabel = [profiler label];
#endif
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    [BEEffectLoader purgeCachedEffects];
    [BETextureLoader purgeCachedTextures];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    [self setupGL];
    BEView *view = [[BEView alloc] initWithFrame:[[UIScreen mainScreen] bounds] context:self.context];
    view.delegate = self;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    self.view = (BEView *)view;

#ifdef BE_PROFILER_ENABLED
    [self.view addSubview:_profilerLabel];
#endif
}

- (void)viewDidUnload
{
    [super viewDidUnload];
        
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
	self.context = nil;
}


#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    /* TODO Implement a configurable fixed interval loop */
    [_actionManager updateWithInterval:self.timeSinceLastUpdate];
    [_renderingManager updateWithInterval:self.timeSinceLastUpdate];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    [_renderingManager render];
    
#ifdef BE_PROFILER_ENABLED
    [self drawProfilerLabel];
#endif
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    /* TODO For now, we are only supporting landscape. */
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

#pragma mark - Private Methods

- (UIInterfaceOrientation)initialInterfaceOrientation
{
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *initialOrientation = [bundleInfo objectForKey:@"UIInterfaceOrientation"];
    
    if (initialOrientation) {
        if ([initialOrientation isEqualToString:@"UIInterfaceOrientationPortrait"]) {
            return UIInterfaceOrientationPortrait;
        } else if ([initialOrientation isEqualToString:@"UIInterfaceOrientationPortraitUpsideDown"]) {
            return UIInterfaceOrientationPortraitUpsideDown;
        } else if ([initialOrientation isEqualToString:@"UIInterfaceOrientationLandscapeLeft"]) {
            return UIInterfaceOrientationLandscapeLeft;
        } else {
            return UIInterfaceOrientationLandscapeRight;
        }
    } else {
        return [[UIApplication sharedApplication] statusBarOrientation];
    }
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    BEsetAlphaBlending(YES);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
}

- (void)drawProfilerLabel
{
    static float timeSinceLastProfilerTick;
    
    _currentFramesPerSecond = (self.framesDisplayed - _framesPreviouslyDisplayed) / self.timeSinceLastDraw;
    timeSinceLastProfilerTick += self.timeSinceLastDraw;
    if (timeSinceLastProfilerTick >= BE_PROFILER_TICK) {
        [_profilerLabel setText:[[BEProfiler sharedProfiler] labelText]];
        timeSinceLastProfilerTick = 0.0f;
    }
    [[BEProfiler sharedProfiler] resetTextureSwitchCount];
    _framesPreviouslyDisplayed = self.framesDisplayed;
}

@end
