//
//  BEDrawingUtils.m
//  BrushEngine
//
//  Created by Germano Guerrini on 21/05/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEDrawingUtils.h"
#import "BEEffectLoader.h"
#import "BEOpenGLStateCache.h"
#import "BESettings.h"

@implementation BEDrawingUtils

+ (void)drawQuad:(BETexturedColoredQuad)quad
{
    [self drawLineFrom:quad.tlVertex.position to:quad.trVertex.position];
    [self drawLineFrom:quad.trVertex.position to:quad.brVertex.position];
    [self drawLineFrom:quad.brVertex.position to:quad.blVertex.position];
    [self drawLineFrom:quad.blVertex.position to:quad.tlVertex.position];
}

+ (void)drawLineFrom:(GLKVector2)start to:(GLKVector2)end
{
    GLKBaseEffect *effect = [BEEffectLoader effectWithKey:BE_DEFAULT_EFFECT];
    GLKVector2 vertices[2] = {
        {start.x, start.y},
        {end.x, end.y}
    };
    GLKVector4 color = GLKVector4Make(1, 0, 1, 1);
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, (void *)vertices);
    
//    glEnableVertexAttribArray(GLKVertexAttribColor);
//    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, 4, (void *)&color);

    glLineWidth(8);
    effect.useConstantColor = YES;
    effect.constantColor = color;
    [effect prepareToDraw];
    glDisable(GL_BLEND);
    glDrawArrays(GL_LINES, 0, 2);
    glEnable(GL_BLEND);
    glDisableVertexAttribArray(GLKVertexAttribPosition);
//    glDisableVertexAttribArray(GLKVertexAttribColor);
}
@end
