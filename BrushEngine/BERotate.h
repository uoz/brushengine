//
//  BERotate.h
//  BrushEngine
//
//  Created by Germano Guerrini on 25/11/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEAction.h"

// --------------------------------------------------------------------------------
// BERotate
// --------------------------------------------------------------------------------

/** Abstract base class that defines a transformation of the rotation parameter of a `BEGameObject` instance.
 
 A `BERotate` object with a duration of 0 will result in a instant rotation of the target object rather than a smooth transition.
 */
@interface BERotate : BEAction
{
@protected
    float _startAngle;
    float _endAngle;
}

/// --------------------------------------------------------------------------------
/// @name Initialization
/// --------------------------------------------------------------------------------

/** Initializes a new rotating action.
 
 This is the designated initializer.
 
 @param target The object the action will be rotated.
 @param angle An angle, in radians, used by the action to calculate the final rotation.
 @param duration The duration of the action in seconds.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle duration:(NSTimeInterval)duration;

/** Initializes a new instantaneous rotating action.
 
 @param target The object the action will be scaled.
 @param angle An angle, in radians, used by the action to calculate the final rotation.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target angleInRadians:(float)angle;

/** Initializes a new instantaneous rotating action.
 
 @param target The object the action will be scaled.
 @param angle An angle, in degrees, used by the action to calculate the final rotation.
 @param duration The duration of the action in seconds.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target angleInDegrees:(float)angle duration:(NSTimeInterval)duration;

/** Initializes a new instantaneous rotating action.
 
 @param target The object the action will be scaled.
 @param angle An angle, in degrees, used by the action to calculate the final rotation.
 @return Returns initialized instance or `nil` if initialization fails.
 */
- (id)initWithTarget:(BEGameObject *)target angleInDegrees:(float)angle;

@end

// --------------------------------------------------------------------------------
// BERotateTo
// --------------------------------------------------------------------------------

/** An action that performs a rotation of a `BEGameObject` instance from its current angle to the given angle. 
 Repeating this action will not rotate the target object any further.
 */
@interface BERotateTo : BERotate
@end

// --------------------------------------------------------------------------------
// BERotateBy
// --------------------------------------------------------------------------------

/** An action that performs a rotation of a `BEGameObject` instance from its current angle by the given angle. 
 Repeating this action will continue to rotate the target object.
 */
@interface BERotateBy : BERotate
@end
