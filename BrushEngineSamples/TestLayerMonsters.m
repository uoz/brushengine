//
//  TestLayerMonsters.m
//  BrushEngine
//
//  Created by Germano Guerrini on 11/01/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "TestLayerMonsters.h"
#import "BETextureAtlas.h"
#import "BESprite.h"
#import "BESpriteBatch.h"
#import "BEFontTextureAtlas.h"
#import "BETextLabel.h"
#import "BEUtils.h"
#import "BERotate.h"
#import "BETranslate.h"
#import "BEScale.h"
#import "BEFade.h"
#import "BEChoreography.h"
#import "BEAnimate.h"
#import "BEAnimatedSprite.h"
#import "BEAnimation.h"
#import "BERepeatingAction.h"
#import "BECamera.h"

@interface TestLayerMonsters ()
{
    BESprite *monkey;
    NSMutableArray *bananas;
    NSMutableArray *bananasChildren;
    BEFade *fadeOut;
    BEScaleTo *scaleBuddy;
    BEFlip *flip;
    BEAnimation *monsterWalkingAnimation;
    BERepeatingAction *monsterWalkingRepeatingAnimation;
    BEAnimate *buddyRunningAction;
    BEAnimatedSprite *buddy;
    BETranslateTo *translateMonster;
}
@end

@implementation TestLayerMonsters
@synthesize monster;
@synthesize monsterAnimation;

- (id)init
{
    if ((self = [super init])) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *textureFileName = [bundle pathForResource:@"test_atlas_default" ofType:@"png"];
        NSString *controlFileName = [bundle pathForResource:@"test_atlas_default" ofType:@"plist"];
        BETextureAtlas *atlas = [[BETextureAtlas alloc] initWithContentsOfFile:textureFileName 
                                                                   controlFile:controlFileName];
        
        BESpriteBatch *batchNode = [[BESpriteBatch alloc] initWithCapacity:128 textureAtlas:atlas];
        
        monkey = [[BESprite alloc] initWithBatch:batchNode key:@"monkey.png"];
        //monkey = [[BESprite alloc] initWithBatch:batchNode key:@"monkey.png"];
        bananas = [[NSMutableArray alloc] init];
        bananasChildren = [[NSMutableArray alloc] init];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                BESprite *banana = [[BESprite alloc] initWithBatch:batchNode key:@"banana.png"];
                //[[BESprite alloc] initWithBatch:batchNode size:CGSizeMake(64, 64) key:@"banana.png"];
                banana.size = CGSizeMake(64, 64);
                banana.position = GLKVector2Make(i * 128, j * 128);
                [monkey addChild:banana];
                //[batchNode addChild:banana];
                [bananas addObject:banana];
                
                //[monkey addChild:banana];
                
                for (int s = 0; s < 2; s++) {
                    for (int t = 0; t < 2; t++) {
                        BESprite *bananaChild = [[BESprite alloc] initWithBatch:batchNode key:@"banana.png"];
                        bananaChild.size = CGSizeMake(32, 32);
                        bananaChild.position = GLKVector2Make(s * 64, t * 64);
                        [bananasChildren addObject:bananaChild];
                        [banana addChild:bananaChild];
                    }
                    
                }
            }
        }
        
        [self addChild:batchNode];
        
        // Label
        NSString *fontTextureFileName = [bundle pathForResource:@"test_font" ofType:@"png"];
        NSString *fontControlFileName = [bundle pathForResource:@"test_font" ofType:@"fnt"];
        BEFontTextureAtlas *fontTexture = [[BEFontTextureAtlas alloc] initWithContentsOfFile:fontTextureFileName controlFile:fontControlFileName];
        BETextLabel *label = [[BETextLabel alloc] initWithLabel:@"{GanZissiMo}" fontTexture:fontTexture];
        label.position = GLKVector2Make(256.0f, 256.0f);
        [self addChild:label];
        BERotateBy *rotate = [[BERotateBy alloc] initWithTarget:label angleInDegrees:180 duration:2.0f];
        BETranslateTo *translate = [[BETranslateTo alloc] initWithTarget:label position:GLKVector2Make(512, 256) duration:2.0f];
        translate.tweeningFunctionType = BETweeningFunctionBounceEaseOut;
        BEScaleTo *scaleTo = [[BEScaleTo alloc] initWithTarget:label scale:GLKVector2Make(2, 2) duration:4.0f];
        
        _labelChoreo = [[BEChoreography alloc] initWithType:BEChoreographyTypeSequence actions:rotate, translate, scaleTo, nil];
        [_labelChoreo play];
        
        // Test new animation - Monster
        textureFileName = [bundle pathForResource:@"monster_walk_default" ofType:@"png"];
        controlFileName = [bundle pathForResource:@"monster_walk_default" ofType:@"plist"];
        BETextureAtlas *atlasWalk = [[BETextureAtlas alloc] initWithContentsOfFile:textureFileName
                                                                       controlFile:controlFileName];
        monsterWalkingAnimation = [[BEAnimation alloc] initWithKey:@"walk"
                                                             atlas:atlasWalk
                                                         keyFormat:@"walk%d.png"
                                                          keyRange:NSMakeRange(1, 16)
                                                         frequency:30];
        monster = [[BEAnimatedSprite alloc] initWithAnimation:monsterWalkingAnimation];
        [self addChild:monster];
        
        BEAnimate *monsterWalkingAction = [[BEAnimate alloc] initWithTarget:monster animationKey:@"walk"];
        monsterWalkingRepeatingAnimation = [[BERepeatingAction alloc] initWithAction:monsterWalkingAction];
//        monsterWalkingRepeatingAnimation = [[BERepeatingAction alloc] initWithAction:monsterWalkingAction];
//        BETranslateTo *translateMonster1 = [[BETranslateTo alloc] initWithTarget:monster position:GLKVector2Make(1600, 800) duration:3];
//        BETranslateTo *translateMonster2 = [[BETranslateTo alloc] initWithTarget:monster position:GLKVector2Make(1024, 0) duration:3];
//        BETranslateTo *translateMonster3 = [[BETranslateTo alloc] initWithTarget:monster position:GLKVector2Make(0, 512) duration:3];
//        BEFlip *flipMonster = [[BEFlip alloc] initWithTarget:monster flipAxis:BEFlipAxisX];
//        BETranslateTo *translateMonster4 = [[BETranslateTo alloc] initWithTarget:monster position:GLKVector2Make(600, 0) duration:3];
//        BEChoreography *monsterWalking = [[BEChoreography alloc] initWithType:BEChoreographyTypeSequence actions:flipMonster, translateMonster1, translateMonster2, flipMonster, translateMonster3, flipMonster, translateMonster4, nil];
//        monsterAnimation = [[BEChoreography alloc] initWithType:BEChoreographyTypeConcurrent actions:monsterWalkingRepeatingAnimation, monsterWalking, nil];
//        [monsterAnimation play];
        
        // Test new animation - Buddy
//        BETextureAtlas *atlasRun = [[BETextureAtlas alloc] initWithContentsOfFile:@"buddy_run.png" 
//                                                                      controlFile:@"buddy_run.plist"];
//        BESprite *buddy = [[BESprite alloc] initWithTextureAtlas:atlasRun key:@"run1.png"];
//        buddy.position = GLKVector2Make(128, 384);
//        [self addChild:buddy];
//
//        BEInnerAnimation *buddyRunInnerAnimation = [[BEInnerAnimation alloc] initWithTarget:buddy atlas:atlasRun keyFormat:@"run%d.png" keyRange:NSMakeRange(1, 25) frequency:30];
//        BERepeatingAction *buddyRunAnimation = [[BERepeatingAction alloc] initWithAction:buddyRunInnerAnimation];
//        [buddyRunAnimation play];
        //[fadeOut play];
        
//        scaleBuddy = [[BEScaleTo alloc] initWithTarget:buddy scale:GLKVector2Make(2,2) duration:4];
//        scaleBuddy.tweeningFunctionType = BETweeningFunctionQuinticEaseOut;
//        [scaleBuddy play];
//        
//        flip = [[BEFlip alloc] initWithTarget:buddy flipAxis:BEFlipAxisX];
        //[flip play];
        
        // Test BEAnimatedSprite -> BEAnimate
        textureFileName = [bundle pathForResource:@"buddy_animations" ofType:@"png"];
        controlFileName = [bundle pathForResource:@"buddy_animations" ofType:@"plist"];
        BETextureAtlas *buddyAnimationsAtlas = [[BETextureAtlas alloc] initWithContentsOfFile:textureFileName
                                                                                  controlFile:controlFileName];

        BESpriteBatch *buddyBatch = [[BESpriteBatch alloc] initWithCapacity:5 textureAtlas:buddyAnimationsAtlas];
        
        BEAnimation *buddyRunningAnimation = [[BEAnimation alloc] initWithKey:@"run"
                                                                        atlas:buddyAnimationsAtlas
                                                                    keyFormat:@"run%d.png"
                                                                     keyRange:NSMakeRange(1, 25)
                                                                    frequency:30];
        
        BEAnimation *buddyIdleAnimation = [[BEAnimation alloc] initWithKey:@"idle"
                                                                     atlas:buddyAnimationsAtlas
                                                                 keyFormat:@"idle%d.png"
                                                                  keyRange:NSMakeRange(1, 19)
                                                                 frequency:30];
        
        
        NSDictionary *buddyAnimations = [NSDictionary dictionaryWithObjectsAndKeys:buddyIdleAnimation, buddyIdleAnimation.key, buddyRunningAnimation, buddyRunningAnimation.key, nil];
        
        buddy = [[BEAnimatedSprite alloc] initWithAnimations:buddyAnimations
                                                                    setCurrent:buddyRunningAnimation];
        buddy.position = GLKVector2Make(384, 384);
        buddy.scale = GLKVector2Make(0.5f, 0.5f);
        //[self addChild:buddy];
        _buddyRunningRepeatingAction = [buddy playAnimationWithKey:@"idle"];
//        buddyRunningAction = [[BEAnimate alloc] initWithTarget:buddy animationKey:@"run"];
//        _buddyRunningRepeatingAction = [[BERepeatingAction alloc] initWithAction:buddyRunningAction];
//        [_buddyRunningRepeatingAction play];
        
        BEAnimatedSprite *buddy2 = [[BEAnimatedSprite alloc] initWithAnimation:buddyRunningAnimation];
        buddy2.position = GLKVector2Make(128, 512);
        //[self addChild:buddy2];
        [buddy2 playAnimationWithKey:@"run"];
//        BEAnimate *buddy2RunningAction = [[BEAnimate alloc] initWithTarget:buddy2 animationKey:@"run"];
//        BERepeatingAction *buddy2RunningRepeatingAction = [[BERepeatingAction alloc] initWithAction:buddy2RunningAction];
//        [buddy2RunningRepeatingAction play];
        
        BEAnimatedSprite *buddy3 = [[BEAnimatedSprite alloc] initWithAnimation:buddyRunningAnimation];
        buddy3.position = GLKVector2Make(256, 512);
        //[self addChild:buddy2];
        BEAnimate *buddy3RunningAction = [[BEAnimate alloc] initWithTarget:buddy3 animationKey:@"run"];
        BERepeatingAction *buddy3RunningRepeatingAction = [[BERepeatingAction alloc] initWithAction:buddy3RunningAction];
        [buddy3RunningRepeatingAction play];
        
        BEAnimatedSprite *buddy4 = [[BEAnimatedSprite alloc] initWithAnimation:buddyRunningAnimation];
        buddy4.position = GLKVector2Make(384, 512);
        //[self addChild:buddy2];
        BEAnimate *buddy4RunningAction = [[BEAnimate alloc] initWithTarget:buddy4 animationKey:@"run"];
        BERepeatingAction *buddy4RunningRepeatingAction = [[BERepeatingAction alloc] initWithAction:buddy4RunningAction];
        [buddy4RunningRepeatingAction play];
        
        BEAnimatedSprite *buddy5 = [[BEAnimatedSprite alloc] initWithAnimation:buddyRunningAnimation];
        buddy5.position = GLKVector2Make(512, 512);
        //[self addChild:buddy2];
        BEAnimate *buddy5RunningAction = [[BEAnimate alloc] initWithTarget:buddy5 animationKey:@"run"];
        BERepeatingAction *buddy5RunningRepeatingAction = [[BERepeatingAction alloc] initWithAction:buddy5RunningAction];
        [buddy5RunningRepeatingAction play];
        
        buddy.batch = buddyBatch;
        buddy2.batch = buddyBatch;
        buddy3.batch = buddyBatch;
        buddy4.batch = buddyBatch;
        buddy5.batch = buddyBatch;
        [self addChild:buddyBatch];
    }
    return self;
}

- (void)updateWithInterval:(NSTimeInterval)anInterval
{
    static float transX = 0;
    static float transY = 0;
    static float rotate = 0;
    static float scaling = 0.5f;
    
    float x = sinf(transX) * sinf(transX) * 1024;
    float y = sinf(transY) * sinf(transY) * 768;
    
    monkey.position = GLKVector2Make(x, y);
    monkey.rotation = M_PI_2 * rotate;
    monkey.color = GLKVector4Make(sinf(transY) * sinf(transY), 1, 1, sinf(transY) * sinf(transY));
    monkey.scale = GLKVector2Make(sinf(transX), sinf(transX));
    
    [bananas enumerateObjectsUsingBlock:^(BESprite *obj, NSUInteger idx, BOOL *stop) {
        obj.rotation = 2 * M_PI * rotate;
    }];
    [bananasChildren enumerateObjectsUsingBlock:^(BESprite *obj, NSUInteger idx, BOOL *stop) {
        obj.scale = GLKVector2Make(sinf(scaling), sinf(scaling));
    }];
    transX += anInterval / 8;
    transY += anInterval / 8;
    rotate += anInterval / 4;
    scaling += 2 * anInterval;
    
//    [labelChoreo updateWithInterval:anInterval];
//    [translateLayer updateWithInterval:anInterval];
//    [fadeOut updateWithInterval:anInterval];
//    [flip updateWithInterval:anInterval];
//    [scaleBuddy updateWithInterval:anInterval];
//    [monsterWalkingAnimation updateWithInterval:anInterval];
    [super updateWithInterval:anInterval];
}

- (void)testTouchPoint:(CGPoint)point
{
    static NSString *currentAnimationKey = @"run";
    if ([currentAnimationKey isEqualToString:@"run"]) {
        [buddy setCurrentAnimation:[buddy animationWithKey:@"idle"]];
        currentAnimationKey = @"idle";
    } else {
        [buddy setCurrentAnimation:[buddy animationWithKey:@"run"]];
        currentAnimationKey = @"run";
    }
    if ([translateMonster isRunning]) {
        [translateMonster stop];
    }
    GLKVector2 dest = [[BERenderingManager sharedRenderingManager].camera localPointToWorldSpace:GLKVector2FromCGPoint(point)];
    translateMonster = [[BETranslateTo alloc] initWithTarget:monster position:dest duration:1];
    [translateMonster play];
}

@end
