//
//  TestViewController.h
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEViewController.h"

@class TestStage;

@interface TestViewController : BEViewController
{
    TestStage *testStage;
}
@property (strong, nonatomic) TestStage *testStage;
@end
