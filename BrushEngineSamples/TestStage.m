//
//  TestStage.m
//  BrushEngine
//
//  Created by Germano Guerrini on 23/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "TestStage.h"
#import "TestLayerBackGround.h"
#import "TestLayerBackgroundParticles.h"
#import "TestLayerMonsters.h"
#import "BEFontTextureAtlas.h"
#import "BETextLabel.h"
#import "BEUtils.h"

@implementation TestStage
@synthesize repeatingAction;
@synthesize choreo;
@synthesize translateAction;
@synthesize rotateAction;
@synthesize animatedSprite2;


- (id)init
{
    if ((self = [super init])) {
        
        // BACKGROUND 
        BELayer *backgroundLayer = [[TestLayerBackGround alloc] init];
        [self addLayer:backgroundLayer atDepth:BELayerDepthBackground];
        
        // BACKGROUND PARTICLES
        BELayer *backgroundParticlesLayer = [[TestLayerBackgroundParticles alloc] init];
        [self addLayer:backgroundParticlesLayer atDepth:BELayerDepthBackgroundParticles];
        
        // MONSTERS
        BELayer *monstersLayer = [[TestLayerMonsters alloc] init];
        [self addLayer:monstersLayer atDepth:BELayerDepthMonsters];
        
//        self.monkey2 = [[BESprite alloc] initWithBatch:batchNode key:@"monkey.png"];
//        self.monkey2.position = GLKVector2Make(512, 0);
//        
//        
//        self.rotateAction = [[BERotateBy alloc] initWithAngleInDegrees:180 duration:1];
//        [self.rotateAction startWithTarget:self.monkey2];
//        self.rotateAction.tweeningFunctionType = BETweeningFunctionElasticEaseIn;
//        
//        self.translateAction = [[BETranslateTo alloc] initWithPosition:GLKVector2Make(512, 768) duration:2];
//        [self.translateAction startWithTarget:self.monkey2];
//        self.translateAction.tweeningFunctionType = BETweeningFunctionElasticEaseOut;
//        
//        BETranslateBy *tb = [[BETranslateBy alloc] initWithPosition:GLKVector2Make(0, -384) duration:2];
//        [tb startWithTarget:self.monkey2];
//        //        BETranslateTo *tt = [[BETranslateTo alloc] initWithTarget:self.monkey2 position:GLKVector2Make(1024, 384) duration:2];
//        BEScale *sb = [[BEScaleBy alloc] initWithScale:GLKVector2Make(-1, -1) duration:2];
//        [sb startWithTarget:self.monkey2];
        
        
        
//        BEChoreography *c = [[BEChoreography alloc] initWithType:BEChoreographyTypeConcurrent actions: tb, sb, nil];
//        self.choreo = [[BEChoreography alloc] initWithType:BEChoreographyTypeSequence actions:
//                       /*self.translateAction, */
//                       self.translateAction, c, nil];
//        self.repeatingAction = [[BERepeatingAction alloc] initWithAction:self.choreo repeatCount:2];
//        //        [self.monkey2 render];
//        [self.repeatingAction play];
                
        
        //        
        
        //        [self.animatedSprite render];
        //
//        BETextureAtlas *atlas_walk = [[BETextureAtlas alloc] initWithContentsOfFile:@"monster_walk_default.png" 
//                                                                    controlFile:@"monster_walk_default.plist"];
//        
//        BEAnimationOptions options = {BEAnimationStateRunning, BEAnimationTypeLoop, BEAnimationDirectionForward};
//
//        BEAnimation *testAnimation2 = [[BEAnimation alloc] initWithKey:(NSString *)key atlas:atlas_walk keyFormat:@"walk%d.png" keyRange:NSMakeRange(1, 16) frequency:30 options:options];
//        self.animatedSprite2 = [[BEAnimatedSprite alloc] initWithAnimation:testAnimation2 withKey:@"walk" size:CGSizeMake(128, 128)];
//        self.animatedSprite2.position = GLKVector2Make(384, 512);
        //        [self.animatedSprite2 render];

    }
    return self;
}

- (void)render
{
    [super render];
    //[batchNode render];
    //[particleEmitter render];

    //[self.animatedSprite render];
    //[self.animatedSprite2 render];
}

- (void)updateWithInterval:(NSTimeInterval)anInterval
{    
    // test 
//    BELayer *bgLayer = [self layerAtDepth:BELayerDepthMonsters];
//    bgLayer.position = GLKVector2Make(bgLayer.position.x + 5 * RANDOM_MINUS_1_1, bgLayer.position.y + 5 * RANDOM_MINUS_1_1);
    
    //[particleEmitter updateWithInterval:anInterval];
//    static float transX = 0;
//    static float transY = 0;
//    static float rotate = 0;
//    float x = sinf(transX) * sinf(transX) * 1024;
//    float y = sinf(transY) * sinf(transY) * 1024;
//    
//    transX += anInterval / 4;
//    transY += anInterval / 4;
//    rotate += anInterval;
//    
//    self.animatedSprite2.rotation = M_PI_2;
//    self.animatedSprite2.position = GLKVector2Make(self.animatedSprite2.position.x, y);
    //[self.animatedSprite2 updateWithInterval:anInterval];
    //    [self.translateAction advanceToOffset:anInterval];
    
    
    //[self.repeatingAction updateWithInterval:anInterval];
    //[batchNode updateWithInterval:anInterval];
    //particleEmitter.sourcePosition = animatedSprite.position;
    [super updateWithInterval:anInterval];
}

@end
