//
//  TestLayerBackGround.m
//  BrushEngine
//
//  Created by Germano Guerrini on 23/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "TestLayerBackGround.h"
#import "BESprite.h"

@implementation TestLayerBackGround

- (id)init
{
    if ((self = [super init])) {
        NSString *fileName;
        fileName = [[NSBundle mainBundle] pathForResource:@"test_bg_2048.png" ofType:nil];
        BESprite *bg = [[BESprite alloc] initWithContentsOfFile:fileName];
        bg.position = GLKVector2Make(1024.0f, 1024.0f);
        [self addChild:bg];
    }
    return self;
}

@end
