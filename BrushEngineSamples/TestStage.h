//
//  TestStage.h
//  BrushEngine
//
//  Created by Germano Guerrini on 23/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "BEStage.h"
#import "BESprite.h"
#import "BESpriteBatch.h"
#import "BEAnimatedSprite.h"
#import "BETextureAtlas.h"
#import "BEAnimation.h"
#import "BERepeatingAction.h"
#import "BEChoreography.h"
#import "BETranslate.h"
#import "BERotate.h"
#import "BEScale.h"

@interface TestStage : BEStage
{
    BEChoreography *choreo;
    BERepeatingAction *repeatingAction;
    BETranslateTo *translateAction;
    BERotateBy *rotateAction;
    BEAnimatedSprite *animatedSprite2;
}
@property (strong, nonatomic) BEChoreography *choreo;
@property (strong, nonatomic) BERepeatingAction *repeatingAction;
@property (strong, nonatomic) BETranslateTo *translateAction;
@property (strong, nonatomic) BERotateBy *rotateAction;
@property (strong, nonatomic) BEAnimatedSprite *animatedSprite2;

@end
