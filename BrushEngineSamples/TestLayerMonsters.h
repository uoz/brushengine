//
//  TestLayerMonsters.h
//  BrushEngine
//
//  Created by Germano Guerrini on 11/01/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BELayer.h"
@class BEAnimatedSprite;
@class BEChoreography;
@class BEAction;
@class BEAnimate;

@interface TestLayerMonsters : BELayer
{
    BEAnimatedSprite *monster;
    BEChoreography *monsterAnimation;
}
@property (strong, nonatomic) BEAnimatedSprite *monster;
@property (strong, nonatomic) BEChoreography *monsterAnimation;
@property (strong, nonatomic) BEAction *buddyRunningRepeatingAction;
@property (strong, nonatomic) BEChoreography *labelChoreo;

- (void)testTouchPoint:(CGPoint)point;

@end
