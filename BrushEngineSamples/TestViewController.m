//
//  TestViewController.m
//  BrushEngine
//
//  Created by Germano Guerrini on 29/10/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "TestViewController.h"
#import "BEView.h"
#import "BERenderingManager.h"
#import "BECamera.h"
#import "BECameraShake.h"
#import "TestStage.h"

#import "TestLayerMonsters.h"
#import "BECameraZoom.h"

@implementation TestViewController
{
    BECamera *camera;
}
@synthesize testStage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    testStage = [[TestStage alloc] init];
    TestLayerMonsters *l = (TestLayerMonsters *)[testStage layerAtDepth:BELayerDepthMonsters];

    camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(1024, 768)
                                             target:l.monster
                                          worldSize:CGSizeMake(2048, 768)
                                        outerFrame:CGRectMake(128, 128, 512, 384)
                                        innerFrame:CGRectMake(256, 256, 128, 128)];
                                         //innerFrame:CGRectMake(0, 0, 512, 512)];
    //camera.zoom = 0.5f;
//    BECameraShake *cameraShake = [[BECameraShake alloc] initWithCamera:camera type:BEShakeTypeOblique magnitude:1.0f duration:5];
//    [cameraShake play];
//    BECameraZoom *cameraZoom = [[BECameraZoom alloc] initWithCamera:camera zoom:4.0f duration:4];
//    [cameraZoom play];
    [self.renderingManager setCamera:camera];
    [self.renderingManager setCurrentStage:testStage];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
    CGPoint touchPoint = [touch locationInView:self.view];
    TestLayerMonsters *l = (TestLayerMonsters *)[testStage layerAtDepth:BELayerDepthMonsters];
    [l testTouchPoint:touchPoint];
//    static BOOL paused = NO;
//    if (!paused) {
//        [l.buddyRunningRepeatingAction pause];
//        //[self.actionManager pause];
//        paused = YES;
//    } else {
//        [l.buddyRunningRepeatingAction play];
//        //[self.actionManager play];
//        paused = NO;
//    }
}

@end
