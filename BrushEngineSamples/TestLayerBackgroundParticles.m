//
//  TestLayerBackgroundParticles.m
//  BrushEngine
//
//  Created by Germano Guerrini on 23/12/11.
//  Copyright (c) 2011 BitCanvas. All rights reserved.
//

#import "TestLayerBackgroundParticles.h"
#import "BEParticleQuadEmitter.h"

@implementation TestLayerBackgroundParticles

- (id)init
{
    if ((self = [super init])) {
        NSString *fileName;
        fileName = [[NSBundle mainBundle] pathForResource:@"particleConfig" ofType:@"json"];
        BEParticleQuadEmitter *particleEmitter = [[BEParticleQuadEmitter alloc] initParticleEmitterWithFile:fileName];
        [self addChild:particleEmitter];
    }
    return self;
}

@end
