//
//  BEQuadBatchRendererTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 24/05/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "BEQuadBatchRendererTest.h"

@implementation BEQuadBatchRendererTest

- (void)testAddAndRemove
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *textureFile = [bundle pathForResource:@"sprite_64_64" ofType:@"png"];
    GLKTextureInfo *texture = [GLKTextureLoader textureWithContentsOfFile:textureFile options:nil error:nil];
    BEQuadBatchRenderer *batch = [[BEQuadBatchRenderer alloc] initWithCapacity:4 texture:texture];
    STAssertTrue(batch.activeQuadsCount == 0, @"No quad should be active");
    
    BESprite *sprite16 = [[BESprite alloc] initWithTexture:texture];
    int index = [batch addQuad:sprite16.quad];
    STAssertTrue(index == 0, @"Quad should have index 0");
    STAssertTrue(batch.activeQuadsCount == 1, @"One quad should be active");
    
    BESprite *sprite32 = [[BESprite alloc] initWithTexture:texture];
    index = [batch addQuad:sprite32.quad];
    STAssertTrue(index == 1, @"Quad should have index 1");
    STAssertTrue(batch.activeQuadsCount == 2, @"Two quads should be active");
    
    BESprite *sprite64 = [[BESprite alloc] initWithTexture:texture];
    index = [batch addQuad:sprite64.quad];
    STAssertTrue(index == 2, @"Quad should have index 2");
    STAssertTrue(batch.activeQuadsCount == 3, @"Three quads should be active");
    
    [batch disableQuadAtIndex:0];
    STAssertTrue(batch.activeQuadsCount == 2, @"Two quads should be active");

    [batch disableQuadAtIndex:0];
    STAssertTrue(batch.activeQuadsCount == 1, @"One quad should be active");

    [batch disableQuadAtIndex:0];
    STAssertTrue(batch.activeQuadsCount == 0, @"No quad should be active");

    STAssertThrows([batch disableQuadAtIndex:4], @"Index is out of range");

    index = [batch addQuad:sprite64.quad];
    STAssertTrue(index == 0, @"Quad should have index 0");
    STAssertTrue(batch.activeQuadsCount == 1, @"One quad should be active");
    
    index = [batch addQuad:sprite32.quad];
    STAssertTrue(index == 1, @"Quad should have index 1");
    STAssertTrue(batch.activeQuadsCount == 2, @"Two quads should be active");
    
    [batch disableQuadAtIndex:1];
    STAssertTrue(batch.activeQuadsCount == 1, @"One quad should be active");
    
    [batch disableQuadAtIndex:1];
    STAssertTrue(batch.activeQuadsCount == 1, @"One quad should be active");
    
    [batch disableQuadAtIndex:0];
    STAssertTrue(batch.activeQuadsCount == 0, @"No quad should be active");
    
    index = [batch addQuad:sprite32.quad];
    index = [batch addQuad:sprite32.quad];
    index = [batch addQuad:sprite32.quad];
    index = [batch addQuad:sprite32.quad];
    index = [batch addQuad:sprite32.quad];
    STAssertTrue(index == -1, @"Quad should have index -1");
    
    [batch disableAllQuads];
    STAssertTrue(batch.activeQuadsCount == 0, @"No quad should be active");
    
    [batch replaceQuadAtIndex:0 withQuad:sprite16.quad];
    NSString *expected = NSStringFromBETexturedColoredQuad(sprite16.quad);
    STAssertTrue([NSStringFromBETexturedColoredQuad([batch quadAtIndex:0]) isEqualToString:expected], @"Quads should be equal");
    
    STAssertThrows([batch replaceQuadAtIndex:12 withQuad:sprite16.quad], @"Index is out of range");
}

@end
