//
//  BETranslateTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 10/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BEGameObject.h"
#import "BETranslate.h"
#import "BERepeatingAction.h"

@interface BETranslateTest : SenTestCase

@end
