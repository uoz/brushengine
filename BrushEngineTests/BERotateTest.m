//
//  BERotateTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 11/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BERotateTest.h"

@implementation BERotateTest

- (void)testRotateTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateTo *rotate = [[BERotateTo alloc] initWithTarget:object angleInRadians:endRotation duration:5.0f];
    [rotate play];
    
    [rotate updateWithInterval:2.5f];
    float expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:2.5f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:2.5f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRotateToReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateTo *rotate = [[BERotateTo alloc] initWithTarget:object angleInRadians:endRotation duration:30.0f];
    [rotate play];
    
    [rotate updateWithInterval:10.0f];
    float expectedRotation = 10.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:5.0f];
    expectedRotation = 5.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:10.0f];
    expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:10.0f];
    expectedRotation = 25.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:5.0f];
    expectedRotation = 20.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:10.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRotateBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateBy *rotate = [[BERotateBy alloc] initWithTarget:object angleInRadians:endRotation duration:5.0f];
    [rotate play];
    
    [rotate updateWithInterval:2.5f];
    float expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:2.5f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:2.5f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRotateByReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateBy *rotate = [[BERotateBy alloc] initWithTarget:object angleInRadians:endRotation duration:30.0f];
    [rotate play];
    
    [rotate updateWithInterval:10.0f];
    float expectedRotation = 10.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:5.0f];
    expectedRotation = 5.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:10.0f];
    expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate updateWithInterval:10.0f];
    expectedRotation = 25.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:5.0f];
    expectedRotation = 20.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [rotate reverse];
    [rotate updateWithInterval:10.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRepeatingRotatingTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateTo *rotate = [[BERotateTo alloc] initWithTarget:object angleInRadians:endRotation duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:rotate];
    [rotate play];
    
    [repeating updateWithInterval:5.0f];
    float expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRepeatingRotatingToReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateTo *rotate = [[BERotateTo alloc] initWithTarget:object angleInRadians:endRotation duration:30.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:rotate];
    [repeating play];
    
    [repeating updateWithInterval:10.0f];
    float expectedRotation = 10.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:5.0f];
    expectedRotation = 5.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:10.0f];
    expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:10.0f];
    expectedRotation = 25.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:5.0f];
    expectedRotation = 20.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:10.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:10.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRepeatingRotatingBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateBy *rotate = [[BERotateBy alloc] initWithTarget:object angleInRadians:endRotation duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:rotate];
    [rotate play];
    
    [repeating updateWithInterval:5.0f];
    float expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedRotation = 60.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedRotation = 90.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

- (void)testRepeatingRotatingByReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    float endRotation = 30.0f;
    BERotateBy *rotate = [[BERotateBy alloc] initWithTarget:object angleInRadians:endRotation duration:30.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:rotate];
    [repeating play];
    
    [repeating updateWithInterval:10.0f];
    float expectedRotation = 10.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:5.0f];
    expectedRotation = 5.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:10.0f];
    expectedRotation = 15.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:10.0f];
    expectedRotation = 25.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:5.0f];
    expectedRotation = 20.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating reverse];
    [repeating updateWithInterval:10.0f];
    expectedRotation = 30.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:10.0f];
    expectedRotation = 40.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
    
    [repeating updateWithInterval:20.0f];
    expectedRotation = 60.0f;
    STAssertTrue(object.rotation == expectedRotation, @"End rotation incorrect.");
}

@end
