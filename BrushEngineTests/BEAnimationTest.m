//
//  BEAnimationTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 03/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEAnimationTest.h"

@implementation BEAnimationTest

- (void)testInit1
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *textureFile = [bundle pathForResource:@"animation_test" ofType:@"png"];
    NSString *controlFile = [bundle pathForResource:@"animation_test" ofType:@"plist"];
    BETextureAtlas *atlas = [[BETextureAtlas alloc] initWithContentsOfFile:textureFile
                                                               controlFile:controlFile];
    BEAnimation *animation = [[BEAnimation alloc] initWithKey:@"test"
                                                        atlas:atlas
                                                    keyFormat:@"run%d.png"
                                                     keyRange:NSMakeRange(1, 10)
                                                    frequency:10];
    
    STAssertTrue([animation frameCount] == 10, @"Wrong number of frames");
    STAssertEqualsWithAccuracy([animation duration], 1.00, 0.001, @"Wrong duration");
    
    BEAnimationFrame *frame0 = [animation frameAtIndex:0];
    BEAnimationFrame *frame1 = [animation frameAtIndex:1];
    BEAnimationFrame *frame2 = [animation frameAtIndex:2];
    BEAnimationFrame *frame3 = [animation frameAtIndex:3];
    BEAnimationFrame *frame4 = [animation frameAtIndex:4];
    BEAnimationFrame *frame5 = [animation frameAtIndex:5];
    BEAnimationFrame *frame6 = [animation frameAtIndex:6];
    BEAnimationFrame *frame7 = [animation frameAtIndex:7];
    BEAnimationFrame *frame8 = [animation frameAtIndex:8];
    BEAnimationFrame *frame9 = [animation frameAtIndex:9];
    
    STAssertEqualsWithAccuracy((float)frame0.offset, 0.1f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame1.offset, 0.2f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame2.offset, 0.3f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame3.offset, 0.4f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame4.offset, 0.5f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame5.offset, 0.6f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame6.offset, 0.7f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame7.offset, 0.8f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame8.offset, 0.9f, 0.001f, @"Wrong offset");
    STAssertEqualsWithAccuracy((float)frame9.offset, 1.0f, 0.001f, @"Wrong offset");
}

@end
