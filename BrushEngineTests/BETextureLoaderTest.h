//
//  BETextureLoaderTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 23/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BETextureLoader.h"
#import "BETextureLoader_Internal.h"

@interface BETextureLoaderTest : SenTestCase

@end
