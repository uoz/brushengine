//
//  BETranslateTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 10/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BETranslateTest.h"

@implementation BETranslateTest

- (void)testTranslateTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateTo *translate = [[BETranslateTo alloc] initWithTarget:object position:endPosition duration:5.0f];
    [translate play];
    
    [translate updateWithInterval:2.5f];
    GLKVector2 expectedPosition = GLKVector2Make(50, 5);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

- (void)testTranslateToReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateTo *translate = [[BETranslateTo alloc] initWithTarget:object position:endPosition duration:5.0f];
    [translate play];
    
    [translate updateWithInterval:2.5f];
    GLKVector2 expectedPosition = GLKVector2Make(50, 5);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate reverse];
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(0, 0);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

- (void)testTranslateBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateBy *translate = [[BETranslateBy alloc] initWithTarget:object position:endPosition duration:5.0f];
    [translate play];
    
    [translate updateWithInterval:2.5f];
    GLKVector2 expectedPosition = GLKVector2Make(50, 5);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

- (void)testTranslateByReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateBy *translate = [[BETranslateBy alloc] initWithTarget:object position:endPosition duration:5.0f];
    [translate play];
    
    [translate updateWithInterval:2.5f];
    GLKVector2 expectedPosition = GLKVector2Make(50, 5);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [translate reverse];
    [translate updateWithInterval:2.5f];
    expectedPosition = GLKVector2Make(0, 0);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

- (void)testRepeatingTranslateTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateTo *translate = [[BETranslateTo alloc] initWithTarget:object position:endPosition duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:translate];
    [repeating play];
    
    [repeating updateWithInterval:5.0f];
    GLKVector2 expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

- (void)testRepeatingTranslateBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endPosition = GLKVector2Make(100, 10);
    BETranslateBy *translate = [[BETranslateBy alloc] initWithTarget:object position:endPosition duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:translate];
    [repeating play];
    
    [repeating updateWithInterval:5.0f];
    GLKVector2 expectedPosition = GLKVector2Make(100, 10);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");

    [repeating updateWithInterval:5.0f];
    expectedPosition = GLKVector2Make(200, 20);
    STAssertTrue(GLKVector2AllEqualToVector2(object.position, expectedPosition), @"End position incorrect.");
}

@end
