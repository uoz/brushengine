//
//  BECameraZoomTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 18/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BECameraZoomTest.h"

@implementation BECameraZoomTest

- (void)testCameraZoomIn
{
    BECamera *camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(100.0f, 100.0f)];
    float endZoom = 0.5f;
    BECameraZoom *cameraZoom = [[BECameraZoom alloc] initWithCamera:camera zoom:endZoom duration:5.0f];
    [cameraZoom play];
    
    [cameraZoom updateWithInterval:2.5f];
    float expectedZoom = 0.75f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 0.5f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 0.5f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
}

- (void)testCameraZoomOut
{
    BECamera *camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(100.0f, 100.0f)];
    float endZoom = 2.0f;
    BECameraZoom *cameraZoom = [[BECameraZoom alloc] initWithCamera:camera zoom:endZoom duration:5.0f];
    [cameraZoom play];
    
    [cameraZoom updateWithInterval:2.5f];
    float expectedZoom = 1.5f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 2.0f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 2.0f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
}

- (void)testCameraZoomInReverse
{
    BECamera *camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(100.0f, 100.0f)];
    float endZoom = 0.5f;
    BECameraZoom *cameraZoom = [[BECameraZoom alloc] initWithCamera:camera zoom:endZoom duration:5.0f];
    [cameraZoom play];
    
    [cameraZoom updateWithInterval:2.5f];
    float expectedZoom = 0.75f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom reverse];
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 1.0f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
    
    [cameraZoom reverse];
    [cameraZoom updateWithInterval:2.5f];
    expectedZoom = 1.0f;
    STAssertTrue(camera.zoom == expectedZoom, @"End zoom incorrect.");
}

@end
