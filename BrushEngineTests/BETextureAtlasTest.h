//
//  BETextureAtlasTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 23/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BETextureAtlas.h"

@interface BETextureAtlasTest : SenTestCase

@end
