//
//  BECameraTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 05/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BECamera.h"
#import "BECamera_Internal.h"

@interface BECameraTest : SenTestCase

@end
