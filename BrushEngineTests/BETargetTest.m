//
//  BETargetTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 21/12/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BETargetTest.h"

@implementation BETargetTest

- (void)testTarget
{
    [BETarget reset];
    
    BETarget *target1 = [[BETarget alloc] init];
    BETarget *target2 = [[BETarget alloc] init];
    BETarget *target3 = [[BETarget alloc] init];
    BETarget *target4 = [[BETarget alloc] init];
    
    STAssertEquals(target1.targetID, 0, @"targetID incorrect");
    STAssertEquals(target2.targetID, 1, @"targetID incorrect");
    STAssertEquals(target3.targetID, 2, @"targetID incorrect");
    STAssertEquals(target4.targetID, 3, @"targetID incorrect");
}

@end
