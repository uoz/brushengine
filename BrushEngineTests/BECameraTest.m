//
//  BECameraTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 05/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BECameraTest.h"

@implementation BECameraTest

- (void)testInit1
{
    CGSize viewportSize = CGSizeMake(500, 500);
    CGSize worldSize = CGSizeMake(1000, 1000);
    BECamera *camera = [[BECamera alloc] initWithViewportSize:viewportSize target:nil worldSize:worldSize];
    
    CGRect expectedOuterFrame = CGRectMake(0.0f, 0.0f, viewportSize.width, viewportSize.height);
    STAssertTrue(CGRectEqualToRect(camera.outerFrame, expectedOuterFrame), @"OuterFrame incorrect.");
    
    CGRect expectedInnerFrame = CGRectMake(viewportSize.width / 2, viewportSize.height / 2, 0, 0);
    STAssertTrue(CGRectEqualToRect(camera.innerFrame, expectedInnerFrame), @"InnerFrame incorrect.");
}

- (void)testInit2
{
    CGSize viewportSize = CGSizeMake(500, 500);
    CGSize worldSize = CGSizeMake(1000, 1000);
    CGRect outerFream = CGRectMake(0.0f, 0.0f, 600, 600);
    CGRect innerFrame = CGRectMake(0.0f, 0.0f, 600, 600);
    CGRect viewport = CGRectMake(0.0f, 0.0f, viewportSize.width, viewportSize.height);
    BECamera *camera = [[BECamera alloc] initWithViewportSize:viewportSize target:nil worldSize:worldSize outerFrame:outerFream innerFrame:innerFrame];
    STAssertTrue(CGRectEqualToRect(camera.outerFrame, viewport), @"OuterFrame incorrect");
    STAssertTrue(CGRectEqualToRect(camera.innerFrame, viewport), @"InnerFrame incorrect");
}

@end
