//
//  BECameraShakeTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 02/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "BECamera.h"
#import "BECameraShake.h"

@interface BECameraShakeTest : SenTestCase

@end
