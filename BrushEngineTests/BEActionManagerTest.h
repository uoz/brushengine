//
//  BEActionManagerTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 09/12/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BEActionManager.h"
#import "BEActionManager_Internal.h"
#import "BETarget.h"
#import "BEAction.h"

@interface BEActionManagerTest : SenTestCase

@end
