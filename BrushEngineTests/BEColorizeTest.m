//
//  BEColorizeTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 13/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEColorizeTest.h"

@implementation BEColorizeTest

- (void)testColorize
{
    BESprite *object = [[BESprite alloc] init];
    GLKVector4 endColor = GLKVector4Make(0.5f, 0.5f, 0.5f, 0.5f);
    BEColorize *colorize = [[BEColorize alloc] initWithTarget:object color:endColor duration:5.0f];
    [colorize play];
    
    [colorize updateWithInterval:2.5f];
    GLKVector4 expectedColor = GLKVector4Make(0.25f, 0.25f, 0.25f, 0.25f);
    STAssertTrue(GLKVector4AllEqualToVector4(object.color, expectedColor), @"End color incorrect.");
    
    [colorize updateWithInterval:2.5f];
    expectedColor = GLKVector4Make(0.5f, 0.5f, 0.5f, 0.5f);
    STAssertTrue(GLKVector4AllEqualToVector4(object.color, expectedColor), @"End color incorrect.");
    
    [colorize updateWithInterval:2.5f];
    expectedColor = GLKVector4Make(0.5f, 0.5f, 0.5f, 0.5f);
    STAssertTrue(GLKVector4AllEqualToVector4(object.color, expectedColor), @"End color incorrect.");
}

- (void)testColorizeReverse
{
    BESprite *object = [[BESprite alloc] init];
    GLKVector4 endColor = GLKVector4Make(0.5f, 0.5f, 0.5f, 0.5f);
    BEColorize *colorize = [[BEColorize alloc] initWithTarget:object color:endColor duration:5.0f];
    [colorize play];
    
    [colorize updateWithInterval:2.5f];
    GLKVector4 expectedColor = GLKVector4Make(0.25f, 0.25f, 0.25f, 0.25f);
    STAssertTrue(GLKVector4AllEqualToVector4(object.color, expectedColor), @"End color incorrect.");
    
    [colorize reverse];
    [colorize updateWithInterval:2.5f];
    expectedColor = GLKVector4Make(0.0f, 0.0f, 0.0f, 0.0f);
    STAssertTrue(GLKVector4AllEqualToVector4(object.color, expectedColor), @"End color incorrect.");
}

@end
