//
//  BETextureLoaderTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 23/02/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BETextureLoaderTest.h"

@implementation BETextureLoaderTest

- (void)testTextureLoader
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *textureFile = [bundle pathForResource:@"animation_test" ofType:@"png"];
    [BETextureLoader purgeCachedTextures];
    STAssertTrue([BETextureLoader loadedTextureCount] == 0, @"Texture count incorrect.");
    [BETextureLoader textureWithContentsOfFile:textureFile];
    STAssertTrue([BETextureLoader loadedTextureCount] == 1, @"Texture count incorrect.");
    [BETextureLoader textureWithContentsOfFile:@"bananas.png"];
    STAssertTrue([BETextureLoader loadedTextureCount] == 1, @"Texture count incorrect.");

}

@end
