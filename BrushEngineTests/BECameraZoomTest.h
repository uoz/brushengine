//
//  BECameraZoomTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 18/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "BECamera.h"
#import "BECameraZoom.h"

@interface BECameraZoomTest : SenTestCase

@end
