//
//  BEQuadBatchRendererTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 24/05/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "BEQuadBatchRenderer.h"
#import "BEQuadBatchRenderer_Internal.h"
#import "BESprite.h"

@interface BEQuadBatchRendererTest : SenTestCase

@end
