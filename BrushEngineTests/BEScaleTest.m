//
//  BEScaleTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 10/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEScaleTest.h"

@implementation BEScaleTest

- (void)testScaleTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleTo *scale = [[BEScaleTo alloc] initWithTarget:object scale:endScale duration:5.0f];
    [scale play];
    
    [scale updateWithInterval:2.5f];
    GLKVector2 expectedScale = GLKVector2Make(50.5f, 5.5f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

- (void)testScaleToReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleTo *scale = [[BEScaleTo alloc] initWithTarget:object scale:endScale duration:5.0f];
    [scale play];
    
    [scale updateWithInterval:2.5f];
    GLKVector2 expectedScale = GLKVector2Make(50.5f, 5.5f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [scale reverse];
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(1.0f, 1.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

- (void)testScaleBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleBy *scale = [[BEScaleBy alloc] initWithTarget:object scale:endScale duration:5.0f];
    [scale play];
    
    [scale updateWithInterval:2.5f];
    GLKVector2 expectedScale = GLKVector2Make(50.5f, 5.5f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

- (void)testScaleByReverse
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleBy *scale = [[BEScaleBy alloc] initWithTarget:object scale:endScale duration:5.0f];
    [scale play];
    
    [scale updateWithInterval:2.5f];
    GLKVector2 expectedScale = GLKVector2Make(50.5f, 5.5f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");

    [scale reverse];
    [scale updateWithInterval:2.5f];
    expectedScale = GLKVector2Make(1.0f, 1.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

- (void)testRepeatingScaleTo
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleTo *scale = [[BEScaleTo alloc] initWithTarget:object scale:endScale duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:scale];
    [scale play];
    
    [repeating updateWithInterval:5.0f];
    GLKVector2 expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

- (void)testRepeatingScaleBy
{
    BEGameObject *object = [[BEGameObject alloc] init];
    GLKVector2 endScale = GLKVector2Make(100.0f, 10.0f);
    BEScaleBy *scale = [[BEScaleBy alloc] initWithTarget:object scale:endScale duration:5.0f];
    BERepeatingAction *repeating = [[BERepeatingAction alloc] initWithAction:scale];
    [scale play];
    
    [repeating updateWithInterval:5.0f];
    GLKVector2 expectedScale = GLKVector2Make(100.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
    
    [repeating updateWithInterval:5.0f];
    expectedScale = GLKVector2Make(10000.0f, 100.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(object.scale, expectedScale), @"End scale incorrect.");
}

@end
