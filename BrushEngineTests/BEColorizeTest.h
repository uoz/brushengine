//
//  BEColorizeTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 13/10/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BESprite.h"
#import "BEColorize.h"

@interface BEColorizeTest : SenTestCase

@end
