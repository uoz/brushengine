//
//  BERotateTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 11/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BEGameObject.h"
#import "BERotate.h"
#import "BERepeatingAction.h"

@interface BERotateTest : SenTestCase

@end
