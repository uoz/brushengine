//
//  BEGameObjectTests.m
//  BrushEngine
//
//  Created by Germano Guerrini on 22/06/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEGameObjectTests.h"

@implementation BEGameObjectTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testDescendancy
{
    BEGameObject *parent = [[BEGameObject alloc] init];
    BEGameObject *childA = [[BEGameObject alloc] init];
    BEGameObject *childB = [[BEGameObject alloc] init];
    BEGameObject *grandChildA1 = [[BEGameObject alloc] init];
    BEGameObject *grandChildA2 = [[BEGameObject alloc] init];
    BEGameObject *grandChildB1 = [[BEGameObject alloc] init];
    BEGameObject *grandChildB2 = [[BEGameObject alloc] init];
    
    [parent addChild:childA];
    [parent addChild:childB];
    
    [childA addChild:grandChildA1];
    [childA addChild:grandChildA2];
    
    [childB addChild:grandChildB1];
    [childB addChild:grandChildB2];
    
    STAssertTrue([grandChildA1 isDescendantOf:parent], @"@grandChildA1 is not a descendant of @parent");
    STAssertTrue([grandChildB1 isDescendantOf:parent], @"@grandChildB1 is not a descendant of @parent");
    
    STAssertFalse([grandChildA1 isDescendantOf:childB], @"@grandChildA1 should not be a descendant of @childB");
    STAssertFalse([grandChildB1 isDescendantOf:childA], @"@grandChildB1 should not be a descendant of @childA");
    
    [childA removeChild:grandChildA1];
    
    STAssertFalse([grandChildA1 isDescendantOf:childA], @"@grandChildA1 should not be a descendant of @childA");
    STAssertFalse([grandChildA1 isDescendantOf:parent], @"@grandChildA1 should not be a descendant of @parent");
    
    [parent swapChild:childA withChild:childB];
    
    STAssertEqualObjects([parent getChildAtIndex:0], childB, @"childB should be the first child of @parent");
    STAssertEqualObjects([parent getLastChild], childA, @"childA should be the last child of @parent");
    
    [childA addChild:grandChildB1];
    
    STAssertFalse([grandChildB1 isDescendantOf:childB], @"@grandChildB1 is still a descendant of @childB");
    STAssertTrue([grandChildB1 isDescendantOf:childA], @"@grandChildB1 is not a descendant of @childA");
    
    [childB removeAllChildren];
    
    STAssertFalse([grandChildB2 isDescendantOf:childB], @"@grandChildB2 should not be a descendant of @childB");
    STAssertNil([childB getChildAtIndex:0], @"child of @childB at index 0 should be nil");
    
    STAssertThrows([parent addChild:nil], @"Should throws assertion");
    STAssertThrows([parent addChild:parent], @"Should throws assertion");
}

- (void)testWorldPointToLocalSpace
{
    BEGameObject *object1 = [[BEGameObject alloc] init];
    BEGameObject *object2 = [[BEGameObject alloc] init];
    BEGameObject *object3 = [[BEGameObject alloc] init];
    
    [object1 addChild:object2];
    [object2 addChild:object3];
    
    object1.position = GLKVector2Make(10.0f, 20.0f);
    object2.position = GLKVector2Make(100.0f, 200.0f);
    object3.position = GLKVector2Make(1000.0f, 2000.0f);
    
    GLKVector2 worldPoint = GLKVector2Make(0.0f, 0.0f);
    
    GLKVector2 localPoint = [object1 worldPointToLocalSpace:worldPoint];
    GLKVector2 expectedPoint = GLKVector2Make(-10.0f, -20.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object2 worldPointToLocalSpace:worldPoint];
    expectedPoint = GLKVector2Make(-110.0f, -220.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object3 worldPointToLocalSpace:worldPoint];
    expectedPoint = GLKVector2Make(-1110.0f, -2220.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
}

- (void)testLocalPointToWorldSpace
{
    BEGameObject *object1 = [[BEGameObject alloc] init];
    BEGameObject *object2 = [[BEGameObject alloc] init];
    BEGameObject *object3 = [[BEGameObject alloc] init];
    
    [object1 addChild:object2];
    [object2 addChild:object3];
    
    object1.position = GLKVector2Make(10.0f, 20.0f);
    object2.position = GLKVector2Make(100.0f, 200.0f);
    object3.position = GLKVector2Make(1000.0f, 2000.0f);
    
    GLKVector2 localPoint = GLKVector2Make(0.0f, 0.0f);
    
    GLKVector2 worldPoint = [object1 localPointToWorldSpace:localPoint];
    GLKVector2 expectedPoint = GLKVector2Make(10.0f, 20.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object2 localPointToWorldSpace:localPoint];
    expectedPoint = GLKVector2Make(110.0f, 220.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object3 localPointToWorldSpace:localPoint];
    expectedPoint = GLKVector2Make(1110.0f, 2220.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
}

- (void)testWorldPointToLocalSpaceRelativeToPivot
{
    BEGameObject *object1 = [[BEGameObject alloc] init];
    BEGameObject *object2 = [[BEGameObject alloc] init];
    BEGameObject *object3 = [[BEGameObject alloc] init];
    
    [object1 addChild:object2];
    [object2 addChild:object3];
    
    object1.position = GLKVector2Make(10.0f, 20.0f);
    object2.position = GLKVector2Make(100.0f, 200.0f);
    object3.position = GLKVector2Make(1000.0f, 2000.0f);
    
    object1.pivot = GLKVector2Make(5.0f, 10.0f);
    object2.pivot = GLKVector2Make(50.0f, 100.0f);
    object3.pivot = GLKVector2Make(500.0f, 1000.0f);
    
    GLKVector2 worldPoint = GLKVector2Make(0.0f, 0.0f);
    
    GLKVector2 localPoint = [object1 worldPointToLocalSpace:worldPoint relativeToPivot:YES];
    GLKVector2 expectedPoint = GLKVector2Make(-10.0f, -20.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object2 worldPointToLocalSpace:worldPoint relativeToPivot:YES];
    expectedPoint = GLKVector2Make(-105.0f, -210.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object3 worldPointToLocalSpace:worldPoint relativeToPivot:YES];
    expectedPoint = GLKVector2Make(-1055.0f, -2110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);

    localPoint = [object1 worldPointToLocalSpace:worldPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(-5.0f, -10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object2 worldPointToLocalSpace:worldPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(-55.0f, -110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
    
    localPoint = [object3 worldPointToLocalSpace:worldPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(-555.0f, -1110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(localPoint, expectedPoint),
                 @"Wrong local point for worldPoint: {%f, %f}", localPoint.x, localPoint.y);
}

- (void)testLocalPointToWorldSpaceRelativeToPivot
{
    BEGameObject *object1 = [[BEGameObject alloc] init];
    BEGameObject *object2 = [[BEGameObject alloc] init];
    BEGameObject *object3 = [[BEGameObject alloc] init];
    
    [object1 addChild:object2];
    [object2 addChild:object3];
    
    object1.position = GLKVector2Make(10.0f, 20.0f);
    object2.position = GLKVector2Make(100.0f, 200.0f);
    object3.position = GLKVector2Make(1000.0f, 2000.0f);
    
    object1.pivot = GLKVector2Make(5.0f, 10.0f);
    object2.pivot = GLKVector2Make(50.0f, 100.0f);
    object3.pivot = GLKVector2Make(500.0f, 1000.0f);
    
    GLKVector2 localPoint = GLKVector2Make(0.0f, 0.0f);
    
    GLKVector2 worldPoint = [object1 localPointToWorldSpace:localPoint relativeToPivot:YES];
    GLKVector2 expectedPoint = GLKVector2Make(10.0f, 20.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object2 localPointToWorldSpace:localPoint relativeToPivot:YES];
    expectedPoint = GLKVector2Make(105.0f, 210.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object3 localPointToWorldSpace:localPoint relativeToPivot:YES];
    expectedPoint = GLKVector2Make(1055.0f, 2110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object1 localPointToWorldSpace:localPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(5.0f, 10.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object2 localPointToWorldSpace:localPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(55.0f, 110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
    
    worldPoint = [object3 localPointToWorldSpace:localPoint relativeToPivot:NO];
    expectedPoint = GLKVector2Make(555.0f, 1110.0f);
    STAssertTrue(GLKVector2AllEqualToVector2(worldPoint, expectedPoint),
                 @"Wrong world point for localPoint: {%f, %f}", worldPoint.x, worldPoint.y);
}

@end
