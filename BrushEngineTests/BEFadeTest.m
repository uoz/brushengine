//
//  BEFadeTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 02/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BEFadeTest.h"

@implementation BEFadeTest

- (void)testInvalidInit
{
    BEGameObject *object = [[BEGameObject alloc] init];
    BEFade *fade = [[BEFade alloc] initWithTarget:object alpha:0.0f];
    STAssertNil(fade, @"nil expected");
}

- (void)testFade
{
    BESprite *object = [[BESprite alloc] init];
    float endAlpha = 0.5f;

    BEFade *fade = [[BEFade alloc] initWithTarget:object alpha:endAlpha duration:5.0f];
    [fade play];
    
    [fade updateWithInterval:2.5f];
    float expectedAlpha = 0.25f;
    STAssertTrue(object.alpha == expectedAlpha, @"End alpha incorrect.");
    
    [fade updateWithInterval:2.5f];
    expectedAlpha = expectedAlpha = 0.5f;
    STAssertTrue(object.alpha == expectedAlpha, @"End alpha incorrect.");
    
    [fade updateWithInterval:2.5f];
    expectedAlpha = expectedAlpha = 0.5f;
    STAssertTrue(object.alpha == expectedAlpha, @"End alpha incorrect.");
}

- (void)testFadeReverse
{
    BESprite *object = [[BESprite alloc] init];
    float endAlpha = 0.5f;
    
    BEFade *fade = [[BEFade alloc] initWithTarget:object alpha:endAlpha duration:5.0f];
    [fade play];
    
    [fade updateWithInterval:2.5f];
    float expectedAlpha = 0.25f;
    STAssertTrue(object.alpha == expectedAlpha, @"End alpha incorrect.");
    
    [fade reverse];
    [fade updateWithInterval:2.5f];
    expectedAlpha = expectedAlpha = 0.0f;
    STAssertTrue(object.alpha == expectedAlpha, @"End alpha incorrect.");}

@end
