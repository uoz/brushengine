//
//  BEActionManagerTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 09/12/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import "BEActionManagerTest.h"

@implementation BEActionManagerTest

- (void)testRegister
{
    BEActionManager *actionManager = [BEActionManager sharedActionManager];
    [actionManager unregisterAllActions];
    
    BETarget *target1 = [[BETarget alloc] init];
    BETarget *target2 = [[BETarget alloc] init];
    BETarget *target3 = [[BETarget alloc] init];
    BETarget *target4 = [[BETarget alloc] init];
    
    BEAction *action1 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action2 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target2];
    BEAction *action3 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target3];
    BEAction *action4 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target4];

    STAssertTrue([actionManager registeredTargetCount] == 4, @"Number of registered target incorrect.");
}

- (void)testRegisterSameTarget
{
    BEActionManager *actionManager = [BEActionManager sharedActionManager];
    [actionManager unregisterAllActions];
    
    BETarget *target1 = [[BETarget alloc] init];
    
    BEAction *action1 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action2 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action3 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action4 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    
    STAssertTrue([actionManager actionsWithTargetCount:target1] == 4, @"Number of registered target incorrect.");
}

- (void)testRegisterMix
{
    BEActionManager *actionManager = [BEActionManager sharedActionManager];
    [actionManager unregisterAllActions];
    
    BETarget *target1 = [[BETarget alloc] init];
    BETarget *target2 = [[BETarget alloc] init];
    
    BEAction *action1 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action2 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action3 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target2];
    BEAction *action4 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target2];
    
    STAssertTrue([actionManager actionsWithTargetCount:target1] == 2, @"Number of registered target incorrect.");
    STAssertTrue([actionManager actionsWithTargetCount:target2] == 2, @"Number of registered target incorrect.");
    STAssertTrue([actionManager registeredTargetCount] == 2, @"Number of registered target incorrect.");
}

- (void)testUnregister
{
    BEActionManager *actionManager = [BEActionManager sharedActionManager];
    [actionManager unregisterAllActions];
    
    BETarget *target1 = [[BETarget alloc] init];
    BETarget *target2 = [[BETarget alloc] init];
    
    BEAction *action1 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action2 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action3 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target2];
    BEAction *action4 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target2];
    
    [actionManager unregisterAction:action1];
    STAssertTrue([actionManager actionsWithTargetCount:target1] == 1, @"Number of registered target incorrect.");
    
    [actionManager unregisterAction:action2];
    STAssertTrue([actionManager actionsWithTargetCount:target1] == 0, @"Number of registered target incorrect.");
    
    [actionManager unregisterActionsWithTarget:target2];
    STAssertTrue([actionManager actionsWithTargetCount:target2] == 0, @"Number of registered target incorrect.");
    
    // Should fail silently
    [actionManager unregisterAction:action1];
    [actionManager unregisterActionsWithTarget:target2];
}

- (void)testPausingAndResumeTarget
{
    BEActionManager *actionManager = [BEActionManager sharedActionManager];
    [actionManager unregisterAllActions];
    
    BETarget *target1 = [[BETarget alloc] init];

    BEAction *action1 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action2 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action3 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    BEAction *action4 __attribute__((unused)) = [[BEAction alloc] initWithTarget:target1];
    
    [action1 play];
    [action3 play];
    
    NSSet *actionsToResume = [actionManager pauseRunningActionsWithTarget:target1];
    
    STAssertTrue([actionsToResume containsObject:action1], @"Action not in set");
    STAssertTrue([actionsToResume containsObject:action3], @"Action not in set");
    STAssertFalse([actionsToResume containsObject:action2], @"Action in set");
    STAssertFalse([actionsToResume containsObject:action4], @"Action in set");
    
    STAssertFalse([action1 isRunning], @"Action is running");
    STAssertFalse([action2 isRunning], @"Action is running");
    STAssertFalse([action3 isRunning], @"Action is running");
    STAssertFalse([action4 isRunning], @"Action is running");
    
    [actionManager playActions:actionsToResume withTarget:target1];
    
    STAssertTrue([action1 isRunning], @"Action is running");
    STAssertFalse([action2 isRunning], @"Action is running");
    STAssertTrue([action3 isRunning], @"Action is running");
    STAssertFalse([action4 isRunning], @"Action is running");
}

@end
