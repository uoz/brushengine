//
//  BESpriteTests.h
//  BrushEngine
//
//  Created by Germano Guerrini on 04/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <GLKit/GLKit.h>

#import "BESprite.h"

@interface BESpriteTests : SenTestCase

@end
