//
//  BECameraShakeTest.m
//  BrushEngine
//
//  Created by Germano Guerrini on 02/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import "BECameraShakeTest.h"

@implementation BECameraShakeTest

- (void)testCameraShakeVertical
{
    BECamera *camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(100.0f, 100.0f)];
    BECameraShake *cameraShake = [[BECameraShake alloc] initWithCamera:camera type:BEShakeTypeVertical magnitude:1.0f duration:5.0f];
    [cameraShake play];
    
    [cameraShake updateWithInterval:2.5f];
    STAssertTrue(camera.shake.x == 0.0f, @"End shake incorrect.");
    STAssertFalse(camera.shake.y == 0.0f, @"End shake incorrect.");
}

- (void)testCameraShakeHorizontal
{
    BECamera *camera = [[BECamera alloc] initWithViewportSize:CGSizeMake(100.0f, 100.0f)];
    BECameraShake *cameraShake = [[BECameraShake alloc] initWithCamera:camera type:BEShakeTypeHorizontal magnitude:1.0f duration:5.0f];
    [cameraShake play];
    
    [cameraShake updateWithInterval:2.5f];
    STAssertFalse(camera.shake.x == 0.0f, @"End shake incorrect.");
    STAssertTrue(camera.shake.y == 0.0f, @"End shake incorrect.");
}

@end
