//
//  BEAnimationTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 03/03/13.
//  Copyright (c) 2013 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BEAnimation.h"
#import "BEAnimationFrame.h"
#import "BETextureAtlas.h"

@interface BEAnimationTest : SenTestCase

@end
