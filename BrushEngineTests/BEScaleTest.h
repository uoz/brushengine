//
//  BEScaleTest.h
//  BrushEngine
//
//  Created by Germano Guerrini on 10/07/12.
//  Copyright (c) 2012 BitCanvas. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "BEGameObject.h"
#import "BEScale.h"
#import "BERepeatingAction.h"

@interface BEScaleTest : SenTestCase

@end
